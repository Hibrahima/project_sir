Projet Doodle - SIR  
## But du projet
Le but de ce projet est de développer une application web java style doodle-like. Il s'agit de permettre aux utilisateurs de proposer un/des créneau(x) horaire(s) afin de choisir la date et le créneau adéquats pour une reunion. 

## Lancer le projet
### Préréquis
- Java 8 ou plus 
- Maven 
- Angular (et cli) 
- Node (et npm)

### Comment lancer
- Faire les étape de la configuration de la base de données ( cf. **ETAPE 1** un peu plus en bas) 
- Cloner le projet ou téléchargez le (tag **v2** ou branche **master**) 
- Se déplacer dans le dossier **backend** du projet et exécuter la commande : **mvn tomcat7:run** 
- Se déplacer dans le dossier **frontend** du projet 
- Installer les modules (dépendances du projet) avec la commande : **npm i** 
- Lancer le frontend avec la commande : **npm start** 
- S'assurer que l'application tourne sur le port **8080** en tapant dans votre navigateur : **localhost:8080** 
- Utiliser les données du fichier **credentials** (à la racine du projet) pour vous connecter. (Vous pourrez tout de même créé de nouveaux comptes si vous le souhaitez) 
- Enjoy it :smile:

**Note**: L'envoi des mails lors de la validation de la date d'un sondage est effective dans cette version. Nous avons utilisé l'api v3.1 de MailJet pour cela. Lorsqu'une date et une plage horaire sont choisies, un mail sera envoyé à tous les participants qui avaient choisi cette date (sous contrainte que les adresses email soient valides) et il leur est démandé de rensigner leurs préférences alimentaires et allergies. Vous pouvez également consulter la rubrique 'Aide' de l'application pour apprendre à l'utiliser plus simplement.

### Quelques images du projet
Exemple de mail envoyé

![alt text][mail]

[mail]: images/mail.png

Dashboard Participant
![alt text][dashboard_participant]

[dashboard_participant]: images/dashboard_participant.png

Dashboard Admin
![alt text][dashboard_admin]

[dashboard_admin]: images/dashboard_admin.png

Sondages
![alt text][surveys]

[surveys]: images/surveys.png

Participer
![alt text][participate]

[participate]: images/participate.png

Un sondage validé
![alt text][validated_surveys.png]

[validated_surveys.png]: images/validated_surveys.png



## ~~ Fonctionnalités (Tout ce qui suit est ancien => pas la peine de lire) ~~ 
### Fonctionnalités supportées par le projet
Dans le but de proposer les fonctionnalités principales du projet, une liste exhaustive des services déjà développés et testés est présentée ci-dessous 
- Créer un compte utilisateur
- Créer un sondage
- Créer des plages horaires
- Ajouter une collection de plages horaires aux sondages crées
- Récupérer les réponses des sondages
- Valider une date et un créneau pour une reunion 
- Envoyer des mails pour renseigner les preferences lorsque le créneau choisi contient une pause
- Envoyer des mails pour participer aux sondages créés 
- Hasher le mot de passe 
### Fonctionnalités encore a développer
- Lien de participation au sondage : cette fonctionnalité a été laissée à plus tard pour le développement du front-end. En effet, il est prévu que ce lien redirige vers un composant du front-end et cela explique notre choix.
- Sécuriser les services backend (en fonction des roles - authorization and authentication) : Un plus que nous envisageons d'implementer
- Tests unitaires
- Intégration d'une api de gestion de pad
- Envoi de mail
## Technologies
Le projet a été mis en oeuvre avec le langage de programmation **Java**. La couche d’accès aux données (DAO = Data Access Object) et de la persistence de données est gérée par **JPA** et **Hibernate** et la couche de service par **Jersey**. La base de données utilisée est **MySQL**. Le hashage est réalisé par la librairie **Bcrypt** et l'envoi de mail par **Apache Commons Email**.
- Hibernate : version 4.3.10
- MySQL : version 5.1.47
- Jersey :  version 1.19.4
- Bcrypt: version 0.3m
- Commons Email : version 1.5

Le plugin maven **Tomcat 7** de Apache est utilisé comme web container pour compiler et exécuter l’application.

## Lancer l'application
Il est nécessaire d'effectuer certaines tâches afin de pouvoir tester aisément le projet.
- Se déplacer dans un dossier ou le projet sera téléchargé.
- Cloner le projet ou télécharger le     
**git clone https://gitlab.istic.univ-rennes1.fr/ihaidara/project_sir.git**
### Base de données (ETAPE 1)
Ces operations peuvent être faites au travers d'une application graphique comme **phpMyAdmin** mais cependant nous fournissons ici les commandes sql à exécuter en ligne de commande.
- Se connecter au serveur mysql installé sur votre machine
- Créer une base de données **MySQL** nommée **db_sir**     
**create database db_sir;**              
- Créer un utilisateur db_sir_user avec le mot de passe db_sir_pass     
**create user 'db_sir_user'@'localhost'  identified by  'db_sir_pass';**
- Octroyer tous les droits sur la base   
 **grant all privileges on db_sir.\*  TO  'db_sir_user'@'localhost';**
- Importer le backup (db_sir.sql à la racine du projet):    
**mysql -u \<user> -p  db_sir < db_sir.sql** - remplacer user par un utilisateur valide - renseigner le mot de passe. Attention toute autre base de donnée avec le même nom sera écrasée. 
### Lancer le projet avec Maven
- Se déplacer dans le dossier **backend** du projet
- Taper **mvn tomcat7:run**
- Enjoy it :smiley: !

## Remarque
- Le dossier **api** contient  des fichiers collections **Postman**
-  Ces fichiers pourront être utilisés pour tester l'application en les important dans Postman
- Toutes les requêtes http devront normalement produire un resultant même en cas d'erreur (grâce a notre super api - couche de service)  
- Le dossier **documentation** contient  le diagramme
- N'oublier pas les point virgule a la fin des requêtes sql :smiley: !
- La branche master ou le tag **backend_v1** peuvent être utilises pour tester
- Ne donc pas oublier de se déplacer sur la bonne branche si le projet a été cloné


#### Auteurs
HAIDARA Ibrahima   
COULIBALY Mariam
 

-- MySQL dump 10.13  Distrib 8.0.16, for Linux (x86_64)
--
-- Host: localhost    Database: db_sir
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `allergy`
--

DROP TABLE IF EXISTS `allergy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `allergy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rl7nfl6fe5xc5kl21h3uxy6qx` (`user_id`),
  CONSTRAINT `FK_rl7nfl6fe5xc5kl21h3uxy6qx` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allergy`
--

LOCK TABLES `allergy` WRITE;
/*!40000 ALTER TABLE `allergy` DISABLE KEYS */;
INSERT INTO `allergy` VALUES (8,'sucre',2);
/*!40000 ALTER TABLE `allergy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_date`
--

DROP TABLE IF EXISTS `custom_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `custom_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fqiuq5ri8yq90slku53hevh61` (`survey_id`),
  CONSTRAINT `FK_fqiuq5ri8yq90slku53hevh61` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_date`
--

LOCK TABLES `custom_date` WRITE;
/*!40000 ALTER TABLE `custom_date` DISABLE KEYS */;
INSERT INTO `custom_date` VALUES (1,_binary '\0','2019-05-13','Date numero 1',1),(2,_binary '\0','2019-05-16','Date numero 2',36),(3,_binary '\0','2019-05-08','Date numero 3',37),(4,_binary '\0','2019-05-14','Date numero 4',38),(5,_binary '\0','2019-05-13','Date numero 5',38),(6,_binary '\0','2019-05-24','Date numero 6',1),(7,_binary '\0','2019-05-20','Date numero 7',38),(8,_binary '\0','2019-05-13','Date numero 8',36),(9,_binary '','2019-05-22','Date numero 9',NULL),(10,_binary '','2019-05-16','Date numero 10',NULL);
/*!40000 ALTER TABLE `custom_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_preference`
--

DROP TABLE IF EXISTS `food_preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `food_preference` (
  `meeting_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`meeting_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_preference`
--

LOCK TABLES `food_preference` WRITE;
/*!40000 ALTER TABLE `food_preference` DISABLE KEYS */;
INSERT INTO `food_preference` VALUES (25,2,'Du poulet bien braise','poulet au beurre');
/*!40000 ALTER TABLE `food_preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meeting`
--

DROP TABLE IF EXISTS `meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `office_address` varchar(255) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `time_slot` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_4r5lkb4luvtkqj7x1gxg96kth` (`title`),
  UNIQUE KEY `UK_gaf9qh6r2tv37w0t8lvy8lo30` (`survey_id`),
  CONSTRAINT `FK_gaf9qh6r2tv37w0t8lvy8lo30` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meeting`
--

LOCK TABLES `meeting` WRITE;
/*!40000 ALTER TABLE `meeting` DISABLE KEYS */;
INSERT INTO `meeting` VALUES (1,'2019-05-24','Rennes',NULL,'14:30 - 15:45','Reunion numero 1',1),(25,'2019-05-16','Paris',NULL,'11:00 - 12:00','Reunion numero 2',36),(26,NULL,'Lyon',NULL,NULL,'Reunion numero 3',37),(27,NULL,'Bamako',NULL,NULL,'Reunion numero 4',38);
/*!40000 ALTER TABLE `meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participation`
--

DROP TABLE IF EXISTS `participation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `participation` (
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `participation_date` date DEFAULT NULL,
  `participation_time` time DEFAULT NULL,
  PRIMARY KEY (`survey_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participation`
--

LOCK TABLES `participation` WRITE;
/*!40000 ALTER TABLE `participation` DISABLE KEYS */;
INSERT INTO `participation` VALUES (1,2,'2019-05-12','11:52:19'),(36,2,'2019-05-12','12:41:10');
/*!40000 ALTER TABLE `participation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participation_time_slot`
--

DROP TABLE IF EXISTS `participation_time_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `participation_time_slot` (
  `participation_survey_id` int(11) NOT NULL,
  `participation_user_id` int(11) NOT NULL,
  `timeSlots_id` int(11) NOT NULL,
  KEY `FK_m1nhvvygrov7j563hpuflqioj` (`timeSlots_id`),
  KEY `FK_9lul3x9be6vvug3wwm6x61r7h` (`participation_survey_id`,`participation_user_id`),
  CONSTRAINT `FK_9lul3x9be6vvug3wwm6x61r7h` FOREIGN KEY (`participation_survey_id`, `participation_user_id`) REFERENCES `participation` (`survey_id`, `user_id`),
  CONSTRAINT `FK_m1nhvvygrov7j563hpuflqioj` FOREIGN KEY (`timeSlots_id`) REFERENCES `time_slot` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participation_time_slot`
--

LOCK TABLES `participation_time_slot` WRITE;
/*!40000 ALTER TABLE `participation_time_slot` DISABLE KEYS */;
INSERT INTO `participation_time_slot` VALUES (1,2,4),(1,2,41),(1,2,44),(1,2,1),(36,2,9),(36,2,51),(36,2,7);
/*!40000 ALTER TABLE `participation_time_slot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8sewwnpamngi6b1dwaa88askk` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (3,'admin'),(2,'creator'),(1,'participant');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey`
--

DROP TABLE IF EXISTS `survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` date DEFAULT NULL,
  `creation_time` time DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1nlisff8xtlxkgr6l8e2x5uqd` (`createdBy_id`),
  CONSTRAINT `FK_1nlisff8xtlxkgr6l8e2x5uqd` FOREIGN KEY (`createdBy_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey`
--

LOCK TABLES `survey` WRITE;
/*!40000 ALTER TABLE `survey` DISABLE KEYS */;
INSERT INTO `survey` VALUES (1,'2019-05-12','03:36:49','surveys/1/participate','Sondage numero 1',3,2),(36,'2019-05-12','10:52:51','surveys/36/participate','Sondage numero 2',3,2),(37,'2019-05-12','11:11:48','surveys/37/participate','Sondage numero 3',1,2),(38,'2019-05-12','11:20:19','surveys/38/participate','Sondage numero 4',1,2);
/*!40000 ALTER TABLE `survey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_result`
--

DROP TABLE IF EXISTS `survey_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `survey_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `time_slot_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_o3eqsfq8ghehmn4jti71x4xjc` (`survey_id`),
  UNIQUE KEY `UK_ebhtt8uhugdjocsd7d9twhdf0` (`time_slot_id`),
  CONSTRAINT `FK_ebhtt8uhugdjocsd7d9twhdf0` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slot` (`id`),
  CONSTRAINT `FK_o3eqsfq8ghehmn4jti71x4xjc` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_result`
--

LOCK TABLES `survey_result` WRITE;
/*!40000 ALTER TABLE `survey_result` DISABLE KEYS */;
INSERT INTO `survey_result` VALUES (1,36,9),(2,1,41);
/*!40000 ALTER TABLE `survey_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_slot`
--

DROP TABLE IF EXISTS `time_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `time_slot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `begin` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `score` int(11) NOT NULL,
  `date_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oaaaor66j787u9gu49p91x9ee` (`date_id`),
  CONSTRAINT `FK_oaaaor66j787u9gu49p91x9ee` FOREIGN KEY (`date_id`) REFERENCES `custom_date` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_slot`
--

LOCK TABLES `time_slot` WRITE;
/*!40000 ALTER TABLE `time_slot` DISABLE KEYS */;
INSERT INTO `time_slot` VALUES (1,_binary '','09:00:00','10:00:00',1,1),(2,_binary '','08:00:00','10:00:00',0,1),(3,_binary '','08:00:00','09:00:00',0,1),(4,_binary '','10:00:00','12:00:00',1,1),(5,_binary '','11:00:00','13:00:00',0,1),(6,_binary '','12:00:00','14:00:00',0,1),(7,_binary '','13:00:00','15:00:00',1,2),(8,_binary '','11:00:00','14:00:00',0,2),(9,_binary '','11:00:00','12:00:00',1,2),(10,_binary '','09:00:00','10:00:00',0,2),(11,_binary '','08:00:00','09:00:00',0,3),(12,_binary '','08:00:00','09:00:00',0,4),(13,_binary '','08:00:00','09:00:00',0,5),(14,_binary '','08:00:00','09:00:00',0,6),(15,_binary '','08:00:00','09:00:00',0,7),(16,_binary '','08:00:00','10:00:00',0,7),(17,_binary '','08:00:00','11:00:00',0,7),(18,_binary '','09:30:00','10:30:00',0,3),(19,_binary '','10:30:00','11:45:00',0,3),(20,_binary '','11:00:00','11:45:00',0,3),(21,_binary '','12:00:00','14:00:00',0,3),(22,_binary '','13:00:00','13:45:00',0,3),(23,_binary '','15:00:00','16:00:00',0,3),(24,_binary '','15:00:00','18:00:00',0,3),(25,_binary '','17:00:00','18:00:00',0,3),(26,_binary '','17:20:00','18:00:00',0,4),(27,_binary '','08:00:00','08:45:00',0,4),(28,_binary '','08:00:00','09:45:00',0,4),(29,_binary '','10:00:00','11:30:00',0,4),(30,_binary '','14:00:00','15:00:00',0,4),(31,_binary '','12:00:00','13:00:00',0,4),(32,_binary '','12:30:00','13:30:00',0,4),(33,_binary '','08:00:00','09:45:00',0,5),(34,_binary '','09:00:00','09:45:00',0,5),(35,_binary '','09:30:00','09:45:00',0,5),(36,_binary '','09:30:00','10:45:00',0,5),(37,_binary '','10:30:00','11:45:00',0,5),(38,_binary '','11:30:00','13:00:00',0,5),(39,_binary '','14:30:00','15:00:00',0,5),(40,_binary '','16:30:00','17:30:00',0,5),(41,_binary '','14:30:00','15:45:00',1,6),(42,_binary '','14:00:00','15:45:00',0,6),(43,_binary '','14:00:00','15:00:00',0,6),(44,_binary '','08:00:00','10:00:00',1,6),(45,_binary '','08:00:00','09:45:00',0,6),(46,_binary '','08:00:00','09:45:00',0,7),(47,_binary '','12:00:00','13:45:00',0,7),(48,_binary '','12:00:00','13:30:00',0,7),(49,_binary '','12:00:00','13:00:00',0,7),(50,_binary '','08:00:00','10:00:00',0,8),(51,_binary '','08:30:00','10:00:00',1,8),(52,_binary '','12:30:00','13:00:00',0,8),(53,_binary '','08:00:00','10:00:00',0,9),(54,_binary '','11:00:00','12:45:00',0,9),(55,_binary '','13:00:00','14:45:00',0,9),(56,_binary '','13:00:00','14:00:00',0,9),(57,_binary '','08:30:00','09:00:00',0,10),(58,_binary '','08:30:00','10:45:00',0,10);
/*!40000 ALTER TABLE `time_slot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'ibrahima.haidara@etudiant.univ-rennes1.fr','admin','admin','$2a$11$n4Erfn13Nxv20NafAnrJge9qs6L5ZUKpp6ryuOwlyDAK6ilzE7MGC'),(12,'haidara99@gmail.com','creator','creator','$2a$11$0pjEx3jyynLN7rexvnUfG.SMYR4Ho8S3X80fm3TrhRJcvF1DaneTm'),(14,'part@part.com','participant','participant','$2a$11$LeVs0rXTcUzcxV7Qy9U5deBzx5WmOF/8wH/3dFtglH.TvACcywMke');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_meeting_absents`
--

DROP TABLE IF EXISTS `user_meeting_absents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_meeting_absents` (
  `user_id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  KEY `FK_d94o08lu2rgxweh3eykji4wnw` (`meeting_id`),
  KEY `FK_8yitv49s0wxirpoon16s6ac2f` (`user_id`),
  CONSTRAINT `FK_8yitv49s0wxirpoon16s6ac2f` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_d94o08lu2rgxweh3eykji4wnw` FOREIGN KEY (`meeting_id`) REFERENCES `meeting` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_meeting_absents`
--

LOCK TABLES `user_meeting_absents` WRITE;
/*!40000 ALTER TABLE `user_meeting_absents` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_meeting_absents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_meeting_presents`
--

DROP TABLE IF EXISTS `user_meeting_presents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_meeting_presents` (
  `user_id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  KEY `FK_377y343qwiyont1yfcceop4tu` (`meeting_id`),
  KEY `FK_6llwc4yp8e1eeeudqapmvb6h7` (`user_id`),
  CONSTRAINT `FK_377y343qwiyont1yfcceop4tu` FOREIGN KEY (`meeting_id`) REFERENCES `meeting` (`id`),
  CONSTRAINT `FK_6llwc4yp8e1eeeudqapmvb6h7` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_meeting_presents`
--

LOCK TABLES `user_meeting_presents` WRITE;
/*!40000 ALTER TABLE `user_meeting_presents` DISABLE KEYS */;
INSERT INTO `user_meeting_presents` VALUES (2,25),(2,1);
/*!40000 ALTER TABLE `user_meeting_presents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `FK_it77eq964jhfqtu54081ebtio` (`role_id`),
  KEY `FK_apcc8lxk2xnug8377fatvbn04` (`user_id`),
  CONSTRAINT `FK_apcc8lxk2xnug8377fatvbn04` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_it77eq964jhfqtu54081ebtio` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (2,3),(12,2),(14,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-13  5:35:06

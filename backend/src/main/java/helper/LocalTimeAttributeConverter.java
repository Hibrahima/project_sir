package helper;

import java.sql.Time;
import java.time.LocalTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply=true)
public class LocalTimeAttributeConverter implements AttributeConverter<LocalTime, Time> {

	@Override
	public Time convertToDatabaseColumn(LocalTime entityTime) {
		return entityTime == null ? null : Time.valueOf(entityTime);
	}

	@Override
	public LocalTime convertToEntityAttribute(Time dbTime) {
		return dbTime == null ? null : dbTime.toLocalTime();
	}

}

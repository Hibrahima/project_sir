package helper;

import org.json.JSONArray;
import org.json.JSONObject;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Emailv31;


public class MailSender {

	public static void sendEmail(String toEmail, String subject, String txtMessage)
			throws MailjetException, MailjetSocketTimeoutException {

		MailjetRequest email;
		MailjetResponse response;

		// Note how we set the version to v3.1 using ClientOptions
		String apiPublicKey = "efcd2f36255534308413521fdbfbf307";
		String apiPrivateKey = "9a3927d217e4c69ae21d7679c301978d";
		MailjetClient client = new MailjetClient(apiPublicKey, apiPrivateKey, new ClientOptions("v3.1"));

		JSONObject message = new JSONObject();
		message.put(Emailv31.Message.FROM, new JSONObject()
		.put(Emailv31.Message.EMAIL, "haidara99@gmail.com"))
		.put(Emailv31.Message.SUBJECT, subject)
		.put(Emailv31.Message.HTMLPART, txtMessage)
		.put(Emailv31.Message.TO, new JSONArray()
		.put(new JSONObject().put(Emailv31.Message.EMAIL, toEmail)));

		email = new MailjetRequest(Emailv31.resource).property(Emailv31.MESSAGES, (new JSONArray()).put(message));

		response = client.post(email);
		System.out.println(response.getStatus());
	}

	public static void main(String[] args) {
		MailSender s = new MailSender();
		try {
			MailSender.sendEmail("mahamadsylla5@gmail.com", "", "");
		} catch (MailjetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MailjetSocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

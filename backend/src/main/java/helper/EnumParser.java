package helper;

import enums.SurveyStatus;

public class EnumParser {
	
	public static SurveyStatus getEnum(String enumValue) {
		for(SurveyStatus s : SurveyStatus.values()) {
			if(s.getStatusName().equals(enumValue))
				return s;
		}
		return null;
	}

}

package helper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import utils.StaticResourceProvider;

public class DateParser {
	
	public static LocalDate parseStringToDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StaticResourceProvider.dateFormat);
		LocalDate localDate = LocalDate.parse(date, formatter);
		return localDate;
	}

}

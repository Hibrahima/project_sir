package helper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import utils.StaticResourceProvider;

public class CustomDateDeserializer extends JsonDeserializer<LocalDate>{
	
	private SimpleDateFormat formatter  = new SimpleDateFormat(StaticResourceProvider.dateFormat);

	@Override
	public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		String date = jp.getText();
        try {
            return new java.sql.Date(formatter.parse(date).getTime() ).toLocalDate();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
	}

}

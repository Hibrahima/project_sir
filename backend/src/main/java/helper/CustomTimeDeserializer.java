package helper;

import java.io.IOException;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import utils.StaticResourceProvider;

public class CustomTimeDeserializer extends JsonDeserializer<LocalTime> {

    @Override
    public LocalTime deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        String time = jp.getText();
        try {
            return LocalTime.parse(time,DateTimeFormatter.ofPattern(StaticResourceProvider.timeFormat));
        } catch (DateTimeParseException e) {
            throw new RuntimeException(e);
        }
    }
    
}
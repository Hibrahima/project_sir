package helper;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply=true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {

	@Override
	public Date convertToDatabaseColumn(LocalDate entityDate) {
		return entityDate == null ? null : Date.valueOf(entityDate);
	}

	@Override
	public LocalDate convertToEntityAttribute(Date dbDate) {
		return dbDate == null ? null : dbDate.toLocalDate();
	}

}

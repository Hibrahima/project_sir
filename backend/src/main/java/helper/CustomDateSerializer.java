package helper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import utils.StaticResourceProvider;

public class CustomDateSerializer extends JsonSerializer<LocalDate>{
	
	private SimpleDateFormat formatter  = new SimpleDateFormat(StaticResourceProvider.dateFormat);

	@Override
	public void serialize(LocalDate value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		jgen.writeString(formatter.format(value)); 
		
	}

}

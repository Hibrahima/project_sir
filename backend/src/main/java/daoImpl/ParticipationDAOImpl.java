package daoImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import dao.ParticipationDAO;
import dao.SurveyDAO;
import dao.UserDAO;
import dto.ParticipationDTO;
import dto.TimeSlotDTO;
import dto.UserP;
import enums.SurveyStatus;
import exceptions.OperationNotAllowedException;
import helper.DateParser;
import helper.EntityManagerHelper;
import models.Participation;
import models.ParticipationCompoundKey;
import models.Survey;
import models.TimeSlot;
import models.User;

public class ParticipationDAOImpl implements ParticipationDAO {

	private EntityManager manager;
	private UserDAO userDAO;
	private SurveyDAO surveyDAO;

	public ParticipationDAOImpl() {
		this.manager = EntityManagerHelper.getEntityManager();
		this.userDAO = new UserDAOImpl();
		this.surveyDAO = new SurveyDAOImpl();
	}

	public Participation update(Participation p) throws IOException {
		Objects.requireNonNull(p, "participation cannot be null");
		EntityManagerHelper.beginTransaction();
		Participation dbParticipation = this.getById(p.getId().getUserId(), p.getId().getSurveyId());
		if (dbParticipation == null)
			throw new EntityNotFoundException();

		p.setTimeSlots(dbParticipation.getTimeSlots());
		Participation result = this.manager.merge(p);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return result;

	}

	public Participation getById(int userId, int surveyId) {
		ParticipationCompoundKey key = new ParticipationCompoundKey(userId, surveyId);
		return this.manager.find(Participation.class, key);
	}

	public List<ParticipationDTO> getAll() {
		TypedQuery<Participation> query = this.manager.createNamedQuery("Participatiom.findAll", Participation.class);
		List<Participation> results = query.getResultList();
		List<ParticipationDTO> toReturn = new ArrayList<>();
		for (Participation p : results) {
			ParticipationDTO ps = new ParticipationDTO(p.getId(), p.getParticipationDate(), p.getParticipationTime());
			for (TimeSlot t : p.getTimeSlots()) {
				TimeSlotDTO ts = new TimeSlotDTO(t.getId(), t.getBegin(), t.getEnd(), t.getScore(), t.isActive(),
						t.getDate().getDate());
				ps.getTimeSlots().add(ts);
			}
			toReturn.add(ps);
		}
		return toReturn;
	}

	public List<Participation> findAllOrderedByScore() {
		TypedQuery<Participation> query = this.manager.createNamedQuery("Participation.findAllOrderedByScore",
				Participation.class);
		List<Participation> results = query.getResultList();
		return results;
	}

	public List<Participation> findByDate(String date) {
		TypedQuery<Participation> query = this.manager.createNamedQuery("Participation.findByDate",
				Participation.class);
		query.setParameter("date", DateParser.parseStringToDate(date));
		List<Participation> results = query.getResultList();
		return results;
	}

	@Override
	public Participation create(Participation p) throws IOException, OperationNotAllowedException {
		Objects.requireNonNull(p, "participation cannot be null");
		User user = this.userDAO.findUserById(p.getId().getUserId());
		Survey survey = this.surveyDAO.getById(p.getId().getSurveyId());
		if (user == null || survey == null)
			throw new EntityNotFoundException("user with id " + p.getId().getUserId() + " or survey with id "
					+ p.getId().getSurveyId() + " does not exist");

		EntityManagerHelper.beginTransaction();
		// very important - to use the utility method and increment the score
		TimeSlot timeDb = null;
		for (TimeSlot t : p.getTimeSlots()) {
			timeDb = this.manager.find(TimeSlot.class, t.getId());
			if (timeDb == null)
				throw new EntityNotFoundException("Cannot find time slot with id " + t.getId());
			// if(!t.isActive())
			// throw new OperationNotAllowedException("Inactive time slot cannot be added to
			// any participation");

			t.incrementScore();
			t.setDate(timeDb.getDate()); // keeps data integrity
			this.manager.merge(t); // merge the new state of each t
		}

		survey.setStatus(SurveyStatus.PENDING); // makes this survey status as pending
		this.manager.merge(survey);
		this.manager.merge(p);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return p;
	}

	@Override
	public UserP didUserAlreadyParticipate(int userId, int surveyId) {
		try {
			TypedQuery<Participation> query = this.manager.createQuery(
					"select p from Participation p where p.id.userId =:userId and p.id.surveyId =:surveyId",
					Participation.class);
			query.setParameter("userId", userId);
			query.setParameter("surveyId", surveyId);
			if (query.getSingleResult() != null)
				return new UserP(true);
			
			return new UserP(false);
		} catch (Exception e) {
			return new UserP(false);
		}
	}

}

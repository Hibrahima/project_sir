package daoImpl;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import dao.MeetingDAO;
import helper.DateParser;
import helper.EntityManagerHelper;
import models.Meeting;
import models.User;
import utils.Utils;

public class MeetingDAOImpl implements MeetingDAO {
	
	private EntityManager manager;
	
	public MeetingDAOImpl() {
		this.manager = EntityManagerHelper.getEntityManager();	
	}

	@Override
	public Meeting update(Meeting m) throws IOException {
		Objects.requireNonNull(m, "meeting cannot be null");
		EntityManagerHelper.beginTransaction();
		Meeting dbMeeting = this.getById(m.getId());
		if( dbMeeting == null)
			throw new EntityNotFoundException();
		
		//m.setId(id);
		m.setAbsents(dbMeeting.getAbsents());
		m.setPresents(dbMeeting.getPresents());
		m.setSurvey(dbMeeting.getSurvey());
		Meeting result = this.manager.merge(m);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return result;
	}

	@Override
	public Meeting getById(int id) {
		return this.manager.find(Meeting.class, id);
	}

	@Override
	public List<Meeting> getAll() {
		TypedQuery<Meeting> query = this.manager.createNamedQuery("Meeting.findAll", Meeting.class);
		return query.getResultList();
	}

	@Override
	public List<Meeting> findByDate(String date) {
		TypedQuery<Meeting> query = this.manager.createNamedQuery("Meeting.findByDate", Meeting.class);
		query.setParameter("date", DateParser.parseStringToDate(date));
		return query.getResultList();
	}

	@Override
	public Meeting findByTitle(String title) {
		TypedQuery<Meeting> query = this.manager.createNamedQuery("Meeting.findByTitle", Meeting.class);
		query.setParameter("title", title);
		return query.getSingleResult();
	}

	@Override
	public List<Meeting> findByOfficeAddress(String address) {
		TypedQuery<Meeting> query = this.manager.createNamedQuery("Meeting.findByOfficeAddress", Meeting.class);
		query.setParameter("address", address);
		return query.getResultList();
	}

	@Override
	public List<Meeting> findByTimeSlot(String timeSlot) {
		TypedQuery<Meeting> query = this.manager.createNamedQuery("Meeting.findByTimeSlot", Meeting.class);
		query.setParameter("slot", timeSlot);
		return query.getResultList();
	}

	@Override
	public List<User> findAbsents(int meetingId) {
		String query = "select m.absents from Meeting m where m.id = :id";
		Query q = this.manager.createQuery(query);
		q.setParameter("id", meetingId);
		Collection<User> absents = q.getResultList();
		return Utils.cloneCollection(absents);
	}

	@Override
	public List<User> findPresents(int meetingId) {
		String query = "select m.presents from Meeting m where m.id = :id";
		Query q = this.manager.createQuery(query);
		q.setParameter("id", meetingId);
		Collection<User> presents = q.getResultList();
		return Utils.cloneCollection(presents); 
	}

}

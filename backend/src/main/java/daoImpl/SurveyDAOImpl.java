package daoImpl;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import org.apache.commons.mail.EmailException;
import dao.CustomDateDAO;
import dao.MeetingDAO;
import dao.SurveyDAO;
import dao.TimeSlotDAO;
import dao.UserDAO;
import dto.ParticipationDTO;
import dto.TimeSlotDTO;
import enums.SurveyStatus;
import exceptions.ExistingObjectException;
import exceptions.OperationNotAllowedException;
import helper.DateParser;
import helper.EntityManagerHelper;
import helper.EnumParser;
import helper.MailSender;
import models.CustomDate;
import models.Participation;
import models.Survey;
import models.SurveyResult;
import models.TimeSlot;
import models.User;
import utils.StaticResourceProvider;
import utils.SurveyMeetingPostObject;
import utils.SurveyValidationData;

public class SurveyDAOImpl implements SurveyDAO {

	private EntityManager manager;
	private CustomDateDAO dateDAO;
	private TimeSlotDAO timeDAO;
	//private UserDAO userDAO;
	private MeetingDAO meetingDAO;

	public SurveyDAOImpl() {
		this.manager = EntityManagerHelper.getEntityManager();
		this.dateDAO = new CustomDateDAOImpl();
		this.timeDAO = new TimeSlotDAOImpl();
		//this.userDAO = new UserDAOImpl();
		this.meetingDAO = new MeetingDAOImpl();
	}

	@Override
	public Survey update(SurveyMeetingPostObject s) throws IOException {
		Objects.requireNonNull(s, "survey meeting object cannot be null");
		EntityManagerHelper.beginTransaction();
		Survey dbSurvey = this.manager.find(Survey.class, s.getSurveyId());
		if (dbSurvey == null || dbSurvey.getStatus().equals(SurveyStatus.READY))
			throw new IllegalArgumentException("You cannot update a survey that  has been marked as ready");

		dbSurvey.getMeeting().setTitle(s.getMeetingTitle());
		dbSurvey.setName(s.getSurveyName());
		dbSurvey.getMeeting().setOfficeAddress(s.getMeetingOfficeAddress());
		dbSurvey.getMeeting().setSummary(s.getMeetingSummary());

		Survey result = this.manager.merge(dbSurvey);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return result;

	}

	@Override
	public List<Survey> getAll() {
		TypedQuery<Survey> query = this.manager.createNamedQuery("Survey.findAll", Survey.class);
		List<Survey> results = query.getResultList();
		return results;
	}

	@Override
	public List<Survey> getUserSurveys(int userId) {
		Objects.requireNonNull(userId, "user id cannot be null");
		User user = this.manager.find(User.class, userId);
		return user.getCreatedSurveys();
	}

	@Override
	public Survey getById(int id) {
		Objects.requireNonNull(id, "id id cannot be null");
		return this.manager.find(Survey.class, id);
	}

	@Override
	public CustomDate addDate(int surveyId, int dateId)
			throws ExistingObjectException, IOException, OperationNotAllowedException {
		Objects.requireNonNull(surveyId, "survey id cannot be null");
		Objects.requireNonNull(dateId, "date id cannot be null");
		EntityManagerHelper.beginTransaction();
		Survey survey = this.getById(surveyId);
		CustomDate date = this.dateDAO.getById(dateId);
		if (survey == null || date == null)
			throw new EntityNotFoundException();

		survey.addDate(date);
		this.manager.persist(survey);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return date;
	}

	@Override
	public CustomDate removeDate(int surveyId, int dateId) throws IllegalArgumentException, IOException {
		Objects.requireNonNull(surveyId, "survey id cannot be null");
		Objects.requireNonNull(dateId, "date id cannot be null");
		EntityManagerHelper.beginTransaction();
		Survey survey = this.getById(surveyId);
		CustomDate date = this.dateDAO.getById(dateId);
		if (survey == null || date == null)
			throw new EntityNotFoundException();

		survey.removeDate(date);
		this.manager.persist(survey);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return date;
	}

	@Override
	public List<Survey> findByCreationDate(String date) {
		Objects.requireNonNull(date, "date id cannot be null");
		TypedQuery<Survey> query = this.manager.createNamedQuery("Survey.findByCreationDate", Survey.class);
		query.setParameter("date", DateParser.parseStringToDate(date));
		return query.getResultList();
	}

	@Override
	public List<Survey> findByCustomDate(String date) {
		Objects.requireNonNull(date, "date id cannot be null");
		TypedQuery<Survey> query = this.manager.createNamedQuery("Survey.findByCustomDate", Survey.class);
		query.setParameter("date", DateParser.parseStringToDate(date));
		return query.getResultList();
	}

	@Override
	public List<Survey> findByCustomDateAndTimeSlot(String date, int begin, int end) {
		Objects.requireNonNull(date, "date id cannot be null");
		Objects.requireNonNull(begin, "begin id cannot be null");
		Objects.requireNonNull(end, "end id cannot be null");
		TypedQuery<Survey> query = this.manager.createNamedQuery("Survey.findByCustomDateAndTimeSlot", Survey.class);
		query.setParameter("date", DateParser.parseStringToDate(date));
		query.setParameter("begin", begin);
		query.setParameter("end", end);
		return query.getResultList();
	}

	@Override
	public List<Survey> findByStatus(String status) {
		Objects.requireNonNull(status, "status id cannot be null");
		TypedQuery<Survey> query = this.manager.createNamedQuery("Survey.findByStatus", Survey.class);
		query.setParameter("status", EnumParser.getEnum(status));
		return query.getResultList();
	}

	@Override
	public List<ParticipationDTO> getSurveyParticipations(int surveyId) {
		Objects.requireNonNull(surveyId, "survey id cannot be null");
		TypedQuery<Participation> query = this.manager.createNamedQuery("Survey.findAllParticipations",
				Participation.class);
		query.setParameter("surveyId", surveyId);
		List<ParticipationDTO> toReturn = new ArrayList<>();
		for (Participation p : query.getResultList()) {
			ParticipationDTO ps = new ParticipationDTO(p.getId(), p.getParticipationDate(), p.getParticipationTime());
			for (TimeSlot t : p.getTimeSlots()) {
				TimeSlotDTO ts = new TimeSlotDTO(t.getId(), t.getBegin(), t.getEnd(), t.getScore(), t.isActive(),
						t.getDate().getDate());
				ps.getTimeSlots().add(ts);
			}
			toReturn.add(ps);
		}

		return toReturn;
	}

	@Override
	public List<User> getParticipants(int surveyId) {
		Objects.requireNonNull(surveyId, "survey id cannot be null");
		TypedQuery<User> query = this.manager.createNamedQuery("Survey.findParticipants", User.class);
		query.setParameter("surveyId", surveyId);
		return query.getResultList();
	}

	@Override
	public Survey validate(SurveyValidationData data)
			throws ExistingObjectException, IOException, OperationNotAllowedException, EmailException {
		Objects.requireNonNull(data.getSurveyId(), "survey id cannot be null");
		Objects.requireNonNull(data.getTimeSlotId(), "time slot id cannot be null");
		Survey s = this.getById(data.getSurveyId());
		TimeSlot t = this.timeDAO.getById(data.getTimeSlotId());
		if (s == null || t == null)
			throw new EntityNotFoundException("Cannot find survey with id " + data.getSurveyId()
					+ " or time slot with id " + data.getTimeSlotId());

		if (s.getStatus() == SurveyStatus.VALIDATED)
			throw new OperationNotAllowedException("This survey has been already validated . Cannot revalidate again!");

		// if(s.getStatus() != SurveyStatus.PENDING)
		// throw new OperationNotAllowedException("Cannot validate that is not marked as
		// pending - nobody took this survey with id "+data.getSurveyId());

		SurveyResult result = new SurveyResult();
		result.setTimeSlot(t);
		s.setFinalResult(result); // set the final result of the survey - should persist the results as well
		s.getMeeting().setTimeSlot("" + t.getBegin() + " - " + t.getEnd());
		s.getMeeting().setDate(t.getDate().getDate());
		s.setStatus(SurveyStatus.VALIDATED);
		EntityManagerHelper.beginTransaction();
		this.manager.persist(s);

		addAttendedMeeting(s); // then persist the presents and absents
		addMissedMeeting(s);
		EntityManagerHelper.commit();

		// send mail for preferences - to do
		//if (t.getBegin().getHour() >= 12 && t.getBegin().getHour() <= 14) {
			try {
				this.sendEmail(s.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}
		//}
			EntityManagerHelper.closeEntityManager();

		return s;
	}

	private void addAttendedMeeting(Survey survey) throws ExistingObjectException {

		List<User> usersThatAttendedMeeting = this.manager.createNamedQuery("User.findUserMemberOfMeeting", User.class)
				.setParameter("surveyId", survey.getId())
				.setParameter("timeId", survey.getFinalResult().getTimeSlot().getId()).getResultList();
		for (User u : usersThatAttendedMeeting) {
			u.addAttendedMeeting(survey.getMeeting());
			this.manager.merge(u);
		}
	}

	private void addMissedMeeting(Survey survey) throws ExistingObjectException {
		List<User> usersThatMissedMeeting = this.manager.createNamedQuery("User.findUserNotMemberOfMeeting", User.class)
				.setParameter("surveyId", survey.getId())
				.setParameter("timeId", survey.getFinalResult().getTimeSlot().getId()).getResultList();

		for (User u : usersThatMissedMeeting) {
			u.addMissedMeeting(survey.getMeeting());
			this.manager.merge(u);
		}
	}

	@Override
	public List<TimeSlot> getAllTimeSlotsByParticipation(int surveyId) {
		Objects.requireNonNull(surveyId, "survey id cannot be null");
		TypedQuery<TimeSlot> query = this.manager.createNamedQuery("Survey.findAllTimeSlotsByParticipation",
				TimeSlot.class);
		query.setParameter("surveyId", surveyId);
		return query.getResultList();
	}

	@Override
	public List<TimeSlot> getAllTimeSlotsByParticipationOderedByScore(int surveyId) {
		Objects.requireNonNull(surveyId, "survey id cannot be null");
		TypedQuery<TimeSlot> query = this.manager
				.createNamedQuery("Survey.findAllTimeSlotsByParticipationOderedByScore", TimeSlot.class);
		query.setParameter("surveyId", surveyId);
		return query.getResultList();
	}

	@Override
	public User getSurveyCreator(int surveyId) {
		Survey s = this.getById(surveyId);
		return s.getCreatedBy();
	}

	@Override
	public SurveyResult getResult(int surveyId) {
		TypedQuery<SurveyResult> query = this.manager.createNamedQuery("Survey.findResult", SurveyResult.class);
		query.setParameter("surveyId", surveyId);
		return query.getSingleResult();

	}

	@Override
	// to do
	public void sendEmail(int surveyId) throws Exception {
		Survey s = this.getById(surveyId);
		List<User> presents = this.meetingDAO.findPresents(s.getMeeting().getId());
		String subject = "Validation de la date et de l'heure du sondage " + s.getName();
		LocalDate localDate = s.getFinalResult().getTimeSlot().getDate().getDate();
		String date = localDate.getDayOfWeek() + " " + localDate.getDayOfMonth() + "-" + localDate.getMonthValue() + "-"
				+ localDate.getYear() + " à " + s.getMeeting().getTimeSlot();
		String txtMessage = "Bonjour, ce sondage a été validé pour <strong>" + date + "</strong> <br>";
		String link = "<a href='" + StaticResourceProvider.DEFAULT_SERVER_PORT + "/users/meetings/" + s.getMeeting().getId() + "/allergies-preferences" + "'> Renseigner Allergies et Préférences Alimentaites </a>"
				;
		txtMessage += "Vous devez rensigner vos allergies et preferences alimentaires en cliquant sur ce lien : "
				+ link;
		try {
			for (User u : presents) {
				MailSender.sendEmail(u.getEmail(), subject, txtMessage);
			}
		} catch (Exception e) {
			throw new Exception("Cannot send mail to presents");
		}
	}

}

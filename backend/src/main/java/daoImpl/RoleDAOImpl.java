package daoImpl;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import dao.RoleDAO;
import helper.EntityManagerHelper;
import models.Role;
import utils.DeepCopyMaker;

public class RoleDAOImpl implements RoleDAO {

	private EntityManager manager;

	public RoleDAOImpl() {
		this.manager = EntityManagerHelper.getEntityManager();
	}

	public Role getRoleById(int id) {
		return this.manager.find(Role.class, id); 
	}

	public Role createRole(Role role) throws IOException {
		EntityManagerHelper.beginTransaction();
		this.manager.persist(role);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return DeepCopyMaker.deepCopy(role);
	}

	public List<Role> getAllRole() {
		TypedQuery<Role> query = this.manager.createQuery("SELECT r FROM Role r", Role.class);
		List<Role> results = query.getResultList();
		return results;

	}

	public Role updateRole(Role role) throws IOException {
		Objects.requireNonNull(role, "role cannot be null");
		EntityManagerHelper.beginTransaction();
		//role.setId(id);
		Role result = this.manager.merge(role);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return result;
	}

	public Role deleteRole(int id) throws IOException {
		Objects.requireNonNull(id, "role id cannot be null");
		EntityManagerHelper.beginTransaction();
		Role role = this.getRoleById(id);
		this.manager.remove(role);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return DeepCopyMaker.deepCopy(role);
	}

	@Override
	public Role findByName(String name) {
		try {
			TypedQuery<Role> query = this.manager.createNamedQuery("Role.findByName", Role.class);
			query.setParameter("roleName", name);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}	
	}
}

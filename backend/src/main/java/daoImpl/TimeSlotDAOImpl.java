package daoImpl;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import dao.TimeSlotDAO;
import exceptions.OperationNotAllowedException;
import helper.EntityManagerHelper;
import models.TimeSlot;

public class TimeSlotDAOImpl implements TimeSlotDAO {
	
	private EntityManager manager;
	
	public TimeSlotDAOImpl() {
		this.manager = EntityManagerHelper.getEntityManager(); 
	}

	@Override
	public TimeSlot create(TimeSlot t) throws IOException, OperationNotAllowedException {
		Objects.requireNonNull(t, "time slot cannot be null");
		if(t.getEnd().isBefore(t.getBegin()))
			throw new OperationNotAllowedException("end must be equal or greater than begin");
		
		EntityManagerHelper.beginTransaction();
		this.manager.persist(t);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return t;
	}

	@Override
	public TimeSlot getById(int id) throws IOException {
		Objects.requireNonNull(id, "id cannot be null");
		TimeSlot t = this.manager.find(TimeSlot.class, id);
		return t;
	}

	@Override
	public TimeSlot update(TimeSlot t) throws IOException, OperationNotAllowedException {
		Objects.requireNonNull(t, "custom date cannot be null");
		EntityManagerHelper.beginTransaction();
		TimeSlot dbTime = this.manager.find(TimeSlot.class, t.getId());
		if(dbTime == null)
			throw new EntityNotFoundException();
		if(t.getEnd().isBefore(t.getBegin()))
			throw new OperationNotAllowedException("end must be equal or greater than begin");
		
		//t.setId(id);
		dbTime.setBegin(t.getBegin());
		dbTime.setEnd(t.getEnd());
		TimeSlot result = this.manager.merge(dbTime);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return result;
	}

	@Override
	public TimeSlot delete(int id) throws IOException, OperationNotAllowedException {
		Objects.requireNonNull(id, "id cannot be null");
		EntityManagerHelper.beginTransaction();
		TimeSlot t = this.manager.find(TimeSlot.class, id);
		if(t == null)
			throw new EntityNotFoundException("Cannot find time slot with id " + id);
		
		if(!t.isActive())
			throw new OperationNotAllowedException("Cannot delete a time slot that is inactive");
		
		this.manager.remove(t);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return t;
	}

	@Override
	public List<TimeSlot> getAll() {
		TypedQuery<TimeSlot> query = this.manager.createNamedQuery("TimeSlot.findAll", TimeSlot.class);
		for(TimeSlot t: query.getResultList())
			System.out.println("begin : " + t.getBegin());
		return query.getResultList();
	}

	@Override
	public List<TimeSlot> getAllByState(boolean active) {
		TypedQuery<TimeSlot> query = this.manager.createNamedQuery("TimeSlot.findByState", TimeSlot.class);
		query.setParameter("state", active);
		return query.getResultList();
	}

	@Override
	public List<TimeSlot> getAllOrderedByScore() {
		TypedQuery<TimeSlot> query = this.manager.createNamedQuery("TimeSlot.findAllOrderedByScore", TimeSlot.class);
		return query.getResultList();
	}

}

package daoImpl;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import dao.CustomDateDAO;
import dao.TimeSlotDAO;
import exceptions.ExistingObjectException;
import exceptions.OperationNotAllowedException;
import helper.DateParser;
import helper.EntityManagerHelper;
import models.CustomDate;
import models.TimeSlot;

public class CustomDateDAOImpl implements CustomDateDAO {

	private EntityManager manager;
	private TimeSlotDAO timeDao;

	public CustomDateDAOImpl() {
		this.manager = EntityManagerHelper.getEntityManager();
		this.timeDao = new TimeSlotDAOImpl(); 
	}

	@Override
	public CustomDate create(CustomDate date) throws IOException, OperationNotAllowedException {
		Objects.requireNonNull(date, "date cannot be null");
		EntityManagerHelper.beginTransaction();
		/*for(TimeSlot t: date.getTimeSlots()){
			date.addTimeSlotI(t);
		}*/
		this.manager.persist(date);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return date;
	}

	@Override
	public CustomDate getById(int id) throws IOException {
		Objects.requireNonNull(id, "id cannot be null");
		return this.manager.find(CustomDate.class, id);
	}

	@Override
	public CustomDate update(CustomDate d) throws IOException {
		Objects.requireNonNull(d, "date cannot be null");
		EntityManagerHelper.beginTransaction();
		CustomDate dbDate = this.manager.find(CustomDate.class, d.getId());
		if ( dbDate == null)
			throw new EntityNotFoundException("cannot find date with id " + d.getId());

		//d.setId(id);
		d.setTimeSlots(dbDate.getTimeSlots());
		CustomDate result = this.manager.merge(d);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return result;
	}

	@Override
	public CustomDate delete(int id) throws IOException {
		Objects.requireNonNull(id, "id cannot be null");
		EntityManagerHelper.beginTransaction();
		CustomDate date = this.getById(id);
		if (date == null)
			throw new EntityNotFoundException();
		
		this.manager.remove(date);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return date;
	}

	@Override
	public TimeSlot addPredefinedTimeSlot(int dateId, int timeId)
			throws ExistingObjectException, IOException, OperationNotAllowedException {
		Objects.requireNonNull(dateId, "date id cannot be null");
		Objects.requireNonNull(timeId, "time id cannot be null");
		EntityManagerHelper.beginTransaction();
		CustomDate date = this.getById(dateId);
		TimeSlot time = this.timeDao.getById(timeId);
		if (date == null || time == null)
			throw new EntityNotFoundException();

		date.addTimeSlot(time);
		this.manager.merge(date);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return time;

	}

	@Override
	public List<CustomDate> getAll() {
		TypedQuery<CustomDate> query = this.manager.createNamedQuery("CustomDate.findAll", CustomDate.class);
		List<CustomDate> results = query.getResultList();
		return results;
	}

	@Override
	public List<CustomDate> findByName(String name) {
		TypedQuery<CustomDate> query = this.manager.createNamedQuery("CustomDate.findByName", CustomDate.class);
		query.setParameter("name", name);
		List<CustomDate> results = query.getResultList();
		return results;
	}

	@Override
	public List<CustomDate> findByDate(String date) {
		TypedQuery<CustomDate> query = this.manager.createNamedQuery("CustomDate.findByDate", CustomDate.class);
		LocalDate localDate = DateParser.parseStringToDate(date);
		query.setParameter("date", localDate);
		List<CustomDate> results = query.getResultList();
		return results;
	}

	@Override
	public TimeSlot removeTimeSlot(int dateId, int timeId) throws IOException {
		Objects.requireNonNull(dateId, "date id cannot be null");
		Objects.requireNonNull(timeId, "time id cannot be null");
		EntityManagerHelper.beginTransaction();
		CustomDate date = this.getById(dateId);
		TimeSlot time = this.timeDao.getById(timeId);
		if (date == null || time == null)
			throw new EntityNotFoundException();

		
		date.removeTimeSlot(time);
		this.manager.merge(date);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return time;
	}

	@Override
	public List<CustomDate> findByStatus(boolean active) {
		TypedQuery<CustomDate> query = this.manager.createNamedQuery("CustomDate.findByStatus", CustomDate.class);
		query.setParameter("active", active);
		List<CustomDate> results = query.getResultList();
		return results;
	}

}

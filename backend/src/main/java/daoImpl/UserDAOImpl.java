package daoImpl;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import dao.RoleDAO;
import dao.SurveyDAO;
import dao.UserDAO;
import exceptions.ExistingObjectException;
import exceptions.OperationNotAllowedException;
import helper.DateParser;
import helper.EntityManagerHelper;
import models.Allergy;
import models.CustomDate;
import models.Meeting;
import models.Participation;
import models.Role;
import models.Survey;
import models.User;
import security.PasswordHasher;
import utils.LoginData;
import utils.SurveyMeetingPostObject;
import utils.Utils;

public class UserDAOImpl implements UserDAO {

	private EntityManager manager;
	private RoleDAO roleDAO;
	private SurveyDAO surveyDAO;

	public UserDAOImpl() {
		this.manager = EntityManagerHelper.getEntityManager();
		this.roleDAO = new RoleDAOImpl();
		this.surveyDAO = new SurveyDAOImpl();
	}
	

	@Override
	public User createUser(User user) throws IOException {
		EntityManagerHelper.beginTransaction();
		// hashing the password using Bcrypt
		user.setPassword(PasswordHasher.hashPassword(user.getPassword()));
		this.manager.persist(user);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return user;
	}

	@Override
	public List<User> getAll() {
		TypedQuery<User> query = this.manager.createNamedQuery("User.findAll", User.class);
		List<User> results = query.getResultList();
		return results;
	}

	@Override
	public User findUserById(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		return this.manager.find(User.class, id);
	}

	@Override
	public User updateUser(User user) throws IOException, EntityNotFoundException {
		Objects.requireNonNull(user, "user cannot be null");
		EntityManagerHelper.beginTransaction();
		User dbUser = this.manager.find(User.class, user.getId());
		if (dbUser == null)
			throw new EntityNotFoundException("cannot find user with id " + user.getId());

		dbUser.setFirstName(user.getFirstName());
		dbUser.setLastName(user.getLastName());
		dbUser.setEmail(user.getEmail());
		dbUser.setPassword(dbUser.getPassword());
		
		User result = this.manager.merge(dbUser);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();

		return result;
	}

	@Override
	public Role addNewRole(int userId, Role role) throws ExistingObjectException, IOException {
		Objects.requireNonNull(userId, "user id cannot be null");
		Objects.requireNonNull(role, "role cannot be null");
		EntityManagerHelper.beginTransaction();
		User user = this.findUserById(userId);
		if (user == null)
			throw new EntityNotFoundException();

		user.addRole(role);
		this.manager.persist(user);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();

		return role;
	}

	@Override
	public Role addPredefinedRole(int userId, int roleId) throws ExistingObjectException, IOException {
		Objects.requireNonNull(userId, "user id cannot be null");
		Objects.requireNonNull(roleId, "role id cannot be null");
		EntityManagerHelper.beginTransaction();
		Role role = this.roleDAO.getRoleById(roleId);
		User user = this.findUserById(userId);
		if (role == null || user == null)
			throw new EntityNotFoundException();

		user.addRole(role);
		this.manager.persist(user);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();

		return role;
	}

	@Override
	public Role removeRole(int userId, int roleId) throws IllegalArgumentException, IOException {
		Objects.requireNonNull(userId, "user id cannot be null");
		Objects.requireNonNull(roleId, "role id cannot be null");
		EntityManagerHelper.beginTransaction();
		Role role = this.roleDAO.getRoleById(roleId);
		User user = this.findUserById(userId);
		if (role == null || user == null)
			throw new EntityNotFoundException();

		user.removeRole(role);
		this.manager.persist(user);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();

		return role;
	}

	@Override
	public List<User> findByRoleName(String roleName) {
		Objects.requireNonNull(roleName, "role name cannot be null");
		TypedQuery<User> query = this.manager.createNamedQuery("User.findByRole", User.class);
		query.setParameter("roleName", roleName);
		return query.getResultList();
	}

	@Override
	public List<User> findByAllergyName(String allergyName) {
		Objects.requireNonNull(allergyName, "allergy name cannot be null");
		TypedQuery<User> query = this.manager.createNamedQuery("User.findByAllergy", User.class);
		query.setParameter("allergyName", allergyName);
		return query.getResultList();
	}

	@Override
	public Allergy addAllergy(int userId, Allergy a) throws ExistingObjectException, IOException {
		Objects.requireNonNull(a, "allergy cannot be null");
		User user = this.manager.find(User.class, userId);
		if (user == null)
			throw new EntityNotFoundException();

		EntityManagerHelper.beginTransaction();
		user.addAllergy(a);
		this.manager.persist(user);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();

		return a;

	}

	@Override
	public SurveyMeetingPostObject addCreatedSurvey(int userId, SurveyMeetingPostObject object)
			throws ExistingObjectException, IOException, OperationNotAllowedException {
		Objects.requireNonNull(object, "compound object cannot be null");
		
		User user = this.manager.find(User.class, userId);
		if(user == null)
			throw new EntityNotFoundException();
		
		EntityManagerHelper.beginTransaction();
		Survey s = new Survey();
		s.setName(object.getSurveyName());
		for(CustomDate d: object.getDates()){
			s.addDate(d);
		}
		s.setDates(object.getDates());
		Meeting m = new Meeting();
		m.setTitle(object.getMeetingTitle());
		m.setOfficeAddress(object.getMeetingOfficeAddress());
		//s.setMeeting(m);
		user.addSurvey(s);
		this.manager.merge(user);
		EntityManagerHelper.commit();
		
		EntityManagerHelper.beginTransaction();
		List<Survey> allSurveys = this.surveyDAO.getAll();
		int lastSurveyId = allSurveys.get(allSurveys.size()-1).getId();
		s.setMeeting(m);
		s.setLink("surveys/" + lastSurveyId + "/participate");
		s.setId(lastSurveyId);
		this.manager.merge(s);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		
		return object;
	}

	@Override
	public User registerUser(User user) throws ExistingObjectException, IOException {
		Objects.requireNonNull(user, "user cannot be null");
		EntityManagerHelper.beginTransaction();
		Role participant = this.roleDAO.findByName("participant");
		System.out.println();
		if(participant == null) {
			participant = new Role();
			participant.setName("participant");
		}
		
		user.addRole(participant);
		user.setPassword(PasswordHasher.hashPassword(user.getPassword()));
		this.manager.persist(user);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		
		return user;
	}

	@Override
	public List<Survey> findCreatedSurveysOnDate(String date, int userId) {
		Objects.requireNonNull(date, "date cannot be null");
		Objects.requireNonNull(userId, "user id cannot be null");
		TypedQuery<Survey> query = this.manager.createNamedQuery("User.findCreatedSurveysOnDate", Survey.class);
		query.setParameter("date", DateParser.parseStringToDate(date));
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@Override
	public List<Participation> findParticipationsOnDate(String date, int userId) {
		Objects.requireNonNull(date, "date cannot be null");
		Objects.requireNonNull(userId, "user id cannot be null");
		TypedQuery<Participation> query = this.manager.createNamedQuery("User.findParticipationsOnDate", Participation.class);
		query.setParameter("date", DateParser.parseStringToDate(date));
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@Override
	public List<Participation> findParticipations(int userId) {
		Objects.requireNonNull(userId, "user id cannot be null");
		TypedQuery<Participation> query = this.manager.createNamedQuery("User.findParticipations", Participation.class);
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@Override
	public User login(LoginData data) throws IOException {
		Objects.requireNonNull(data, "login data cannot be null");
		List<User> dbUsers = this.getAll();
		for(User u : dbUsers) {
			if( PasswordHasher.verifyPasswords(data.getPassword(), u.getPassword()) 
					&& data.getEmail().equals(u.getEmail()))
				return u;

		}
		return null;	
	}

	@Override
	public List<Meeting> getAttendedMettings(int userId) throws IOException {
		User user = this.findUserById(userId);
		return Utils.cloneCollection(user.getAttendedMeetings());
	}

	@Override
	public List<Meeting> getMissedMettings(int userId) throws IOException {
		User user = this.findUserById(userId);
		return Utils.cloneCollection(user.getMissedMeetings());
	}



}

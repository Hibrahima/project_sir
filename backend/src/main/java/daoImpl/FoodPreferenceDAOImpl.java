package daoImpl;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import dao.FoodPreferenceDAO;
import dao.MeetingDAO;
import dao.UserDAO;
import dto.FoodPrefAllergyDTO;
import helper.EntityManagerHelper;
import models.Allergy;
import models.FoodCompoundKey;
import models.FoodPreference;
import models.Meeting;
import models.User;


public class FoodPreferenceDAOImpl implements FoodPreferenceDAO {

	private EntityManager manager;
	private UserDAO userDAO;
	private MeetingDAO meetingDAO;

	public FoodPreferenceDAOImpl() {
		this.manager = EntityManagerHelper.getEntityManager();
		this.userDAO = new UserDAOImpl();
		this.meetingDAO = new MeetingDAOImpl();
	}

	@Override
	public FoodPreference create(FoodPreference fp) throws IOException {
		Objects.requireNonNull(fp, "preference cannot be null");
		User user = this.userDAO.findUserById(fp.getId().getUserId());
		Meeting meeting = this.meetingDAO.getById(fp.getId().getMeetingId());
		if(user == null) 
			throw new EntityNotFoundException("Cannot find user with id " + fp.getId().getUserId()); 
		
		if(meeting == null) 
			throw new EntityNotFoundException("Cannot find meeting with id " + fp.getId().getMeetingId());
		
		EntityManagerHelper.beginTransaction();
		this.manager.persist(fp);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return fp;
	}

	@Override
	public FoodPreference update(FoodPreference fp) throws IOException {
		Objects.requireNonNull(fp, "preference cannot be null");
		EntityManagerHelper.beginTransaction();
		if (this.getById(fp.getId().getUserId(), fp.getId().getMeetingId()) == null)
			throw new EntityNotFoundException();

		// FoodCompoundKey key = new FoodCompoundKey(userId, meetingId);
		// fp.setId(key);
		FoodPreference result = this.manager.merge(fp);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
		return result;
	}

	@Override
	public FoodPreference getById(int userId, int meetingId) {
		FoodCompoundKey key = new FoodCompoundKey(userId, meetingId);
		return this.manager.find(FoodPreference.class, key);
	}

	public List<FoodPreference> getAll() {
		CriteriaBuilder cb = this.manager.getCriteriaBuilder();
		CriteriaQuery<FoodPreference> query = cb.createQuery(FoodPreference.class);
		Root<FoodPreference> root = query.from(FoodPreference.class);
		query.select(root);
		return this.manager.createQuery(query).getResultList();
	}

	@Override
	public List<FoodPreference> findByUser(int userId) {
		TypedQuery<FoodPreference> typedQuery = this.manager.createNamedQuery("Preference.findByUser",
				FoodPreference.class);
		typedQuery.setParameter("userId", userId);
		return typedQuery.getResultList();
	}

	@Override
	public List<FoodPreference> findByMeeting(int meetingId) {
		TypedQuery<FoodPreference> typedQuery = this.manager.createNamedQuery("Preference.findByMeeting",
				FoodPreference.class);
		typedQuery.setParameter("meetingId", meetingId);
		return typedQuery.getResultList();
	}

	@Override
	public List<FoodPreference> findByUserAndMeeting(int userId, int meetingId) {
		TypedQuery<FoodPreference> typedQuery = this.manager.createNamedQuery("Preference.findByUserAndMeeting",
				FoodPreference.class);
		typedQuery.setParameter("userId", userId);
		typedQuery.setParameter("meetingId", meetingId);
		return typedQuery.getResultList();
	}

	@Override
	public List<User> findUsersWithThatPereference(String prefName) {
		TypedQuery<User> query = this.manager.createNamedQuery("Preference.findUserByPrefName", User.class);
		query.setParameter("prefName", prefName);
		return query.getResultList();
	}

	@Override
	public List<FoodPreference> findByName(String name) {
		CriteriaBuilder cb = this.manager.getCriteriaBuilder();
		CriteriaQuery<FoodPreference> query = cb.createQuery(FoodPreference.class);
		Root<FoodPreference> root = query.from(FoodPreference.class);
		ParameterExpression<String> param = cb.parameter(String.class);
		query.select(root).where(cb.equal(root.get("name"), param));
		TypedQuery<FoodPreference> typedQuery = this.manager.createQuery(query);
		typedQuery.setParameter(param, name); 
		return typedQuery.getResultList(); 
	}

	@Override
	public FoodPrefAllergyDTO savePrefAndAllergy(FoodPrefAllergyDTO object) {
		try {
			//EntityManagerHelper.beginTransaction();
			User user = this.userDAO.findUserById(object.getPref().getId().getUserId());
			if(user == null) 
				throw new Exception("Cannot find user with id " + object.getPref().getId().getUserId()); 
			
			user.addAllergy(object.getAllergy());
			this.manager.merge(user);
			this.create(object.getPref());
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return object;
	}

}

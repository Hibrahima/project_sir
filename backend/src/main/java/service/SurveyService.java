package service;

import java.io.IOException;
import javax.ws.rs.core.Response;
import org.apache.commons.mail.EmailException;
import exceptions.ExistingObjectException;
import models.Survey;
import utils.SurveyMeetingPostObject;
import utils.SurveyValidationData;

public interface SurveyService {
	
	public Response update(SurveyMeetingPostObject s) throws IOException;
	public Response getAll();
	public Response getUserSurveys(int userId);
	public Response getById(int id);
	public Response addDate(int surveyId ,int dateId)throws ExistingObjectException, IOException;
	public Response removeDate(int surveyId, int dateId)throws IllegalArgumentException, IOException;
	public Response findByCreationDate(String date);
	public Response findByCustomDate(String date);
	public Response findByStatus(String status);
	public Response findByCustomDateAndTimeSlot(String date,int begin, int end);
	public Response getSurveyParticipations(int surveyId);
	public Response getParticipants(int surveyId);
	public Response validate(SurveyValidationData data) throws ExistingObjectException, IOException;
	public Response getAllTimeSlotsByParticipation(int surveyId);
	public Response getAllTimeSlotsByParticipationOderedByScore(int surveyId);
	public Response getSurveyCreator(int surveyId);
	public Response getResult(int surveyId);
	//public Response sendEmail(int surveyId) throws EmailException;

}

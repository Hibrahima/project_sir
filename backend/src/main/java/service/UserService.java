package service;

import java.io.IOException;
import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import exceptions.ExistingObjectException;
import models.Allergy;
import models.Role;
import models.Survey;
import models.User;
import utils.LoginData;
import utils.SurveyMeetingPostObject;

public interface UserService  {
	
	public Response createUser(User user) throws IOException;
	public Response findUserById(int id); 
	public Response updateUser(User user) throws IOException, EntityNotFoundException;
	public Response getAll();
	//public List<Meeting> getMeetings();
	public Response addNewRole(int userId, Role role) throws ExistingObjectException, IOException;
	public Response addPredefinedRole(int userId, int roleId) throws ExistingObjectException, IOException;
	public Response removeRole(int userId, int roleId) throws IllegalArgumentException, IOException;
	public Response findByRoleName(String roleName);
	public Response findByAllergyName(String allergyName);
	//public List<User> findByPreferenceName(String preferenceName);
	public Response addAllergy(int userId, Allergy a) throws ExistingObjectException, IOException ;
	public Response addCreatedSurvey(int userId, SurveyMeetingPostObject object) throws ExistingObjectException, IOException;
	//public void addParticipation(int userId, int surveyId, Participation p) throws ExistingObjectException;
	
	public Response registerUser(User user) throws ExistingObjectException, IOException;
	public Response findCreatedSurveysOnDate(String date, int userId);
	public Response findParticipationsOnDate(String date, int userId);
	public Response findParticipations(int userId);
	public Response login(LoginData data) throws IOException;
	public Response getAttendedMettings(int userId) throws IOException;
	public Response getMissedMettings(int userId) throws IOException;
	
}

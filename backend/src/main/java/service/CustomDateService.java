package service;

import java.io.IOException;
import javax.ws.rs.core.Response;
import exceptions.ExistingObjectException;
import models.CustomDate;

public interface CustomDateService {

	public Response create(CustomDate date) throws IOException;

	public Response getById(int id) throws IOException;

	public Response update(CustomDate d) throws IOException;

	public Response delete(int id) throws IOException;

	public Response addPredefinedTimeSlot(int dateId, int timeId) throws ExistingObjectException, IOException;

	public Response removeTimeSlot(int dateId, int timeId) throws IOException;

	public Response getAll();

	public Response findByName(String name);

	public Response findByDate(String date);

	public Response findByStatus(boolean active);

}

package service;

import java.io.IOException;
import javax.ws.rs.core.Response;
import models.Meeting;

public interface MeetingService {
	
	public Response update(Meeting m) throws IOException;
	public Response getById(int id);
	public Response getAll();
	public Response findByDate(String date);
	public Response findByTitle(String title);
	public Response findByOfficeAddress(String address);
	public Response findByTimeSlot(String timeSlot);
	public Response findAbsents(int meetingId);
	public Response findPresents(int meetingId);

}

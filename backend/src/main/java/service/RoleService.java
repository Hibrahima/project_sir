package service;

import java.io.IOException;
import javax.ws.rs.core.Response;
import models.Role;

public interface RoleService {
	
	public Response getRoleById(int id);
	public Response createRole(Role role) throws IOException;
	public Response getAllRole();
	public Response updateRole(Role role) throws IOException;
	public Response deleteRole(int id) throws IOException;
	public Response findByName(String name);

}

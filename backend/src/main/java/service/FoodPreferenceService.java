package service;

import java.io.IOException;

import javax.ws.rs.core.Response;

import dto.FoodPrefAllergyDTO;
import models.Allergy;
import models.FoodPreference;

public interface FoodPreferenceService  {
	
	public Response create(FoodPreference fp) throws IOException ;
	public Response update(FoodPreference fp) throws IOException ;
	public Response getById(int userId, int meetingId) throws IOException ;
	public Response getAll();
	public Response findByUser(int userId) ;
	public Response findByMeeting(int meetingId);
	public Response findByUserAndMeeting(int userId, int meetingId);
	public Response findUsersWithThatPerefernce(String prefName);
	public Response findByName(String name);
	public Response savePrefAndAllergy(FoodPrefAllergyDTO object);

}

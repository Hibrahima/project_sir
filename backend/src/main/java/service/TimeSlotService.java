package service;

import java.io.IOException;
import javax.ws.rs.core.Response;
import models.TimeSlot;

public interface TimeSlotService {
	
	public Response create(TimeSlot t) throws IOException;
	public Response getById(int id) throws IOException;
	public Response update(TimeSlot t) throws IOException;
	public Response delete(int id) throws IOException;
	public Response getAll();
	public Response getAllByState(boolean active);
	public Response getAllOrderedByScore();
}

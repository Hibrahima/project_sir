package service;

import java.io.IOException;
import javax.ws.rs.core.Response;
import models.Participation;

public interface ParticipationService {

	public Response create(Participation p) throws IOException;
	public Response update(Participation p) throws IOException;
	public Response getById(int userId, int surveyId);
	public Response getAll();
	public Response findAllOrderedByScore();
	public Response findByDate(String date);
	public Response didUserAlreadyParticipate(int userId, int surveyId);
}

package enums;

public enum SurveyStatus {
	
	DEFAULT("default"),
	READY("ready"),
	PENDING("pending"),
	VALIDATED("validated");
	
	
	private SurveyStatus(String status) {
		this.statusName = status;
	}
	
	private String statusName;
	
	public String getStatusName() {
		return statusName;
	}
	
}

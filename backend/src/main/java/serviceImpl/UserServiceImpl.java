package serviceImpl;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dao.UserDAO;
import daoImpl.UserDAOImpl;
import exceptions.ExistingObjectException;
import models.Allergy;
import models.Meeting;
import models.Participation;
import models.Role;
import models.Survey;
import models.User;
import service.UserService;
import utils.LoginData;
import utils.ResponseBuilder;
import utils.SurveyMeetingPostObject;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserServiceImpl implements UserService {

	private UserDAO userDAO;

	public UserServiceImpl() {
		this.userDAO = new UserDAOImpl();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createUser(User user) throws IOException {
		try {
			User result = this.userDAO.createUser(user);
			return ResponseBuilder.buildResponse(result, "Cannot persist user");

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot persist user - Cause : " + e.getMessage());
		}

	}

	@GET
	public Response getAll() { 
		try {
			List<User> results = this.userDAO.getAll();
			return ResponseBuilder.buildResponse(results, "Cannot get users from database");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot get users from database - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/{id}")
	public Response findUserById(@PathParam("id") int id) {
		try {
			User result = this.userDAO.findUserById(id);
			return ResponseBuilder.buildResponse(result, "Cannot find user with id " + id);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot find user with id " + id + " - Cause : " + e.getMessage());
		}

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(User user) throws EntityNotFoundException, IOException {
		try {
			User result = this.userDAO.updateUser(user);
			return ResponseBuilder.buildResponse(result, "Cannot update user with id " + user.getId());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot update user with id " + user.getId() + " - Cause : " + e.getMessage());
		}

	}

	@POST
	@Path("/{userId}/roles")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addNewRole(@PathParam("userId") int userId, Role role) throws ExistingObjectException, IOException {
		try {
			Role result = this.userDAO.addNewRole(userId, role);
			return ResponseBuilder.buildResponse(result, "Cannot add role to user with id " + userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot add role to user with id " + userId + " - Cause : " + e.getMessage());
		}

	}

	@POST
	@Path("/{userId}/add-role/{roleId}")
	public Response addPredefinedRole(@PathParam("userId") int userId, @PathParam("roleId") int roleId)
			throws ExistingObjectException, IOException {
		try {
			Role result = this.userDAO.addPredefinedRole(userId, roleId);
			return ResponseBuilder.buildResponse(result, "Cannot add role to user  with id " + userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot add role to user  with id " + userId + " - Cause : " + e.getMessage());
		}

	}

	@DELETE
	@Path("/{userId}/{roleId}")
	public Response removeRole(@PathParam("userId") int userId, @PathParam("roleId") int roleId)
			throws IllegalArgumentException, IOException {
		try {
			Role result = this.userDAO.removeRole(userId, roleId);
			return ResponseBuilder.buildResponse(result, "Cannot remove role from user with id " + userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot remove role from user with id " + userId + " - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/search/by-roles/{roleName}")
	public Response findByRoleName(@PathParam("roleName") String roleName) {
		try {
			List<User> results = this.userDAO.findByRoleName(roleName);
			return ResponseBuilder.buildResponse(results, "Cannot find users with role name " + roleName);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot find users with role name " + roleName + " - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/search/by-allergies/{allergyName}")
	public Response findByAllergyName(@PathParam("allergyName") String allergyName) {
		try {
			List<User> results = this.userDAO.findByAllergyName(allergyName);
			return ResponseBuilder.buildResponse(results, "Cannot find users with allergy name " + allergyName);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot find users with allergy name " + allergyName + " - Cause : " + e.getMessage());
		}

	}


	@POST
	@Path("/{userId}/add-allergy")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addAllergy(@PathParam("userId") int userId, Allergy a) throws ExistingObjectException, IOException {
		try {
			Allergy result = this.userDAO.addAllergy(userId, a);
			return ResponseBuilder.buildResponse(result, "Cannot add allergy to user with id " + userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot add allergy to user with id " + userId + "- Cause : " + e.getMessage());
		}
	}

	@POST
	@Path("/{userId}/surveys")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addCreatedSurvey(@PathParam("userId") int userId, SurveyMeetingPostObject object)
			throws ExistingObjectException, IOException {

		try {
			SurveyMeetingPostObject result = this.userDAO.addCreatedSurvey(userId, object);
			return ResponseBuilder.buildResponse(result, "Cannot created survey for user with id " + userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot created survey for user with id " + userId + " - Cause : " + e.getMessage());
		}

	}

	@Path("/register")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerUser(User user) throws ExistingObjectException, IOException {
		try {
			User result = this.userDAO.registerUser(user);
			return ResponseBuilder.buildResponse(result, "Cannot register user");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot register user - Cause :" + e.getMessage());
		}

	}

	@GET
	@Path("/{userId}/created-surveys/by-date/{date}")
	public Response findCreatedSurveysOnDate(@PathParam("date") String date, @PathParam("userId") int userId) {
		try {
			List<Survey> results = this.userDAO.findCreatedSurveysOnDate(date, userId);
			return ResponseBuilder.buildResponse(results,
					"Cannot find surveys that have been created by user with id " + userId + " on date " + date);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, "Cannot find surveys that have been created by user with id "
					+ userId + " on date " + date + " - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/{userId}/participations/by-date/{date}")
	public Response findParticipationsOnDate(@PathParam("date") String date, @PathParam("userId") int userId) {
		try {
			List<Participation> results = this.userDAO.findParticipationsOnDate(date, userId);
			return ResponseBuilder.buildResponse(results,
					"Cannot find participations for user with id " + userId + " on date " + date);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find participations for user with id " + userId
					+ " on date " + date + " - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/{userId}/participations")
	public Response findParticipations(@PathParam("userId") int userId) {
		try {
			List<Participation> results = this.userDAO.findParticipations(userId);
			return ResponseBuilder.buildResponse(results, "Cannot find participations for user with id " + userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot find participations for user with id " + userId + " - Cause : " + e.getMessage());
		}

	}

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(LoginData data) throws IOException {
		try {
			User result = this.userDAO.login(data);
			return ResponseBuilder.buildResponse(result, "Cannot login");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot login - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/{userId}/attended-meetings")
	public Response getAttendedMettings(@PathParam("userId") int userId) throws IOException {
		try {
			List<Meeting> results = this.userDAO.getAttendedMettings(userId);
			return ResponseBuilder.buildResponse(results, "Cannot find attended meetings for user with id " + userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot find attended meetings for user with id " + userId + " - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/{userId}/missed-meetings")
	public Response getMissedMettings(@PathParam("userId") int userId) throws IOException {
		try {
			List<Meeting> results = this.userDAO.getMissedMettings(userId);
			return ResponseBuilder.buildResponse(results, "Cannot find missed meetings for user with id " + userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null,
					"Cannot find missed meetings for user with id " + userId + " - Cause : " + e.getMessage());
		}

	}

}

package serviceImpl;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dao.RoleDAO;
import daoImpl.RoleDAOImpl;
import models.Role;
import service.RoleService;
import utils.ResponseBuilder;

@Path("/roles")
@Produces(MediaType.APPLICATION_JSON)
public class RoleServiceImpl implements RoleService {

	private RoleDAO roleDAO;

	public RoleServiceImpl() {
		this.roleDAO = new RoleDAOImpl();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createRole(Role role) {
		String message = null;
		try {
			message = "Cannot create role";
			Role result = this.roleDAO.createRole(role);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	public Response getRoleById(@PathParam("id") int id) {
		String message = null;
		try {
			message = "Cannot find role with id " + id;
			Role result = this.roleDAO.getRoleById(id);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	public Response getAllRole() {
		String message = null;
		try {
			message = "Cannot get all roles";
			List<Role> results = this.roleDAO.getAllRole();
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateRole(Role role) {
		String message = null;
		try {
			message = "Cannot update role";
			Role result = this.roleDAO.updateRole(role);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteRole(@PathParam("id") int id) {
		String message = null;
		try {
			message = "Cannot delete role with id " + id;
			Role result = this.roleDAO.deleteRole(id);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/search/by-name/{name}")
	@GET
	public Response findByName(@PathParam("name") String name) {
		String message = null;
		try {
			message = "Cannot find any role with name " + name;
			Role result = this.roleDAO.findByName(name);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}
}
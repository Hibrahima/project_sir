package serviceImpl;

import java.io.IOException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.mail.EmailException;
import dao.SurveyDAO;
import daoImpl.SurveyDAOImpl;
import dto.ParticipationDTO;
import exceptions.ExistingObjectException;
import models.CustomDate;
import models.Participation;
import models.Survey;
import models.SurveyResult;
import models.TimeSlot;
import models.User;
import service.SurveyService;
import utils.ResponseBuilder;
import utils.SurveyMeetingPostObject;
import utils.SurveyValidationData;

@Path("/surveys")
@Produces(MediaType.APPLICATION_JSON)
public class SurveyServiceImpl implements SurveyService {

	private SurveyDAO surveyDAO;

	public SurveyServiceImpl() {
		this.surveyDAO = new SurveyDAOImpl();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(SurveyMeetingPostObject s) {
		String message = null;
		try {
			message = "Cannot update survey with name " + s.getSurveyName();
			Survey result = this.surveyDAO.update(s);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	public Response getAll() {
		String message = null;
		try {
			message = "Cannot get all surveys";
			List<Survey> results = this.surveyDAO.getAll();
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/search/by-users/{userId}")
	@GET
	public Response getUserSurveys(@PathParam("userId") int userId) {
		String message = null;
		try {
			message = "Cannot find surveys for user with id " + userId;
			List<Survey> results = this.surveyDAO.getUserSurveys(userId);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/{id}")
	@GET
	public Response getById(@PathParam("id") int id) {
		String message = null;
		try {
			message = "Cannot find surveys with id " + id;
			Survey result = this.surveyDAO.getById(id);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/{surveyId}/add-date/{dateId}")
	@POST
	public Response addDate(@PathParam("surveyId") int surveyId, @PathParam("dateId") int dateId)
			throws ExistingObjectException {
		String message = null;
		try {
			message = "Cannot add date with id " + dateId + " to suvey with id " + surveyId;
			CustomDate result = this.surveyDAO.addDate(surveyId, dateId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/{surveyId}/remove-date/{dateId}")
	@DELETE
	public Response removeDate(@PathParam("surveyId") int surveyId, @PathParam("dateId") int dateId)
			throws IllegalArgumentException {
		String message = null;
		try {
			message = "Cannot delete date with id " + dateId + " from suvey with id " + surveyId;
			CustomDate result = this.surveyDAO.removeDate(surveyId, dateId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/search/by-creation-date/{date}")
	@GET
	public Response findByCreationDate(@PathParam("date") String date) {
		String message = null;
		try {
			message = "Cannot find surveys created on date " + date;
			List<Survey> results = this.surveyDAO.findByCreationDate(date);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/search/by-date/{date}")
	@GET
	public Response findByCustomDate(@PathParam("date") String date) {
		String message = null;
		try {
			message = "Cannot find surveys on date " + date;
			List<Survey> results = this.surveyDAO.findByCustomDate(date);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/search/by-status/{status}")
	@GET
	public Response findByStatus(@PathParam("status") String status) {
		String message = null;
		try {
			message = "Cannot find surveys with status " + status;
			List<Survey> results = this.surveyDAO.findByStatus(status);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}

	}

	@Path("/search/by-date/{date}/and-by-slot/{begin}/{end}")
	@GET
	public Response findByCustomDateAndTimeSlot(@PathParam("date") String date, @PathParam("begin") int begin,
			@PathParam("end") int end) {
		String message = null;
		try {
			message = "Cannot find surveys with date " + date + " that starts at " + begin + " and ends at " + end;
			List<Survey> results = this.surveyDAO.findByCustomDateAndTimeSlot(date, begin, end);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/{surveyId}/participations")
	@GET
	public Response getSurveyParticipations(@PathParam("surveyId") int surveyId) {
		String message = null;
		try {
			message = "Cannot find participations for survey with survey id " + surveyId;
			List<ParticipationDTO> results = this.surveyDAO.getSurveyParticipations(surveyId);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/{surveyId}/participants")
	@GET
	public Response getParticipants(@PathParam("surveyId") int surveyId) {
		String message = null;
		try {
			message = "Cannot find participations for survey with survey id " + surveyId;
			List<User> results = this.surveyDAO.getParticipants(surveyId);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@POST
	@Path("/validate")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response validate(SurveyValidationData data) throws ExistingObjectException, IOException {
		String message = null;
		try {
			message = "Cannot validate survey with id " + data.getSurveyId();
			Survey results = this.surveyDAO.validate(data);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/{surveyId}/time-slots")
	public Response getAllTimeSlotsByParticipation(@PathParam("surveyId") int surveyId) {
		String message = null;
		try {
			message = "Cannot find all time slots associated with survey with id " + surveyId;
			List<TimeSlot> results = this.surveyDAO.getAllTimeSlotsByParticipation(surveyId);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/{surveyId}/time-slots/ordered-by-score")
	public Response getAllTimeSlotsByParticipationOderedByScore(@PathParam("surveyId") int surveyId) {
		String message = null;
		try {
			message = "Cannot find all ordered time slots associated with survey with id " + surveyId;
			List<TimeSlot> results = this.surveyDAO.getAllTimeSlotsByParticipationOderedByScore(surveyId);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/{surveyId}/creator")
	public Response getSurveyCreator(@PathParam("surveyId") int surveyId) {
		String message = null;
		try {
			message = "Cannot find creator for survey with id " + surveyId;
			User result = this.surveyDAO.getSurveyCreator(surveyId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/{surveyId}/result")
	public Response getResult(@PathParam("surveyId") int surveyId) {
		String message = null;
		try {
			message = "Cannot find result for survey with id " + surveyId;
			SurveyResult result = this.surveyDAO.getResult(surveyId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	/*@Path("/{surveyId}/send-email")
	@GET
	public Response sendEmail(@PathParam("surveyId")int surveyId) throws EmailException {
		String message = null;
		try {
			message = "Cannot send email for survey with id " + surveyId;
			Survey result = this.surveyDAO.sendEmail(surveyId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage()); 
		}
	}*/

}

package serviceImpl;

import java.io.IOException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dao.FoodPreferenceDAO;
import daoImpl.FoodPreferenceDAOImpl;
import dto.FoodPrefAllergyDTO;
import models.Allergy;
import models.FoodPreference;
import models.User;
import service.FoodPreferenceService;
import utils.ResponseBuilder;

@Path("/food-preferences")
@Produces(MediaType.APPLICATION_JSON)
public class FoodPreferenceServiceImpl implements FoodPreferenceService {
	
	private FoodPreferenceDAO foodDAO;
	
	public FoodPreferenceServiceImpl() {
		this.foodDAO = new FoodPreferenceDAOImpl();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(FoodPreference fp) throws IOException {
		try {
			FoodPreference result = this.foodDAO.create(fp);;
			return ResponseBuilder.buildResponse(result, "Cannot create food preference");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot create food preference - Cause : "+e.getMessage());
		}

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(FoodPreference fp) {
		try {
			FoodPreference result = this.foodDAO.update(fp);
			return ResponseBuilder.buildResponse(result, "Cannot update food preference");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot update food preference - Cause : "+e.getMessage());
		}
		
	}

	@GET
	@Path("/{userId}/{meetingId}")
	public Response getById(@PathParam("userId") int userId, @PathParam("meetingId") int meetingId) {
		try {
			FoodPreference result = this.foodDAO.getById(userId, meetingId);
			return ResponseBuilder.buildResponse(result, "Cannot find food preference with user id "+userId+" and meeting id "+meetingId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot update food preference - Cause : "+e.getMessage());
		}
	}

	@GET
	public Response getAll() {
		try {
			List<FoodPreference> results = this.foodDAO.getAll();
			return ResponseBuilder.buildResponse(results, "Cannot find all food preferences");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find all food preferences");
		}
		
	}

	@GET
	@Path("/search/by-user/{userId}")
	public Response findByUser(@PathParam("userId")int userId) {
		try {
			List<FoodPreference> results = this.foodDAO.findByUser(userId);
			return ResponseBuilder.buildResponse(results, "Cannot find preferences for user with id "+userId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find preferences for user - Cause : "+e.getMessage());
		}
		
	}

	@GET
	@Path("/search/by-meeting/{meetingId}")
	public Response findByMeeting(@PathParam("meetingId") int meetingId) {
		try {
			List<FoodPreference> results = this.foodDAO.findByMeeting(meetingId);
			return ResponseBuilder.buildResponse(results, "Cannot find preferences for meeting with id "+meetingId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find preferences for meeting - Cause : "+e.getMessage());
		}
	}

	@GET
	@Path("/users/{userId}/meetings/{meetingId}")
	public Response findByUserAndMeeting(@PathParam("userId") int userId, @PathParam("meetingId") int meetingId) { 
		try {
			List<FoodPreference> results = this.foodDAO.findByUserAndMeeting(userId, meetingId);
			return ResponseBuilder.buildResponse(results, "Cannot find preferences for user with id "+userId +" and for meeting with id "+meetingId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find preferences for user and for meeting - Cause : "+e.getMessage());
		}
	}

	@GET
	@Path("/search/users/by-pref-name/{prefName}")
	public Response findUsersWithThatPerefernce(@PathParam("prefName")String prefName) {
		try {
			List<User> results = this.foodDAO.findUsersWithThatPereference(prefName);
			return ResponseBuilder.buildResponse(results, "Cannot find users with  preference "+prefName);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find users with  preference - Cause : "+e.getMessage());
		}
	}


	@GET
	@Path("/search/by-name/{prefName}")
	public Response findByName(@PathParam("prefName")String name) {
		try {
			List<FoodPreference> results = this.foodDAO.findByName(name);
			return ResponseBuilder.buildResponse(results, "Cannot find preferences with  name "+name);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot  find preferences - Cause : "+e.getMessage());
		} 
	}

	@POST
	@Path("/save-prefs-allergies")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response savePrefAndAllergy(FoodPrefAllergyDTO object) {
		try {
			FoodPrefAllergyDTO result = this.foodDAO.savePrefAndAllergy(object);
			return ResponseBuilder.buildResponse(result, "Cannot save preferences for user "+ result.getPref().getId().getUserId());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot  find preferences - Cause : "+e.getMessage());
		} 
	}

}

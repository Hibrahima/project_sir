package serviceImpl;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dao.MeetingDAO;
import daoImpl.MeetingDAOImpl;
import models.Meeting;
import models.User;
import service.MeetingService;
import utils.ResponseBuilder;

@Path("/meetings")
@Produces(MediaType.APPLICATION_JSON)
public class MeetingServiceImpl implements MeetingService {

	private MeetingDAO meetingDAO;

	public MeetingServiceImpl() {
		this.meetingDAO = new MeetingDAOImpl();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(Meeting m) {
		try {
			Meeting result = this.meetingDAO.update(m);
			return ResponseBuilder.buildResponse(result, "Cannot find meeting with  id " + m.getId());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find meeting - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/{id}")
	public Response getById(@PathParam("id") int id) { 
		try {
			Meeting result = this.meetingDAO.getById(id);
			return ResponseBuilder.buildResponse(result, "Cannot find meeting with  id " + id);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find meeting - Cause : " + e.getMessage());
		}
	}

	@GET
	public Response getAll() {
		try {
			List<Meeting> results = this.meetingDAO.getAll();
			return ResponseBuilder.buildResponse(results, "Cannot get all meetings");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot get all meetings - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/search/by-date/{date}")
	public Response findByDate(@PathParam("date") String date) {
		try {
			List<Meeting> results = this.meetingDAO.findByDate(date);
			return ResponseBuilder.buildResponse(results, "Cannot find meetings on date "+date);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find meetings - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/search/by-title/{title}")
	public Response findByTitle(@PathParam("title") String title) {
		try {
			Meeting result = this.meetingDAO.findByTitle(title);
			return ResponseBuilder.buildResponse(result, "Cannot find meetings with title "+title);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find meetings  with title "+title+" - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/search/by-address/{address}")
	public Response findByOfficeAddress(@PathParam("address") String address) {
		try {
			List<Meeting> results = this.meetingDAO.findByOfficeAddress(address);
			return ResponseBuilder.buildResponse(results, "Cannot find meetings held at  "+address);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find meetings held at "+address+" - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/search/by-slot/{slot}")
	public Response findByTimeSlot(@PathParam("slot") String timeSlot) { 
		try {
			List<Meeting> results = this.meetingDAO.findByTimeSlot(timeSlot);
			return ResponseBuilder.buildResponse(results, "Cannot find meetings held at  "+timeSlot);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find meetings held at "+timeSlot+" - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/{meetingId}/absents")
	public Response findAbsents(@PathParam("meetingId") int meetingId) {
		try {
			List<User> results = this.meetingDAO.findAbsents(meetingId);
			return ResponseBuilder.buildResponse(results, "Cannot find absents of meetings with id "+meetingId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find absents of meetings with id "+meetingId+" - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/{meetingId}/presents")
	public Response findPresents(@PathParam("meetingId") int meetingId) {
		try {
			List<User> results = this.meetingDAO.findPresents(meetingId);
			return ResponseBuilder.buildResponse(results, "Cannot find presents of meetings with id "+meetingId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, "Cannot find presents of meetings with id "+meetingId+" - Cause : " + e.getMessage());
		}
	}

}

package serviceImpl;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dao.ParticipationDAO;
import daoImpl.ParticipationDAOImpl;
import dto.ParticipationDTO;
import dto.UserP;
import models.Participation;
import service.ParticipationService;
import utils.ResponseBuilder;

@Path("/participations")
@Produces(MediaType.APPLICATION_JSON)
public class ParticipationServiceImpl implements ParticipationService {

	private ParticipationDAO participationDAO;

	public ParticipationServiceImpl() {
		this.participationDAO = new ParticipationDAOImpl();
	}

	@PUT
	@Consumes
	public Response update(Participation p) {
		String message = null;
		try {
			message = "Cannot update participation with  user id " + p.getId().getUserId() + " and survey id "
					+ p.getId().getSurveyId();
			Participation result = this.participationDAO.update(p);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}

	}

	@GET
	@Path("/users/{userId}/surveys/{surveyId}")
	public Response getById(@PathParam("userId") int userId, @PathParam("surveyId") int surveyId) {
		String message = null;
		try {
			message = "Cannot find participation with  user id " + userId + " and survey id " + surveyId;
			Participation result = this.participationDAO.getById(userId, surveyId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	public Response getAll() {
		String message = null;
		try {
			message = "Cannot get all participations";
			List<ParticipationDTO> results = this.participationDAO.getAll();
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/ordered-by-score")
	@GET
	public Response findAllOrderedByScore() {
		String message = null;
		try {
			message = "Cannot get all participations ordered by score";
			List<Participation> results = this.participationDAO.findAllOrderedByScore();
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@Path("/search/by-date/{date}")
	@GET
	public Response findByDate(@PathParam("date") String date) {
		String message = null;
		try {
			message = "Cannot find participations on date " + date;
			List<Participation> results = this.participationDAO.findByDate(date);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@POST
	@Consumes
	public Response create(Participation p) {
		String message = null;
		try {
			message = "Cannot create participations";
			Participation result = this.participationDAO.create(p);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/can-participate/{userId}/{surveyId}")
	public Response didUserAlreadyParticipate(@PathParam("userId") int userId, @PathParam("surveyId") int surveyId) {
		String message = null;
		try {
			message = "Cannot get the boolean whether or not the user has alreday participated";
			UserP result = this.participationDAO.didUserAlreadyParticipate(userId, surveyId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

}

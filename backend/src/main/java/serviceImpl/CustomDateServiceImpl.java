package serviceImpl;

import java.io.IOException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dao.CustomDateDAO;
import daoImpl.CustomDateDAOImpl;
import exceptions.ExistingObjectException;
import models.CustomDate;
import models.TimeSlot;
import service.CustomDateService;
import utils.ResponseBuilder;

@Path("/dates")
@Produces(MediaType.APPLICATION_JSON)
public class CustomDateServiceImpl implements CustomDateService {

	private CustomDateDAO CustomDateDAO;

	public CustomDateServiceImpl() {
		this.CustomDateDAO = new CustomDateDAOImpl();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(CustomDate date) {
		String message = null;
		try {
			message = "Cannot create custom date named "+date.getName();
			CustomDate result = this.CustomDateDAO.create(date);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}

	}

	@GET
	@Path("/{id}")
	public Response getById(@PathParam("id") int id) {
		String message = null;
		try {
			message = "Cannot find customdate with id "+id;
			CustomDate result = this.CustomDateDAO.getById(id);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(CustomDate d) {
		String message = null;
		try {
			message = "Cannot update customdate with id "+d.getId();
			CustomDate result = this.CustomDateDAO.update(d);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}

	}

	@Path("/{id}")
	@DELETE
	public Response delete(@PathParam("id") int id) {
		String message = null;
		try {
			message = "Cannot delete custom date with id "+id;
			CustomDate result = this.CustomDateDAO.delete(id);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}
	}

	@Path("/{dateId}/add-time/{timeId}")
	@POST
	public Response addPredefinedTimeSlot(@PathParam("dateId") int dateId, @PathParam("timeId") int timeId)
			throws ExistingObjectException, IOException {
		String message = null;
		try {
			message = "Cannot add time slot with id "+timeId+" to the custom date with id "+dateId;
			TimeSlot result = this.CustomDateDAO.addPredefinedTimeSlot(dateId, timeId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}

	}

	@GET
	public Response getAll() {
		String message = null;
		try {
			message = "Cannot get all custom dates";
			List<CustomDate> results = this.CustomDateDAO.getAll();
			System.out.println("------------------------------all dates size "+ results.size());
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}
	}

	@Path("/search/by-name/{name}")
	@GET
	public Response findByName(@PathParam("name") String name) {
		String message = null;
		try {
			message = "Cannot find custom date with name "+name;
			List<CustomDate> results = this.CustomDateDAO.findByName(name);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}
	}

	@Path("/search/by-date/{date}")
	@GET
	public Response findByDate(@PathParam("date") String date) {
		String message = null;
		try {
			message = "Cannot find custom date on date "+date;
			List<CustomDate> results = this.CustomDateDAO.findByDate(date);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}
	}

	@Path("/{dateId}/remove/{timeId}")
	@DELETE
	public Response removeTimeSlot(@PathParam("dateId") int dateId, @PathParam("timeId") int timeId) throws IOException {
		String message = null;
		try {
			message = "Cannot remove tims slot with id "+timeId+" from date with id "+dateId;
			TimeSlot result = this.CustomDateDAO.removeTimeSlot(dateId, timeId);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}
	}

	@Path("/search/by-status/{status}")
	@GET
	public Response findByStatus(@PathParam("status") boolean active) {
		String message = null;
		try {
			message = "Cannot find dates with status "+active;
			List<CustomDate> results = this.CustomDateDAO.findByStatus(active);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message+" - Cause : "+e.getMessage());
		}
	}

}

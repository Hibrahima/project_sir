package serviceImpl;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dao.TimeSlotDAO;
import daoImpl.TimeSlotDAOImpl;
import models.TimeSlot;
import service.TimeSlotService;
import utils.ResponseBuilder;

@Path("/time-slots")
@Produces(MediaType.APPLICATION_JSON)
public class TimeSlotSericeImpl implements TimeSlotService {

	private TimeSlotDAO timeSlotDAO;

	public TimeSlotSericeImpl() {
		this.timeSlotDAO = new TimeSlotDAOImpl();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(TimeSlot t) {
		String message = null;
		try {
			message = "Cannot create time slot";
			TimeSlot result = this.timeSlotDAO.create(t);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	public Response getById(@PathParam("id") int id) {
		String message = null;
		try {
			message = "Cannot find time slot with id " + id;
			TimeSlot result = this.timeSlotDAO.getById(id);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(TimeSlot t) {
		String message = null;
		try {
			message = "Cannot update time slot";
			TimeSlot result = this.timeSlotDAO.update(t);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") int id) {
		String message = null;
		try {
			message = "Cannot delete time slot with id " + id;
			TimeSlot result = this.timeSlotDAO.delete(id);
			return ResponseBuilder.buildResponse(result, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	public Response getAll() {
		String message = null;
		try {
			message = "Cannot get all time slots";
			List<TimeSlot> results = this.timeSlotDAO.getAll();
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/search/by-status/{active}")
	public Response getAllByState(@PathParam("active") boolean active) {
		String message = null;
		try {
			message = "Cannot get all time slots by status";
			List<TimeSlot> results = this.timeSlotDAO.getAllByState(active);
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

	@GET
	@Path("/ordered-by-score")
	public Response getAllOrderedByScore() {
		String message = null;
		try {
			message = "Cannot get all time slots ordered by score";
			List<TimeSlot> results = this.timeSlotDAO.getAllOrderedByScore();
			return ResponseBuilder.buildResponse(results, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseBuilder.buildResponse(null, message + " - Cause : " + e.getMessage());
		}
	}

}

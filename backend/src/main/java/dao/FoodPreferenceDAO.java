package dao;

import java.io.IOException;
import java.util.List;

import dto.FoodPrefAllergyDTO;
import models.Allergy;
import models.FoodPreference;
import models.User;

public interface FoodPreferenceDAO {
	
	public FoodPreference create(FoodPreference fp) throws IOException ;
	public FoodPreference update(FoodPreference fp) throws IOException ;
	public FoodPreference getById(int userId, int meetingId) throws IOException ;
	public List<FoodPreference> getAll();
	public List<FoodPreference> findByUser(int userId);
	public List<FoodPreference> findByMeeting(int meetingId);
	public List<FoodPreference> findByUserAndMeeting(int userId, int meetingId);
	public List<User> findUsersWithThatPereference(String prefName);
	public List<FoodPreference> findByName(String name);
	public FoodPrefAllergyDTO savePrefAndAllergy(FoodPrefAllergyDTO object);

}

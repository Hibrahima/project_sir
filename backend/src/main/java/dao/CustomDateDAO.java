package dao;

import java.io.IOException;
import java.util.List;
import exceptions.ExistingObjectException;
import exceptions.OperationNotAllowedException;
import models.CustomDate;
import models.TimeSlot;

public interface CustomDateDAO {

	public CustomDate create(CustomDate date) throws IOException,  OperationNotAllowedException ;

	public CustomDate getById(int id) throws IOException;

	public CustomDate update(CustomDate d) throws IOException;

	public CustomDate delete(int id) throws IOException;

	public TimeSlot addPredefinedTimeSlot(int dateId, int timeId)
			throws ExistingObjectException, IOException, OperationNotAllowedException;

	public TimeSlot removeTimeSlot(int dateId, int timeId) throws IOException;

	public List<CustomDate> getAll();

	public List<CustomDate> findByName(String name);

	public List<CustomDate> findByDate(String date);

	public List<CustomDate> findByStatus(boolean active);

}

package dao;

import java.io.IOException;
import java.util.List;

import dto.ParticipationDTO;
import dto.UserP;
import exceptions.OperationNotAllowedException;
import models.Participation;

public interface ParticipationDAO {

	public Participation create(Participation p) throws IOException, OperationNotAllowedException;
	public Participation update(Participation p) throws IOException;
	public Participation getById(int userId, int surveyId);
	public List<ParticipationDTO> getAll();
	public List<Participation> findAllOrderedByScore();
	public List<Participation> findByDate(String date);
	public UserP didUserAlreadyParticipate(int userId, int surveyId);
	
}

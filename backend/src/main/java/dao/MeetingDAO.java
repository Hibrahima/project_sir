package dao;

import java.io.IOException;
import java.util.List;

import models.Meeting;
import models.User;

public interface MeetingDAO {
	
		public Meeting update(Meeting m) throws IOException;
		public Meeting getById(int id);
		public List<Meeting> getAll();
		public List<Meeting> findByDate(String date);
		public Meeting findByTitle(String title);
		public List<Meeting> findByOfficeAddress(String address);
		public List<Meeting> findByTimeSlot(String timeSlot);
		public List<User> findAbsents(int meetingId);
		public List<User> findPresents(int meetingId);

}

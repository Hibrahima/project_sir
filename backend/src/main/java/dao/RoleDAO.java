package dao;

import java.io.IOException;
import java.util.List;

import models.Role;

public interface RoleDAO {
	
	public Role getRoleById(int id);
	public Role createRole(Role role) throws IOException;
	public List<Role> getAllRole();
	public Role updateRole(Role role) throws IOException;
	public Role deleteRole(int id) throws IOException;
	public Role findByName(String name);
}

package dao;

import java.io.IOException;
import java.util.List;

import org.apache.commons.mail.EmailException;

import dto.ParticipationDTO;
import exceptions.ExistingObjectException;
import exceptions.OperationNotAllowedException;
import models.CustomDate;
import models.Participation;
import models.Survey;
import models.SurveyResult;
import models.TimeSlot;
import models.User;
import utils.SurveyMeetingPostObject;
import utils.SurveyValidationData;

public interface SurveyDAO {
	//public void createSurvey(Survey s);
	public Survey update(SurveyMeetingPostObject s) throws IOException;
	public List<Survey> getAll();
	public List<Survey> getUserSurveys(int userId);
	public Survey getById(int id);
	public CustomDate addDate(int surveyId ,int dateId)throws ExistingObjectException, IOException, OperationNotAllowedException;
	public CustomDate removeDate(int surveyId, int dateId)throws IllegalArgumentException, IOException;
	public List<Survey> findByCreationDate(String date);
	public List<Survey> findByCustomDate(String date);
	public List<Survey> findByStatus(String status);
	public List<Survey> findByCustomDateAndTimeSlot(String date,int begin, int end);
	public List<ParticipationDTO> getSurveyParticipations(int surveyId);
	public List<User> getParticipants(int surveyId);
	public Survey validate(SurveyValidationData data) throws ExistingObjectException, IOException, OperationNotAllowedException, EmailException;
	public List<TimeSlot> getAllTimeSlotsByParticipation(int surveyId);
	public List<TimeSlot> getAllTimeSlotsByParticipationOderedByScore(int surveyId);
	public User getSurveyCreator(int surveyId);
	public SurveyResult getResult(int surveyId);
	public void sendEmail(int surveyId) throws EmailException, Exception;

}

package dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import exceptions.ExistingObjectException;
import exceptions.OperationNotAllowedException;
import models.Allergy;
import models.Meeting;
import models.Participation;
import models.Role;
import models.Survey;
import models.User;
import utils.LoginData;
import utils.SurveyMeetingPostObject;

public interface UserDAO {

	public User createUser(User user) throws IOException;
	public User findUserById(int id); 
	public User updateUser(User user) throws IOException, EntityNotFoundException;
	public List<User> getAll();
	//public List<Meeting> getMeetings();
	public Role addNewRole(int userId, Role role) throws ExistingObjectException, IOException;
	public Role addPredefinedRole(int userId, int roleId) throws ExistingObjectException, IOException;
	public Role removeRole(int userId, int roleId) throws IllegalArgumentException, IOException;
	public List<User> findByRoleName(String roleName);
	public List<User> findByAllergyName(String allergyName);
	//public List<User> findByPreferenceName(String preferenceName);
	public Allergy addAllergy(int userId, Allergy a) throws ExistingObjectException, IOException ;
	public SurveyMeetingPostObject addCreatedSurvey(int userId, SurveyMeetingPostObject object) throws ExistingObjectException, IOException,  OperationNotAllowedException;
	//public void addParticipation(int userId, int surveyId, Participation p) throws ExistingObjectException;
	
	public User registerUser(User user) throws ExistingObjectException, IOException;
	public List<Survey> findCreatedSurveysOnDate(String date, int userId);
	public List<Participation> findParticipationsOnDate(String date, int userId);
	public List<Participation> findParticipations(int userId);
	public User login(LoginData data) throws IOException;
	public List<Meeting> getAttendedMettings(int userId) throws IOException;
	public List<Meeting> getMissedMettings(int userId) throws IOException;

}

package dao;

import java.io.IOException;
import java.util.List;

import exceptions.OperationNotAllowedException;
import models.TimeSlot;

public interface TimeSlotDAO {
	
	public TimeSlot create(TimeSlot t) throws IOException, OperationNotAllowedException;
	public TimeSlot getById(int id) throws IOException;
	public TimeSlot update(TimeSlot t) throws IOException, OperationNotAllowedException;
	public TimeSlot delete(int id) throws IOException, OperationNotAllowedException;
	public List<TimeSlot> getAll();
	public List<TimeSlot> getAllByState(boolean active);
	public List<TimeSlot> getAllOrderedByScore();

}

package models;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;
import enums.SurveyStatus;
import exceptions.ExistingObjectException;
import exceptions.OperationNotAllowedException;
import utils.Utils;

@Entity
@Table(name = "survey")
@NamedQueries({
	@NamedQuery(name="Survey.findAll", query="select s from Survey s"),
	@NamedQuery(name="Survey.findByCreationDate", query="select s from Survey s where s.creationDate=:date"),
	@NamedQuery(name="Survey.findByStatus", query="select s from Survey s where s.status=:status"),
	@NamedQuery(name="Survey.findByCustomDate", query="select s from Survey s join s.dates as d where d.date=:date"),
	@NamedQuery(name="Survey.findByCustomDateAndTimeSlot", 
		query="select s from Survey s join s.dates as d join d.timeSlots as ts where d.date=:date and ts.begin=:begin and ts.end=:end"),
	@NamedQuery(name="Survey.findAllParticipations", query="select p from Participation p where p.id.surveyId=:surveyId"),
	@NamedQuery(name="Survey.findParticipants", query="select u from User u where u.id in (select p.id.userId from Participation p where p.id.surveyId=:surveyId)"),
	@NamedQuery(name="Survey.findAllTimeSlotsByParticipation", query="select t from Participation p join p.timeSlots t where p.id.surveyId =:surveyId"),
	@NamedQuery(name="Survey.findAllTimeSlotsByParticipationOderedByScore", 
		query="select t from Participation p join p.timeSlots t where p.id.surveyId =:surveyId order by t.score desc"),
	@NamedQuery(name="Survey.findResult", query="select r from Survey s join s.finalResult r where s.id=:surveyId")
})
public class Survey {

	private int id;
	private String name;
	private LocalDate creationDate = LocalDate.now();
	private LocalTime creationTime = LocalTime.now();
	private String link;
	private SurveyStatus status = SurveyStatus.DEFAULT;
	private User createdBy;
	private Meeting meeting;
	private SurveyResult finalResult;
	private Collection<CustomDate> dates;

	public Survey() {
		this.dates = new ArrayList<CustomDate>();
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		Objects.requireNonNull(name, "name cannot be null");
		this.name = name;
	}

	@Column(name="creation_date")
	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		Objects.requireNonNull(creationDate, "creation date cannot be null");
		this.creationDate = creationDate;
	}

	@Column(name="creation_time")
	public LocalTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalTime creationTime) {
		Objects.requireNonNull(creationTime, "creation time cannot be null");
		this.creationTime = creationTime;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		// Objects.requireNonNull(link, "link cannot be null");
		this.link = link;
	}

	public SurveyStatus getStatus() {
		return status;
	}

	public void setStatus(SurveyStatus status) {
		Objects.requireNonNull(status, "status cannot be null");
		this.status = status;
	}
	
	
	@ManyToOne()
	@JsonBackReference(value="created_surveys")
	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE}, mappedBy = "survey")
	@JsonManagedReference(value = "meeting")
	public Meeting getMeeting() {
		return meeting;
	}

	public void setMeeting(Meeting meeting) {
		if (meeting == null)
			return;
		this.meeting = meeting;
		this.meeting.setSurvey(this);
	}

	@OneToOne(cascade = CascadeType.PERSIST, mappedBy = "survey")
	@JsonManagedReference(value = "result")
	public SurveyResult getFinalResult() {
		return finalResult;
	}

	public void setFinalResult(SurveyResult finalResult) {
		if (finalResult == null)
			return;
		this.finalResult = finalResult;
		this.finalResult.setSurvey(this);
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "survey_id")
	public List<CustomDate> getDates() {
		return Utils.cloneCollection(dates);
	}

	public void setDates(Collection<CustomDate> dates) {
		Objects.requireNonNull(dates, "dates cannot be null");
		this.dates = dates;
	}


	public void addDate(CustomDate d) throws ExistingObjectException, OperationNotAllowedException {
		Objects.requireNonNull(d, "choice cannot be null");
		if (this.dates.contains(d))
			throw new ExistingObjectException(this.name + " has already this choice");
		
		if(!d.isActive())
			throw new OperationNotAllowedException("Inactive date cannot be added to any survey");

		this.dates.add(d);
		d.setActive(false);
		this.status = SurveyStatus.READY;
	}

	
	public void removeDate(CustomDate d) throws IllegalArgumentException {
		Objects.requireNonNull(d, "choice cannot be null");
		if (!this.dates.contains(d))
			throw new IllegalArgumentException(this.name + " has already this choice");
		
		this.dates.remove(d);
		d.setActive(true);
		
		// if there is any date make the status default
		if(this.dates.size()==0)
			this.status = SurveyStatus.DEFAULT;
		
	}
	

}

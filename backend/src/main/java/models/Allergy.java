package models;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="allergy")
public class Allergy {
	
	private int id;
	private String name;
	
	public Allergy() {
		
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		Objects.requireNonNull(name, "name cannot be null");
		this.name = name;
	}
	
	

}

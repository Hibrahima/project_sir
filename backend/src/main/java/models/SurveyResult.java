package models;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name="survey_result")
public class SurveyResult {
	
	private int id;
	private TimeSlot timeSlot;
	private Survey survey;
	
	public SurveyResult() {
		
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		this.id = id;
	}
	
	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="time_slot_id", unique=true)
	public TimeSlot getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(TimeSlot timeSlot) {
		//Objects.requireNonNull(timeSlot, "time slot cannot be null");
		if(timeSlot == null)
			return;
		this.timeSlot = timeSlot;
	}
	

	@OneToOne
	@JoinColumn(unique=true)
	@JsonBackReference(value="result")
	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		//Objects.requireNonNull(survey, "survey cannot be null");
		if(survey == null)
			return;
		this.survey = survey;
	}
	
}

package models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FoodCompoundKey implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	private int userId;
	private int meetingId;
	
	public FoodCompoundKey() {
		
	}
	
	public FoodCompoundKey(int userId, int meetingId) {
		this.userId = userId;
		this.meetingId = meetingId;
	}
	
	@Column(name="user_id")
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	@Column(name="meeting_id")
	public int getMeetingId() {
		return meetingId;
	}
	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FoodCompoundKey other = (FoodCompoundKey) obj;
		if (meetingId != other.meetingId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + meetingId;
		result = prime * result + userId;
		return result;
	}
	
	

}

package models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import exceptions.ExistingObjectException;
import exceptions.OperationNotAllowedException;
import helper.CustomDateDeserializer;
import utils.Utils;

@Entity
@Table(name="custom_date")
@NamedQueries({
	@NamedQuery(name="CustomDate.findAll", query="select c from CustomDate c"),
	@NamedQuery(name="CustomDate.findByDate", query="select c from CustomDate c where c.date=:date"),
	@NamedQuery(name="CustomDate.findByName", query="select c from CustomDate c where c.name=:name"),
	@NamedQuery(name="CustomDate.findByStatus", query="select c from CustomDate c where c.active=:active")
})
public class CustomDate {

	private int id;
	private String name;
	private LocalDate date;	
	private boolean active = true;
	private Collection<TimeSlot> timeSlots;
	
	public CustomDate() {
		this.timeSlots = new ArrayList<TimeSlot>();
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		Objects.requireNonNull(name, "name cannot be null");
		this.name = name;
	}
	
	@JsonDeserialize(using=CustomDateDeserializer.class)
	//@JsonSerialize(using = CustomDateSerializer.class)
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy="date")
	@JsonManagedReference(value="date")
	public Collection<TimeSlot> getTimeSlots() {
		return Utils.cloneCollection(timeSlots);
	}

	public void setTimeSlots(Collection<TimeSlot> timeSlots) {
		Objects.requireNonNull(timeSlots, "time slots cannot be null");
		this.timeSlots = timeSlots;
	}
	

	public void addTimeSlot(TimeSlot t) throws ExistingObjectException, OperationNotAllowedException {
		Objects.requireNonNull(t, "time slot cannot be null");
		if(this.timeSlots.contains(t))
			throw new ExistingObjectException("this time slot is already available in  this date : "+this.date);
		if(!t.isActive())
			throw new OperationNotAllowedException("Inactive time slot cannot be added to any date");
		
		this.timeSlots.add(t);
		t.setDate(this);
		t.setActive(false);
	}

	public void addTimeSlotI(TimeSlot t) throws  OperationNotAllowedException {
		Objects.requireNonNull(t, "time slot cannot be null");
		
		if(!t.isActive())
			throw new OperationNotAllowedException("Inactive time slot cannot be added to any date");
		
		this.timeSlots.add(t);
		t.setDate(this);
		t.setActive(false);
	}
	
	public void removeTimeSlot(TimeSlot t) throws IllegalArgumentException {
		Objects.requireNonNull(t, "time slot cannot be null");
		if(!this.timeSlots.contains(t))
			throw new IllegalArgumentException("this time slot is not attached to the this date : "+this.date);
		
		this.timeSlots.remove(t);
		t.setDate(null);
		t.setActive(true);
	}
	
}

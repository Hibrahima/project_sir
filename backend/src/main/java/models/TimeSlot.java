package models;

import java.time.LocalTime;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import helper.CustomTimeDeserializer;

@Entity
@Table(name="time_slot")
@NamedQueries({
	@NamedQuery(name="TimeSlot.findAll", query="select t from TimeSlot t"),
	@NamedQuery(name="TimeSlot.findAllOrderedByScore", query="select t from TimeSlot t order by t.score desc"),
	@NamedQuery(name="TimeSlot.findByState", query="select t from TimeSlot t where t.active=:state")
})
public class TimeSlot {
	
	private int id;
	private LocalTime begin;
	private LocalTime end;
	private int score = 0;
	private boolean active = true;
	private CustomDate date;
	
	public TimeSlot() {
		
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		this.id = id;
	}

	@JsonDeserialize(using = CustomTimeDeserializer.class)
	public LocalTime getBegin() {
		return begin;
	}

	public void setBegin(LocalTime begin) {
		Objects.requireNonNull(begin, "begin cannot be null");
		this.begin = begin;
	}

	@JsonDeserialize(using = CustomTimeDeserializer.class)
	public LocalTime getEnd() {
		return end;
	}

	public void setEnd(LocalTime end) {
		Objects.requireNonNull(end, "end cannot be null");
		this.end = end;
	}
	
	

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JsonBackReference(value="date")
	public CustomDate getDate() {
		return date;
	}

	public void setDate(CustomDate date) {
		this.date = date;
	}
	
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		Objects.requireNonNull(score, "score cannot be null");
		this.score = score;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		Objects.requireNonNull(active, "active cannot be null");
		this.active = active;
	}

	public void incrementScore() {
		this.score++;
	}
	
}

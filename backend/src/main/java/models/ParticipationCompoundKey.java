package models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ParticipationCompoundKey implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int userId;
	private int surveyId;
	
	public ParticipationCompoundKey() { 
		
	}
	
	public ParticipationCompoundKey(int userId, int surveyId){
		this.userId = userId;
		this.surveyId = surveyId;
	}
	
	@Column(name="user_id")
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	@Column(name="survey_id")
	public int getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + surveyId;
		result = prime * result + userId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticipationCompoundKey other = (ParticipationCompoundKey) obj;
		if (surveyId != other.surveyId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
	
	
	

}

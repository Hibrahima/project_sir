package models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;

import exceptions.ExistingObjectException;
import utils.Utils;

@Entity
@Table(name="user")
@NamedQueries({
	@NamedQuery(name="User.findAll", query="SELECT u FROM User u order by u.lastName"),
	@NamedQuery(name="User.findByRole", query="select u from User u join u.roles as r where r.name = :roleName"),
	@NamedQuery(name="User.findByAllergy", query="select u from User u join u.allergies as a where a.name = :allergyName"),
	@NamedQuery(name="User.findCreatedSurveysOnDate", query="select s from User u join u.createdSurveys as s where s.creationDate=:date and u.id=:userId"),
	@NamedQuery(name="User.findParticipationsOnDate", query="select p from Participation p where p.participationDate=:date and p.id.userId=:userId"),
	@NamedQuery(name="User.findParticipations", query="select p from Participation p where p.id.userId=:userId"),
	@NamedQuery(name="User.findUserMemberOfMeeting", 
			query="select u from User u where u.id in (select p.id.userId from Participation p "
					+ "join p.timeSlots t where p.id.surveyId=:surveyId and t.id=:timeId)"),
	@NamedQuery(name="User.findUserNotMemberOfMeeting", 
			query="select u from User u where u.id not in (select p.id.userId from Participation p "
			+ "join p.timeSlots t where p.id.surveyId=:surveyId and t.id=:timeId)"),
	@NamedQuery(name="User.findUserMemberOfMeeting2", 
			query="select m from Meeting m where m.survey.id = (select p.id.surveyId from Participation p "
					+ "join p.timeSlots t where t.id=:timeId)"),
	@NamedQuery(name="User.login", query="select u from User u where u.email=:email and u.password=:password")
})
public class User {
	
	private int id;
	private String lastName;
	private String  firstName;
	private String email;
	private String password;
	private Collection<Role> roles;
	private Collection<Allergy> allergies;
	private Collection<Survey> createdSurveys;
	private Collection<Meeting> attendedMeetings;
	private Collection<Meeting> missedMeetings;
	
	public User() {
		this.roles = new ArrayList<Role>();
		this.createdSurveys = new ArrayList<Survey>();
		this.allergies = new ArrayList<Allergy>();
		this.attendedMeetings = new HashSet<>();
		this.missedMeetings = new HashSet<>();
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name="first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(unique= true, nullable=false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToMany(cascade = {CascadeType.PERSIST}, fetch=FetchType.EAGER)
	@JoinTable(name = "user_role",
	           joinColumns = { @JoinColumn(name = "user_id") }, 
	           inverseJoinColumns = { @JoinColumn(name = "role_id") })
	public List<Role> getRoles() {
		List<Role> rolesCopy = Utils.cloneCollection(this.roles);
		return rolesCopy;
	}
	
	public void setRoles(Collection<Role> roles) {
		Objects.requireNonNull(roles, "roles cannot be null");
		this.roles = roles;
	}

	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="user_id")
	public List<Allergy> getAllergies() {
		List<Allergy> copyAllergies = Utils.cloneCollection(this.allergies);
		return copyAllergies;
	}
	
	public void setAllergies(Collection<Allergy> allergies) {
		Objects.requireNonNull(allergies, "allergies cannot be null");
		this.allergies = allergies;
	}

	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy="createdBy")
	@JsonManagedReference(value="created_surveys")
	//@JoinColumn(name="user_id")
	public List<Survey> getCreatedSurveys() {
		List<Survey> copySurveys = Utils.cloneCollection(this.createdSurveys);
		return copySurveys;
	}
	
	public void setCreatedSurveys(Collection<Survey> surveys) {
		Objects.requireNonNull(surveys, "surveys cannot be null");
		this.createdSurveys = surveys;
	}

	
	
	@ManyToMany(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name="user_meeting_presents",
	joinColumns=@JoinColumn(name="user_id"),
	inverseJoinColumns=@JoinColumn(name="meeting_id"))
	@JsonIgnore
	//@JsonManagedReference(value="user")
	public Collection<Meeting> getAttendedMeetings() {
		return Utils.cloneCollection(attendedMeetings);
	}

	public void setAttendedMeetings(Collection<Meeting> attendedMeetings) {
		Objects.requireNonNull(attendedMeetings, "attended meeting cannot be null");
		this.attendedMeetings = attendedMeetings;
	}
	
	@ManyToMany(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name="user_meeting_absents",
	joinColumns=@JoinColumn(name="user_id"),
	inverseJoinColumns=@JoinColumn(name="meeting_id"))
	@JsonIgnore
	//@JsonManagedReference(value="user")
	public Collection<Meeting> getMissedMeetings() {
		return Utils.cloneCollection(missedMeetings);
	}

	public void setMissedMeetings(Collection<Meeting> missedMeetings) {
		Objects.requireNonNull(missedMeetings, "missedMeetings meeting cannot be null");
		this.missedMeetings = missedMeetings;
	}


	public void addRole(Role role) throws ExistingObjectException{
		Objects.requireNonNull(role, "role cannot be null");
		if(this.roles.contains(role))
			throw new ExistingObjectException(this.firstName+" has already this role : "+role.getName());
		
		this.roles.add(role);
	}
	
	public void removeRole(Role role) throws IllegalArgumentException{
		Objects.requireNonNull(role, "role cannot be null");
		if(!this.roles.contains(role))
			throw new IllegalArgumentException(this.firstName+" does not have this role : "+role.getName());
		
		this.roles.remove(role);
	}
	public void addAllergy(Allergy a) throws ExistingObjectException{
		Objects.requireNonNull(a, "allergy cannot be null");
		if(this.allergies.contains(a))
			throw new ExistingObjectException(this.firstName+" is already allergic to "+a.getName());
		
		this.allergies.add(a);
	}
	
	public void addSurvey(Survey s) throws ExistingObjectException{
		Objects.requireNonNull(s, "survey cannot be null");
		if(this.createdSurveys.contains(s))
			throw new ExistingObjectException(this.firstName+" has already cxreated this survey "+s.getName());
		
		this.createdSurveys.add(s);
		s.setCreatedBy(this);
	}

	
	public void addAttendedMeeting(Meeting m) throws ExistingObjectException {
		Objects.requireNonNull(m, "meeting cannot be null");
		if(this.attendedMeetings.contains(m))
			throw new ExistingObjectException(this.firstName+" has already attented this meeting : "+m.getTitle());
		
		this.attendedMeetings.add(m);
		m.addPresent(this);
			
	}
	
	public void addMissedMeeting(Meeting m) throws ExistingObjectException {
		Objects.requireNonNull(m, "meeting cannot be null");
		if(this.missedMeetings.contains(m))
			throw new ExistingObjectException(this.firstName+" has already misseed this meeting : "+m.getTitle());
		
		this.missedMeetings.add(m);
		m.addAbsent(this);
			
	}

}

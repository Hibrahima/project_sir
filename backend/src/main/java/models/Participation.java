package models;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import exceptions.ExistingObjectException;
import utils.Utils;

@Entity
@Table(name="participation")
@NamedQueries({
	@NamedQuery(name="Participatiom.findAll", query="select p from Participation p"),
	@NamedQuery(name="Participation.findAllOrderedByScore", query="select p from Participation p join p.timeSlots as ts order by ts.score desc"),
	@NamedQuery(name="Participation.findByDate", query="select p from Participation p where p.participationDate=:date")
})
public class Participation {
	
	private ParticipationCompoundKey id;
	private LocalDate participationDate = LocalDate.now();
	private LocalTime participationTime = LocalTime.now();
	private Collection<TimeSlot> timeSlots;
	
	public Participation() {
		this.timeSlots = new ArrayList<TimeSlot>();
	}

	
	@EmbeddedId
	public ParticipationCompoundKey getId() {
		return id;
	}

	public void setId(ParticipationCompoundKey id) {
		this.id = id;
	}
	
	@Column(name="participation_date")
	public LocalDate getParticipationDate() {
		return participationDate;
	}

	public void setParticipationDate(LocalDate participationDate) {
		this.participationDate = participationDate;
	}
	
	@Column(name="participation_time")
	public LocalTime getParticipationTime() {
		return participationTime;
	}

	public void setParticipationTime(LocalTime participationTime) {
		this.participationTime = participationTime;
	}
	
	
	@ManyToMany(cascade = {CascadeType.PERSIST}, fetch=FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name="participation_user_id", referencedColumnName="user_id"),
		@JoinColumn(name="participation_survey_id", referencedColumnName="survey_id")
	})
	public Collection<TimeSlot> getTimeSlots() {
		return Utils.cloneCollection(timeSlots);
	}

	public void setTimeSlots(Collection<TimeSlot> timeSlots) {
		Objects.requireNonNull(timeSlots, "time slots cannot be null");
		this.timeSlots = timeSlots;
	}
	
	
	public void addTimeSlot(TimeSlot t) throws ExistingObjectException {
		Objects.requireNonNull(t, "choice cannot be null");
		//if(this.timeSlots.contains(t))
		//	throw new ExistingObjectException("this choice is already available in this participation");
		
		this.timeSlots.add(t);
		t.incrementScore();
	}
	

}

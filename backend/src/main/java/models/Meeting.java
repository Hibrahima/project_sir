package models;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;

import exceptions.ExistingObjectException;
import utils.Utils;

@Entity
@Table(name="meeting")
@NamedQueries({
	@NamedQuery(name="Meeting.findAll", query="select m from Meeting m"),
	@NamedQuery(name="Meeting.findByTitle", query="select m from Meeting m where m.title=:title"),
	@NamedQuery(name="Meeting.findByDate", query="select m from Meeting m where m.date=:date"),
	@NamedQuery(name="Meeting.findByOfficeAddress", query="select m from Meeting m where m.officeAddress=:address"),
	@NamedQuery(name="Meeting.findByTimeSlot", query="select m from Meeting m where m.timeSlot=:slot"),
})
public class Meeting {
	
	private int id;
	private String title;
	private String summary;
	private String officeAddress;
	private String timeSlot;
	private LocalDate date;
	private Survey survey;
	private Collection<User> presents;
	private Collection<User> absents;
	
	public Meeting() {
		this.presents = new HashSet<>();
		this.absents = new HashSet<>();
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		this.id = id;
	}

	@Column(unique=true)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		Objects.requireNonNull(title, "title cannot be null");
		this.title = title;
	}
	
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	@Column(name="office_address")
	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}

	@OneToOne
	@JoinColumn(unique=true)
	@JsonBackReference(value="meeting")
	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		//Objects.requireNonNull(survey, "survey cannot be null");
		if (survey == null)
			return;
		this.survey = survey;
	}
	
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	@Column(name="time_slot")
	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}
	

	@ManyToMany(mappedBy="attendedMeetings")
	@JsonIgnore
	//@JsonManagedReference(value="user")
	public Collection<User> getPresents() {
		return Utils.cloneCollection(presents);
	}

	public void setPresents(Collection<User> users) {
		Objects.requireNonNull(users, "presents cannot be null");
		this.presents = users;
	}
	
	@ManyToMany(mappedBy="missedMeetings")
	@JsonIgnore
	//@JsonManagedReference(value="user")
	public Collection<User> getAbsents() {
		return Utils.cloneCollection(absents);
	}

	public void setAbsents(Collection<User> absents) {
		Objects.requireNonNull(absents, "absents cannot be null");
		this.presents = absents;
	}
	
	public void addPresent(User u) throws ExistingObjectException{
		Objects.requireNonNull(u, "user cannot be null");
		if(this.presents.contains(u))
			throw new ExistingObjectException("this user has already attened this meeting : "+u.getFirstName());
		
		this.presents.add(u);
	}
	
	public void addAbsent(User u) throws ExistingObjectException{
		Objects.requireNonNull(u, "user cannot be null");
		if(this.absents.contains(u))
			throw new ExistingObjectException("this user has already missed this meeting : "+u.getFirstName());
		
		this.absents.add(u);
	}
       
}

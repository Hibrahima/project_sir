package models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="role")
@NamedQueries({
	@NamedQuery(name="Role.findAll", query="select r from Role r"),
	@NamedQuery(name="Role.findByName", query="select r from Role r where r.name=:roleName")
})
public class Role {
	
	private int id;
	private String name;
	
	public Role() {
		
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		this.id = id;
	}

	@Column(nullable=false, unique=true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		Objects.requireNonNull(name, "name cannot be null");
		this.name = name;
	}


}

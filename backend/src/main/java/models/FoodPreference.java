package models;

import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="food_preference")
@NamedQueries({
	@NamedQuery(name="Preference.findAll", query="select p from FoodPreference p"),
	@NamedQuery(name="Preference.findByName", query="select p from FoodPreference p where p.name=:prefName"),
	@NamedQuery(name="Preference.findByUserAndMeeting", 
				query="select p from FoodPreference p where p.id.userId=:userId and p.id.meetingId=:meetingId"),
	@NamedQuery(name="Preference.findByUser", query="select p from FoodPreference p where p.id.userId=:userId"),
	@NamedQuery(name="Preference.findByMeeting", query="select p from FoodPreference p where p.id.meetingId=:meetingId"),
	@NamedQuery(name="Preference.findUserByPrefName", 
		query="select u from User u where u.id = (select f.id.userId from FoodPreference f where f.name=:prefName)")	
})
public class FoodPreference {

	private FoodCompoundKey id;
	private String name;
	private String description;
	
	public FoodPreference() {
		
	}
	
	@EmbeddedId
	public FoodCompoundKey getId() {
		return id;
	}

	public void setId(FoodCompoundKey id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		Objects.requireNonNull(name, "name cannot be null");
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		Objects.requireNonNull(description, "description cannot be null");
		this.description = description;
	}
	
	
}

package exceptions;

public class ExistingObjectException extends Exception {

	private static final long serialVersionUID = 1L;
	private String message;
	
	public ExistingObjectException(String message) {
		this.message = message;
		System.out.println(this.message);
	}
	
	public String getMessage() {
		return this.message;
	}

}

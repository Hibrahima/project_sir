package exceptions;

public class OperationNotAllowedException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private String message;
	
	public OperationNotAllowedException(String message) {
		this.message = message;
		System.out.println(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}

package security;

public class PasswordHasher {

	private static UpdatableHasher hasher = new UpdatableHasher(11);

	public static String hashPassword(String plainPassword) {
		return hasher.hash(plainPassword);
	}
	
	public static boolean verifyPasswords(String plainPassword, String hash) {
		return hasher.verifyHash(plainPassword, hash);
	}

}

package utils;

import java.util.ArrayList;
import java.util.Collection;
import models.CustomDate;


public class SurveyMeetingPostObject {
	
	private int surveyId;
	private String surveyName;
	private String meetingTitle;
	private String meetingOfficeAddress;
	private String meetingSummary;
	private Collection<CustomDate> dates;
	
	public SurveyMeetingPostObject() {
		this.dates = new ArrayList<>();
	}

	public SurveyMeetingPostObject(String surveyName, String meetingTitle, String meetingOfficeAddress) {
		this.surveyName = surveyName;
		this.meetingTitle = meetingTitle;
		this.meetingOfficeAddress = meetingOfficeAddress;
	}

	public String getSurveyName() {
		return surveyName;
	}

	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	public String getMeetingTitle() {
		return meetingTitle;
	}

	public void setMeetingTitle(String meetingTitle) {
		this.meetingTitle = meetingTitle;
	}

	public String getMeetingOfficeAddress() {
		return meetingOfficeAddress;
	}

	public void setMeetingOfficeAddress(String meetingOfficeAddress) {
		this.meetingOfficeAddress = meetingOfficeAddress;
	}

	public int getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}

	public String getMeetingSummary() {
		return meetingSummary;
	}

	public void setMeetingSummary(String summary) {
		this.meetingSummary = summary;
	}

	public Collection<CustomDate> getDates() {
		return dates;
	}


	public void setDates(Collection<CustomDate> customDates) {
		this.dates = customDates;
	}


	
	

}

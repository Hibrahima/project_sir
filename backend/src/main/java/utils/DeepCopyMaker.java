package utils;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;

public class DeepCopyMaker<T> {
	
	public static  <T>  T deepCopy(T object) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
	     
	    T deepCopy = (T) objectMapper.readValue(objectMapper.writeValueAsString(object), object.getClass());
	    return deepCopy;
	}

}

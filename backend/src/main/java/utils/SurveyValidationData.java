package utils;

public class SurveyValidationData {
	
	private int surveyId;
	private int timeSlotId;
	
	public SurveyValidationData() {

	}

	public SurveyValidationData(int surveyId, int timeSlotId) {
		this.surveyId = surveyId;
		this.timeSlotId = timeSlotId;
	}

	public int getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}

	public int getTimeSlotId() {
		return timeSlotId;
	}

	public void setTimeSlotId(int timeSlotId) {
		this.timeSlotId = timeSlotId;
	}
	
}

package utils;


public class StaticResourceProvider {
	
	public static String dateFormat = "yyyy-MM-dd";
	public static String timeFormat = "HH:mm:ss";
	public static final String DEFAULT_SERVER_PORT = "localhost:4200";

}

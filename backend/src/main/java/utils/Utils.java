package utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Utils {
	
	public static <T> List<T> cloneCollection(Collection<T> c){
		List<T> result = new ArrayList<T>();
		result.addAll(c);
		return result;
	}

}

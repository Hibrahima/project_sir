package dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import helper.CustomTimeDeserializer;


public class TimeSlotDTO {
	
	private int id;
	private LocalTime begin;
	private LocalTime end;
	private int score = 0;
	private boolean active = true;
	private LocalDate date;
	
	public TimeSlotDTO() {
		
	}

	public TimeSlotDTO(int id, LocalTime begin, LocalTime end, int score, boolean active, LocalDate date){
		this.id = id;
		this.begin = begin;
		this.end = end;
		this.score = score;
		this.date = date;
		this.active = active;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		Objects.requireNonNull(id, "id cannot be null");
		this.id = id;
	}

	@JsonDeserialize(using = CustomTimeDeserializer.class)
	public LocalTime getBegin() {
		return begin;
	}

	public void setBegin(LocalTime begin) {
		Objects.requireNonNull(begin, "begin cannot be null");
		this.begin = begin;
	}

	@JsonDeserialize(using = CustomTimeDeserializer.class)
	public LocalTime getEnd() {
		return end;
	}

	public void setEnd(LocalTime end) {
		Objects.requireNonNull(end, "end cannot be null");
		this.end = end;
	}
	
	
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		Objects.requireNonNull(score, "score cannot be null");
		this.score = score;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		Objects.requireNonNull(active, "active cannot be null");
		this.active = active;
	}
	
}

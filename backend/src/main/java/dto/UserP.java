package dto;

public class UserP {
	
	private boolean hasParticipated = false;
	
	public UserP() {
		
	}
	
	public UserP(boolean hasParticipated) {
		this.hasParticipated = hasParticipated;
	}

	public boolean isHasParticipated() {
		return hasParticipated;
	}

	public void setHasParticipated(boolean hasParticipated) {
		this.hasParticipated = hasParticipated;
	}

}

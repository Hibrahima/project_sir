package dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import models.ParticipationCompoundKey;
import utils.Utils;


public class ParticipationDTO {
	
	private ParticipationCompoundKey id;
	private LocalDate participationDate = LocalDate.now();
	private LocalTime participationTime = LocalTime.now();
	private Collection<TimeSlotDTO> timeSlots;
	
	public ParticipationDTO() {
		this.timeSlots = new ArrayList<TimeSlotDTO>();
	}

	public ParticipationDTO(ParticipationCompoundKey key, LocalDate date, LocalTime time){
		this.id = key;
		this.participationDate = date;
		this.participationTime = time;
		this.timeSlots = new ArrayList<>();
	}

	
	
	public ParticipationCompoundKey getId() {
		return id;
	}

	public void setId(ParticipationCompoundKey id) {
		this.id = id;
	}
	
	
	public LocalDate getParticipationDate() {
		return participationDate;
	}

	public void setParticipationDate(LocalDate participationDate) {
		this.participationDate = participationDate;
	}
	
	
	public LocalTime getParticipationTime() {
		return participationTime;
	}

	public void setParticipationTime(LocalTime participationTime) {
		this.participationTime = participationTime;
	}
	

	public Collection<TimeSlotDTO> getTimeSlots() {
		return timeSlots;
	}

	public void setTimeSlots(Collection<TimeSlotDTO> timeSlots) {
		Objects.requireNonNull(timeSlots, "time slots cannot be null");
		this.timeSlots = timeSlots;
	}
	

}

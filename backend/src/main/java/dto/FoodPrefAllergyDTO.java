package dto;

import models.Allergy;
import models.FoodPreference;

public class FoodPrefAllergyDTO {
	
	private FoodPreference pref;
	private Allergy allergy;
	
	public FoodPrefAllergyDTO() {
		// TODO Auto-generated constructor stub
	}

	public FoodPreference getPref() {
		return pref;
	}

	public void setPref(FoodPreference pref) {
		this.pref = pref;
	}

	public Allergy getAllergy() {
		return allergy;
	}

	public void setAllergy(Allergy allergy) {
		this.allergy = allergy;
	}
	

}

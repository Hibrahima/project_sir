import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeSlotListWrapperComponent } from './time-slot-list-wrapper.component';

describe('TimeSlotListWrapperComponent', () => {
  let component: TimeSlotListWrapperComponent;
  let fixture: ComponentFixture<TimeSlotListWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeSlotListWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSlotListWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { DateFormater } from './../../helper/date-formater';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'time-slot-list-wrapper',
  templateUrl: './time-slot-list-wrapper.component.html',
  styleUrls: ['./time-slot-list-wrapper.component.css']
})
export class TimeSlotListWrapperComponent implements OnInit {

  @Input('timeSlots') timeSlots;
  @Output('timeSlotId') timeSlotId = new EventEmitter<number>();
  dateFormater: DateFormater;

  constructor() { this.dateFormater = new DateFormater();}

  ngOnInit() {
  }

  formatTime(time:any){
    return this.dateFormater.formatTime(time);
  }

  formatActionTexts(status: boolean, textToDisplay: string) {
    if (status)
      return 'No action available';

    return textToDisplay;
  }

  formatStatus(status: boolean): string {
    if (status)
      return "Actif";

    return "Inactif";
  }

  deleteTimeSlot(id: number) {
    this.timeSlotId.emit(id);
  }

}

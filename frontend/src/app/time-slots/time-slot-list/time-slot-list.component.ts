import { Component, OnInit, OnDestroy } from '@angular/core';
import { TimeSlotService } from 'src/app/services/time-slot.service';
import { NotificationService } from 'src/app/services/notification.service';
import { Subscription } from 'rxjs';
import { Route, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-time-slot-list',
  templateUrl: './time-slot-list.component.html',
  styleUrls: ['./time-slot-list.component.css']
})
export class TimeSlotListComponent implements OnInit, OnDestroy {
  timeSlots;
  subsciption: Subscription;

  constructor(
    private timeSlotService: TimeSlotService,
    private notif: NotificationService,
    private router: Router) { }

  ngOnInit() {
    this.updateView();
  }


  ngOnDestroy(): void {
    this.subsciption.unsubscribe();
  }

  updateView() {
    this.subsciption = this.timeSlotService.getAll().subscribe((res: any) => {
      if (res.error) {
        this.notif.showError('Oups, something wne wrong', 'Unknown Error');
        return;
      }
      this.timeSlots = res;
    })
  }

  deleteTimeSlot(id: number) {
    let shouldIDelete: boolean = confirm('Are you sure you want to delete this time slot ?');
    if (shouldIDelete) {
      this.timeSlotService.delete(id).subscribe((result: any) => {
        if (result.error) {
          this.notif.showError('Oups, something went wrong! Error : ' + result.message, 'Unkown Error');
          return;
        }

        this.notif.showSuccess('This time slot was successfully deleted', 'Successful Deletion');
        this.updateView();
      },
        error => {
          this.notif.showError('Oups, something went wrong! Error : ' + error, 'Unkkown Error');
        });
    }

  }

}

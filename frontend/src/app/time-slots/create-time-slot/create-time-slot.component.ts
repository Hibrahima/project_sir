import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TimeSlotService } from '../../services/time-slot.service';
import { NotificationService } from '../../services/notification.service';
import { DataShaperService } from 'src/app/services/data-shaper.service';

@Component({
  selector: 'create-time-slot',
  templateUrl: './create-time-slot.component.html',
  styleUrls: ['./create-time-slot.component.css']
})
export class CreateTimeSlotComponent implements OnInit {
  @Output("emitter") emitter = new EventEmitter<any>();

  timeSlot = {
    id: undefined,
    begin: {
      hour: 8,
      minute: 0
    },
    end: {
      hour: 9,
      minute: 0
    },
  };
  id: number;

  constructor(
    private timeSlotService: TimeSlotService,
    private notif: NotificationService,
    private route: ActivatedRoute,
    private dataShaper: DataShaperService) {

    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      timeSlotService.getById(this.id).subscribe((t: any) => {
        this.timeSlot = {
          id: t.id,
          begin: t.begin,
          end: t.end
        }
      },
        error => {
          notif.showError("Oups, something went wrong!", "Unknown Error");
        })
    }
  }

  ngOnInit() {
  }

  /*formatTimeSlot(timeSlot: any) {
    let beginHour = timeSlot.begin.hour;
    let beginMinute = timeSlot.begin.minute;
    let endHour = timeSlot.end.hour;
    let endMinute = timeSlot.end.minute;
    // add  leading zeros
    if (beginHour < 10)
      beginHour = '0' + beginHour;

    if (beginMinute < 10)
      beginMinute = '0' + beginMinute;

    if (endHour < 10)
      endHour = '0' + endHour;

    if (endMinute < 10)
      endMinute = '0' + endMinute;

    return {
      id: timeSlot.id,
      begin: '' + beginHour + ':' + beginMinute + ':00', // 0 second by defaul
      end: '' + endHour + ':' + endMinute + ':00' // 0 second by defaul as well
    }
  }*/

  IsBeginGreaterThanEnd() {
    if (this.timeSlot.begin.hour > this.timeSlot.end.hour)
      return true;
    else if (this.timeSlot.begin.hour == this.timeSlot.end.hour) {
      if (this.timeSlot.begin.minute >= this.timeSlot.end.minute)
        return true;
    }

    return false;

  }


  persistTimeSlot() {
    if (this.IsBeginGreaterThanEnd()) {
      this.notif.showError('Begin must be less than end', 'Begin - End Error');
      return;
    }

    let formattedTimeSlot = this.dataShaper.formatTimeSlot(this.timeSlot);
    this.timeSlotService.create(formattedTimeSlot).subscribe((t: any) => {
      if (t.error) {
        this.notif.showError(t.message, 'Unknown Error');
        return;
      }
      this.notif.showSuccess('This time slot was successfully created!', 'Success');
    },
      error => {
        this.notif.showError('This time slot cannot be posted!', 'Unknown Error')
      })
  }

  updateTimeSlot() {
    if (this.IsBeginGreaterThanEnd()) {
      this.notif.showError('Begin must be less than end', 'Begin - End Error');
      return;
    }

    let formattedTimeSlot = this.dataShaper.formatTimeSlot(this.timeSlot);
    this.timeSlotService.update(formattedTimeSlot).subscribe((t: any) => {
      if (t.error) {
        this.notif.showError('Oups. something went wrong! Error : ' + t.message, 'Unknown Error');
        return;
      }
      this.notif.showSuccess('This time slot was sucessfully updated', 'Update');
    },
      error => {
        this.notif.showError('Oups. something went wrong!', 'Unknown Error');
      })
  }

  /*saveChanges() {
    if (this.id) {
      this.updateTimeSlot();
    } else {
      this.persistTimeSlot();
    }
  }*/

  saveChanges(){
    if (this.IsBeginGreaterThanEnd()) {
      this.notif.showError('Begin time must be less than end time', 'Begin - End Error');
      return;
    }
    let formattedTimeSlot = this.dataShaper.formatTimeSlot(this.timeSlot);
    this.emitter.emit(formattedTimeSlot);
  }

}

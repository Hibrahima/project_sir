import { AboutAppComponent } from './about-app/about-app.component';
import { CreateFoodPrefAllergyComponent } from './preferences-allergies/create/create.component';
import { UserAttendedMeetingsComponent } from './users/attended-meetings/attended-meetings.component';
import { UserParticipationListComponent } from './users/participations/participations.component';
import { AdminAuthGuard } from './services/admin-auth-guard.service';
import { NoRightComponent } from './no-right/no-right.component';
import { ValidationComponent } from './surveys/validation/validation.component';
import { MeetingDetailsComponent } from './meetings/meeting-details/meeting-details.component';
import { CreateParticipationComponent } from './surveys/participations/create-participation/create-participation.component';
import { TimeSlotListComponent } from './time-slots/time-slot-list/time-slot-list.component';
import { CreateTimeSlotComponent } from './time-slots/create-time-slot/create-time-slot.component';
import { SurveyListComponent } from './surveys/survey-list/survey-list.component';
import { LoginRegisterComponent } from './login-register/login-register.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUserComponent } from './about-user/about-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CreateSurveyComponent } from './surveys/create-survey/create-survey.component';
import { SurveyDetailsComponent } from './surveys/survey-details/survey-details.component';
import { DateListComponent } from './dates/date-list/date-list.component';
import { CreateDateComponent } from './dates/create-date/create-date.component';
import { AuthGuard } from './services/auth-guard.service';
import { LoginRedirectAuthGuard } from './services/login-redirect-auth.service';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { ParticipationListComponent } from './surveys/participations/participation-list/participation-list.component';
import { CreatorAuthGuard } from './services/creator-auth-guard.service';
import { UserMissedMeetingsComponent } from './users/missed-meetings/missed-meetings.component';
import { ParticipantListComponent } from './surveys/participant-list/participant-list.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'login-register', component: LoginRegisterComponent, canActivate: [LoginRedirectAuthGuard] },
 // { path: 'login', component: LoginComponent },
//{ path: 'register', component: CreateAccountComponent },
  { path: 'users/:id/details', component: UserDetailsComponent, canActivate: [AuthGuard, AdminAuthGuard]},
  { path: 'users/:id/created-surveys', component: SurveyListComponent, canActivate: [AuthGuard]},
  { path: 'users/:id/participations', component: UserParticipationListComponent, canActivate: [AuthGuard]},
  { path: 'users/:id/attended-meetings', component: UserAttendedMeetingsComponent, canActivate: [AuthGuard]},
  { path: 'users/:id/missed-meetings', component: UserMissedMeetingsComponent, canActivate: [AuthGuard]}, 
  { path: 'users/meetings/:id/allergies-preferences', component: CreateFoodPrefAllergyComponent, canActivate: [AuthGuard]},
  { path: 'users/update-user/:id', component: UpdateUserComponent, canActivate: [AuthGuard, AdminAuthGuard] },
  { path: 'users', component: AboutUserComponent, canActivate: [AuthGuard, AdminAuthGuard] },
  { path: 'meetings/:id/details', component: MeetingDetailsComponent, canActivate: [AuthGuard]},
  { path: 'surveys', component: SurveyListComponent, canActivate: [AuthGuard] },
  { path: 'surveys/:id/details', component: SurveyDetailsComponent, canActivate: [AuthGuard] },
  { path: 'surveys/:id/update', component: CreateSurveyComponent, canActivate: [AuthGuard, AdminAuthGuard] },
  { path: 'surveys/:id/participate', component: CreateParticipationComponent, canActivate: [AuthGuard] },
  { path: 'surveys/:id/participations', component: ParticipationListComponent, canActivate: [AuthGuard] },
  { path: 'surveys/:id/participants', component: ParticipantListComponent, canActivate: [AuthGuard] },
  { path: 'surveys/:id/validate', component: ValidationComponent, canActivate: [AuthGuard, AdminAuthGuard] },
  { path: 'surveys/create', component: CreateSurveyComponent, canActivate: [AuthGuard, CreatorAuthGuard] },
  { path: 'time-slots', component: TimeSlotListComponent, canActivate: [AuthGuard, CreatorAuthGuard] },
  { path: 'time-slots/create', component: CreateTimeSlotComponent, canActivate: [AuthGuard, CreatorAuthGuard] },
  { path: 'time-slots/:id/update', component: CreateTimeSlotComponent, canActivate: [AuthGuard, CreatorAuthGuard] },
  { path: 'dates', component: DateListComponent, canActivate: [AuthGuard, CreatorAuthGuard] },
  { path: 'surveys/:id/dates', component: DateListComponent, canActivate: [AuthGuard, CreatorAuthGuard] },
  { path: 'dates/create', component: CreateDateComponent, canActivate: [AuthGuard, CreatorAuthGuard] },
  { path: 'no-right', component: NoRightComponent },
  { path: 'about-app', component: AboutAppComponent },
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { DateFormater } from './../../helper/date-formater';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'meetings-list-wrapper',
  templateUrl: './meetings-list-wrapper.component.html',
  styleUrls: ['./meetings-list-wrapper.component.css']
})
export class MeetingsListWrapperComponent implements OnInit {

  @Input('meetings') meetings
  headers: string[];
  fields: string[];
  @Input('data') data;
  @Output('showPresent') showPresent = new EventEmitter<number>();
  @Output('showAbsent') showAbsent = new EventEmitter<number>();
  dateFormater: DateFormater

  constructor() { 
    this.dateFormater = new DateFormater()
  }

  ngOnInit() {
  }

  showPresents(meetingId: number) {
    this.headers = ['Prenom', 'Nom', 'Actions'];
    this.fields = ['firstName', 'lastName'];
    this.showPresent.emit(meetingId);
  }

  showAbsents(meetingId: number){
    this.headers = ['Prenom', 'Nom', 'Actions'];
    this.fields = ['firstName', 'lastName'];
    this.showAbsent.emit(meetingId);
  }

  formatDate(date){
    return this.dateFormater.formatDate(date);
  }

  

}

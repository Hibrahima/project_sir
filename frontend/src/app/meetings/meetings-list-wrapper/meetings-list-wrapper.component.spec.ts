import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingsListWrapperComponent } from './meetings-list-wrapper.component';

describe('MeetingsListWrapperComponent', () => {
  let component: MeetingsListWrapperComponent;
  let fixture: ComponentFixture<MeetingsListWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingsListWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingsListWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { MeetingService } from './../../services/meeting.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-meeting-details',
  templateUrl: './meeting-details.component.html',
  styleUrls: ['./meeting-details.component.css']
})
export class MeetingDetailsComponent implements OnInit {

  meeting: any;
  meetings: any[] = [];
  data;
  meetingId: number;

  constructor(
    private route: ActivatedRoute, 
    private userService: UserService, 
    private notif: NotificationService,
    private meetingService: MeetingService) { }

  ngOnInit() {
    this.meetingId = Number(this.route.snapshot.paramMap.get('id'));
    this.meetingService.getById(this.meetingId).subscribe((meeting: any) => {
      if (meeting.error) {
        this.notif.showInfo("Je ne trouve pas la reunion avec id " + this.meetingId + 'Erreur : ' + meeting.message, "Erreur");
        return;
      }

      this.meeting = meeting;
      this.meetings.push(this.meeting);
    },
      error => { this.notif.showError("Je ne trouve pas la reunion avec id " + this.meetingId + 'Erreur : ' + JSON.stringify(error), "Erreur"); });
  }

  showPresents(value){
    if(this.meetingId){
      this.meetingService.getPresents(this.meetingId).subscribe((users:any) => {
        if (users.error) {
          this.notif.showInfo("Je ne trouve pas les presents de la reunion avec id " + this.meetingId + 'Erreur : ' + users.message, "Erreur");
          return;
        }

        this.data = users;
      },
      error => { this.notif.showError("Je ne trouve pas les presents de la reunion avec id " + this.meetingId + 'Erreur : ' + JSON.stringify(error), "Erreur"); } )
    }
  }

  showAbsents(value){
    if(this.meetingId){
      this.meetingService.getAbsents(this.meetingId).subscribe((users:any) => {
        if (users.error) {
          this.notif.showInfo("Je ne trouve pas les absents de la reunion avec id " + this.meetingId + 'Erreur : ' + users.message, "Erreur");
          return;
        }
        console.log('users from m '+JSON.stringify(users));
        this.data = users;
      },
      error => { this.notif.showError("Je ne trouve pas les absents de la reunion avec id " + this.meetingId + 'Erreur : ' + JSON.stringify(error), "Erreur"); } )
    }
  }

}

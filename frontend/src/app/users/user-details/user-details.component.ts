import { UserService } from './../../services/user.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  user: any;
  users: any[] = [];
  data;
  userId: number;

  constructor(private route: ActivatedRoute, private userService: UserService, private notif: NotificationService) { }

  ngOnInit() {
    this.userId = Number(this.route.snapshot.paramMap.get('id'));
    this.userService.getUserById(this.userId).subscribe((user: any) => {
      if (user.error) {
        this.notif.showInfo("Je ne trouve pas l'utilsareur avec id " + this.userId + 'Erreur : ' + user.message, "Erreur");
        return;
      }

      this.user = user;
      this.users.push(this.user);
    },
      error => { this.notif.showError("Je ne trouve pas l'utilsareur avec id " + this.userId + 'Erreur : ' + JSON.stringify(error), "Erreur"); });
  }

  showRoles(value) {
    if (this.user)
      this.data = this.user.roles;
  }

  showAllergies(value) {
    if (this.user)
      this.data = this.user.allergies;
  }

  showAttendedMeetings(value) {
    if(this.userId){
      this.userService.getAttentedMeetings(this.userId).subscribe((meetings:any) => {
        if (meetings.error) {
          this.notif.showInfo("Je ne trouve pas les reunions de l'utilisateur avec id " + this.userId + 'Erreur : ' + meetings.message, "Erreur");
          return;
        }
        this.data = meetings;
      },
      error => { this.notif.showError("Je ne trouve pas les reunions de l'utilsareur avec id " + this.userId + 'Erreur : ' + JSON.stringify(error), "Erreur"); });
    }
  }

  showMissedMeetings(value) {
    if(this.userId){
      this.userService.getMissededMeetings(this.userId).subscribe((meetings:any) => {
        if (meetings.error) {
          this.notif.showInfo("Je ne trouve pas les reunions de l'utilisateur avec id " + this.userId + 'Erreur : ' + meetings.message, "Erreur");
          return;
        }
        this.data = meetings;
      },
      error => { this.notif.showError("Je ne trouve pas les reunions de l'utilsareur avec id " + this.userId + 'Erreur : ' + JSON.stringify(error), "Erreur"); });
    }
  }

  showSurveys(value) {
    if(this.user)
    this.data = this.user.createdSurveys;
  }

}

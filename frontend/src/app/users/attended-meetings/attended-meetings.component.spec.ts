import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAttendedMeetingsComponent } from './attended-meetings.component';

describe('AttendedMeetingsComponent', () => {
  let component: UserAttendedMeetingsComponent;
  let fixture: ComponentFixture<UserAttendedMeetingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAttendedMeetingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAttendedMeetingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

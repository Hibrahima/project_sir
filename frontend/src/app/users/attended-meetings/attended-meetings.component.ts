import { MeetingService } from './../../services/meeting.service';
import { Subscription } from 'rxjs';
import { UserService } from './../../services/user.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-attended-meetings',
  templateUrl: './attended-meetings.component.html',
  styleUrls: ['./attended-meetings.component.css']
})
export class UserAttendedMeetingsComponent implements OnInit, OnDestroy {

  meetings: any[];
  subscription: Subscription;
  data: any[];

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private notif: NotificationService,
    private meetingService: MeetingService) {

    let userId = Number(this.route.snapshot.paramMap.get('id'));
    if (userId) {
      this.subscription = this.userService.getAttentedMeetings(userId).subscribe((m: any) => {
        if (m.error) {
          this.notif.showInfo('Je ne trouve pas les reunions auxquelles vous avez assistées, Erreur : ' + m.message, "Erreur");
          return;
        }
        this.meetings = m;
      },
        error => { this.notif.showInfo('Je ne trouve pas es reunions auxquelles vous avez assistées, Erreur : ' + JSON.stringify(error), "Erreur"); })
    }

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  showPresents(meetingId: number) {

    this.subscription = this.meetingService.getPresents(meetingId).subscribe((users: any) => {
      if (users.error) {
        this.notif.showInfo("Je ne trouve pas les presents de la reunion avec id " + meetingId + 'Erreur : ' + users.message, "Erreur");
        return;
      }

      this.data = users;
    },
      error => { this.notif.showError("Je ne trouve pas les presents de la reunion avec id " + meetingId + 'Erreur : ' + JSON.stringify(error), "Erreur"); })

  }

  showAbsents(meetingId: number) {

    this.subscription = this.meetingService.getAbsents(meetingId).subscribe((users: any) => {
      if (users.error) {
        this.notif.showInfo("Je ne trouve pas les absents de la reunion avec id " + meetingId + 'Erreur : ' + users.message, "Erreur");
        return;
      }

      this.data = users;
    },
      error => { this.notif.showError("Je ne trouve pas les absents de la reunion avec id " + meetingId + 'Erreur : ' + JSON.stringify(error), "Erreur"); })

  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserParticipationListComponent } from './participations.component';

describe('ParticipationsComponent', () => {
  let component: UserParticipationListComponent;
  let fixture: ComponentFixture<UserParticipationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserParticipationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserParticipationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

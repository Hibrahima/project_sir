import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-participations',
  templateUrl: './participations.component.html',
  styleUrls: ['./participations.component.css']
})
export class UserParticipationListComponent implements OnInit, OnDestroy {

  participations: any[];
  subscription: Subscription

  constructor(
    private userService: UserService, 
    private route: ActivatedRoute, 
    private notif: NotificationService) {

    let userId = Number(this.route.snapshot.paramMap.get('id'));
    if(userId){
      this.subscription = this.userService.getParticipations(userId).subscribe((p:any) => {
        if(p.error){
          this.notif.showInfo('Je ne trouve pas vos participation, Erreur : ' + p.message, "Erreur");
          return;
        }
        this.participations = p;
      }, 
      error => { this.notif.showInfo('Je ne trouve pas vos participation, Erreur : ' + JSON.stringify(error), "Erreur"); })
    } 
   }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  viewMore(participationId:number){

  }

}

import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { NotificationService } from 'src/app/services/notification.service';
import { MeetingService } from 'src/app/services/meeting.service';

@Component({
  selector: 'app-missed-meetings',
  templateUrl: './missed-meetings.component.html',
  styleUrls: ['./missed-meetings.component.css']
})
export class UserMissedMeetingsComponent implements OnInit {

  meetings: any[];
  subscription: Subscription;
  data: any[];

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private notif: NotificationService,
    private meetingService: MeetingService) {

    let userId = Number(this.route.snapshot.paramMap.get('id'));
    if (userId) {
      this.subscription = this.userService.getMissededMeetings(userId).subscribe((m: any) => {
        if (m.error) {
          this.notif.showInfo('Je ne trouve pas les reunions que vous avez manquées, Erreur : ' + m.message, "Erreur");
          return;
        }
        this.meetings = m;
        
      },
        error => { this.notif.showInfo('Je ne trouve pas les reunions que vous avez manquées, Erreur : ' + JSON.stringify(error), "Erreur"); })
    }

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  showPresents(meetingId: number) {

    this.subscription = this.meetingService.getPresents(meetingId).subscribe((users: any) => {
      if (users.error) {
        this.notif.showInfo("Je ne trouve pas les presents de la reunion avec id " + meetingId + 'Erreur : ' + users.message, "Erreur");
        return;
      }

      this.data = users;
    },
      error => { this.notif.showError("Je ne trouve pas les presents de la reunion avec id " + meetingId + 'Erreur : ' + JSON.stringify(error), "Erreur"); })

  }

  showAbsents(meetingId: number) {

    this.subscription = this.meetingService.getAbsents(meetingId).subscribe((users: any) => {
      if (users.error) {
        this.notif.showInfo("Je ne trouve pas les absents de la reunion avec id " + meetingId + 'Erreur : ' + users.message, "Erreur");
        return;
      }

      this.data = users;
    },
      error => { this.notif.showError("Je ne trouve pas les absents de la reunion avec id " + meetingId + 'Erreur : ' + JSON.stringify(error), "Erreur"); })

  }

}

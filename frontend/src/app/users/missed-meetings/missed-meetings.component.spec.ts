import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMissedMeetingsComponent } from './missed-meetings.component';

describe('MissedMeetingsComponent', () => {
  let component: UserMissedMeetingsComponent;
  let fixture: ComponentFixture<UserMissedMeetingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMissedMeetingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMissedMeetingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

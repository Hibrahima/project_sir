import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'user-list-wrapper',
  templateUrl: './user-list-wrapper.component.html',
  styleUrls: ['./user-list-wrapper.component.css']
})
export class UserListWrapperComponent implements OnInit {

  @Input('users') users
  headers: string[];
  fields: string[];
  @Input('data') data;
  @Output('showRole') showRole = new EventEmitter<number>();
  @Output('showAllergy') showAllergy = new EventEmitter<number>();;
  @Output('showAttendedMeeting') showAttendedMeeting = new EventEmitter<number>();
  @Output('showMissedMeeting') showMissedMeeting = new EventEmitter<number>();
  @Output('showSurvey') showSurvey = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  showRoles(userId:number) {
    /*this.showRole = true;
    this.showAllergy = false;
    this.showAttendedMeeting = false;
    this.showMissedMeeting = false;
    this.showSurvey = false;
    */
    this.headers = ['Nom', 'Actions'];
    this.fields = ['name'];
    this.showRole.emit(userId);
  }

  showAllergies(userId:number){
    /*this.showAllergy = true;
    this.showRole = false;
    this.showAttendedMeeting = false;
    this.showMissedMeeting = false;
    this.showSurvey = false;
    */
    this.headers = ['Nom', 'Actions'];
    this.fields = ['name'];
    this.showAllergy.emit(userId);
  }

  showSurveys(userId:number){
    /*this.showSurvey = true;
    this.showAllergy = false;
    this.showRole = false;
    this.showAttendedMeeting = false;
    this.showMissedMeeting = false;
    */
    this.headers = ['Nom', 'Lien', 'Actions'];
    this.fields = ['name', 'link'];
    this.showSurvey.emit(userId);
  }

  showAttendedMeetings(userId:number){
    /*this.showAttendedMeeting = true;
    this.showSurvey = false;
    this.showAllergy = false;
    this.showRole = false;
    this.showMissedMeeting = false;
    */
    this.headers = ['Libelle', 'Adresse', 'Actions'];
    this.fields = ['title', 'officeAddress'];
    this.showAttendedMeeting.emit(userId);
  }

  showMissedMeetings(userId:number){
    /*this.showMissedMeeting = true;
    this.showAttendedMeeting = false;
    this.showSurvey = false;
    this.showAllergy = false;
    this.showRole = false;
    */
    this.headers = ['Libelle', 'Adresse', 'Actions'];
    this.fields = ['title', 'officeAddress'];
    this.showMissedMeeting.emit(userId);
  }

}

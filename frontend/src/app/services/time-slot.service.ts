import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimeSlotService {

  baseUrl: string = "/api/time-slots";
  headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  create(timeSlot) {
    return this.http.post(this.baseUrl, timeSlot, { headers: this.headers });
  }

  getAll() {
    return this.http.get(this.baseUrl);
  }

  getById(id: number) {
    return this.http.get(this.baseUrl + '/' + id);
  }

  update(timeSlot) {
    return this.http.put(this.baseUrl, timeSlot, { headers: this.headers });
  }

  delete(id: number) {
    return this.http.delete(this.baseUrl + '/' + id);
  }

}

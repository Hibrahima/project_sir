import { LoginService } from './login.service';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private loginService: LoginService) { }

  isAuthenticated() {
    return this.loginService.getUser().then(user => {
      if (user) return true;

      return false;
    })
  }

  isAdmin() {
    return this.loginService.getUser().then(user => {
      if (user) {
        if (this.containRole(user, 'admin'))
          return true;

        return false;
      }
    })
  }

  isAdminOrCreator() {
    return this.loginService.getUser().then(user => {
      if (user) {
        if (this.containRole(user, 'admin') || this.containRole(user, 'creator') )
          return true;

        return false;
      }
    })
  }


  isCreator() {
    return this.loginService.getUser().then(user => {
      if (user) {
        if (this.containRole(user, 'createur'))
          return true;

        return false;
      }
    })
  }

  isParticipant() {
    return this.loginService.getUser().then(user => {
      if (user) {
        if (this.containRole(user, 'participant'))
          return true;

        return false;
      }
    })
  }

  containRole(user: any, roleName:string): boolean {
    let roles: any[] = user.roles;
    for (let i = 0; i < roles.length; i++) {
      if (roles[i].name === roleName)
        return true;
    }

    return false;
  }

}
import { ActivatedRoute } from '@angular/router';
import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url: string = '/api/users/login';
  headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  loggedUser = null;
  returnUrl: string;
  //options = new RequestOptions({ headers: this.headers });

  constructor(private http: HttpClient, private userService: UserService, private route: ActivatedRoute) { }


  getLoggedUserData(loginData) {

    // gets the returl url from the activated route
    // the return url was injected by the auth guard as a query param
    this.returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/dashboard';
    localStorage.setItem('returnUrl', this.returnUrl); 

    return this.http.post(this.url, loginData, { headers: this.headers });
  }

  setUser(user) {
    this.loggedUser = user;
  }

  async getUser() {
    if (this.loggedUser == null) {
      let id: number = Number(sessionStorage.getItem('user_id'));
      if(id) this.loggedUser = await this.userService.getUserById(id).toPromise();
    }
    return this.loggedUser;
  }

  logout(){
    this.loggedUser = null;
  }


}

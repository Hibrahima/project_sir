
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginRedirectAuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.authService.isAuthenticated().then(result => {
      // there is no logged in user, so return true so that this route can be activated
      if (!result) return true;

    
      let returnUrl = localStorage.getItem('returnUrl');
      if (!returnUrl) return;

      // removes the return url from the local storage
      localStorage.removeItem('returnUrl');
      this.router.navigateByUrl(returnUrl);
      return false;
    });
  }
}

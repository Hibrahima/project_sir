import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MeetingService {

  baseUrl: string = '/api/meetings';
  headers:HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  getById(id: number) {
    return this.http.get(this.baseUrl + '/' + id);
  }

  getAll() {
    return this.http.get(this.baseUrl);
  }

  getPresents(meetingId: number) {
    return this.http.get(this.baseUrl + '/' + meetingId + '/presents');
  }

  getAbsents(meetingId: number) {
    return this.http.get(this.baseUrl + '/' + meetingId + '/absents');
  }


}

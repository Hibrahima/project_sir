import { TestBed } from '@angular/core/testing';

import { CreatorAuthGuardService } from './creator-auth-guard.service';

describe('CreatorAuthGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreatorAuthGuardService = TestBed.get(CreatorAuthGuardService);
    expect(service).toBeTruthy();
  });
});

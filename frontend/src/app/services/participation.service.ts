import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParticipationService {

  baseUrl: string = '/api/participations';
  headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  create(participation :any){
    return this.http.post(this.baseUrl, participation, {headers:this.headers});
  }

  getAll(){
    return this.http.get(this.baseUrl);
  }

  canUserParticipate(userId:number, surveyId:number){
    return this.http.get(this.baseUrl + '/can-participate/' + userId +'/' + surveyId);
  }
}

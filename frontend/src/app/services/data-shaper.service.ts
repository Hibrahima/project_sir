import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataShaperService {

  constructor() { }

  formatTimeSlot(timeSlot: any) {
    let beginHour = timeSlot.begin.hour;
    let beginMinute = timeSlot.begin.minute;
    let endHour = timeSlot.end.hour;
    let endMinute = timeSlot.end.minute;
    // add  leading zeros
    if (beginHour < 10)
      beginHour = '0' + beginHour;

    if (beginMinute < 10)
      beginMinute = '0' + beginMinute;

    if (endHour < 10)
      endHour = '0' + endHour;

    if (endMinute < 10)
      endMinute = '0' + endMinute;

    return {
      id: timeSlot.id,
      begin: '' + beginHour + ':' + beginMinute + ':00', // 0 second by defaul
      end: '' + endHour + ':' + endMinute + ':00' // 0 second by defaul as well
    }
  }

  shapeDate(date:any){
    let month = '' + date.date.month;
    let day = '' + date.date.day;
    if(Number(month) < 10)
      month = '0' + month;
    if(Number(day) < 10)
      day = '0' + day;
   
    return {
      name: date.name,
      date: date.date.year + '-' + month + '-' + day,
      timeSlots: date.timeSlots
    }
  }

  shapeComplexDate(date:any){
    let timeSlots = [];
    for(let i=0; i<date.timeSlots.length; i++){
      let current = date.timeSlots[i];
      timeSlots.push(this.formatTimeSlot(current));
    }


    return {
      id: date.id,
      name: date.name,
      date: date.date.year + '-' + date.date.monthValue + '-' + date.date.dayOfMonth,
      timeSlots: timeSlots
     }
   /* let shapedDate = this.shapeDate(date);
    shapedDate.timeSlots = timeSlots;
    return shapedDate;*/
  }
}

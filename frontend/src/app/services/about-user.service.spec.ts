import { TestBed } from '@angular/core/testing';

import { AboutUserService } from './about-user.service';

describe('AboutUserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AboutUserService = TestBed.get(AboutUserService);
    expect(service).toBeTruthy();
  });
});

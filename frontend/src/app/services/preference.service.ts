import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PreferenceService {

  baseUrl: string = 'api/food-preferences';
  headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private hhtp: HttpClient) { }

  savePrefAndAllergy(object: any){
    return this.hhtp.post(this.baseUrl + '/save-prefs-allergies', object, { headers: this.headers });
  }
}

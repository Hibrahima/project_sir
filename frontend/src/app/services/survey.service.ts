import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {
  baseUrl: string = '/api/surveys';
  headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  create(userId: number, survey) {
    let url: string = 'api/users/' + userId + '/surveys';
    return this.http.post(url, survey, { headers: this.headers })
  }

  getAll() {
    return this.http.get(this.baseUrl);
  }

  getById(id: number) {
    return this.http.get(this.baseUrl + '/' + id);
  }

  getCreator(surveyId: number) {
    return this.http.get(this.baseUrl + '/' + surveyId + '/creator');
  }

  getParticipations(surveyId: number) {
    return this.http.get(this.baseUrl + '/' + surveyId + '/participations');
  }

  getParticipants(surveyId: number) {
    return this.http.get(this.baseUrl + '/' + surveyId + '/participants');
  }

  getSuveyWithAllData(surveyId: number, fn) {

    this.getById(surveyId).pipe(take(1)).toPromise().then((survey: any) => {
      if (survey.error) fn(undefined);

      this.getCreator(surveyId).pipe(take(1)).toPromise().then((creator: any) => {
        if (creator.error) return undefined;
        survey.creator = creator;
      }, error => { fn(undefined); })
      .then(() => {
        this.getParticipations(surveyId).toPromise().then((p1: any) => {
          if (p1.error) fn(undefined);
          survey.participations = p1;
        }, error => { fn(undefined); }) 
      })
      .then(() => {
        this.getParticipants(surveyId).toPromise().then((p2: any) => {
          if (p2.error) fn(undefined);
          survey.participants = p2;
          fn(survey);
        }, error => { fn(undefined); })
      });


    }, error => { fn(undefined); });
  }

  update(suveyMeetingObject) {
    return this.http.put(this.baseUrl, suveyMeetingObject, { headers: this.headers });
  }

  validate(data:any){
    return this.http.post(this.baseUrl + '/validate', data, { headers: this.headers });
  }

}

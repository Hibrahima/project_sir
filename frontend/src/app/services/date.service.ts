import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  baseUrl: string = '/api/dates';
  headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http:HttpClient) {
   }

   getAll(){
     return this.http.get(this.baseUrl);
   }

   delete(dateId:number){
     return this.http.delete(this.baseUrl + '/' + dateId);
   }

   create(date:any){
     return this.http.post(this.baseUrl, date, {headers:this.headers});
   }

   getByStatus(status:boolean){
     return this.http.get(this.baseUrl + '/search/by-status/' + status);
   }
}

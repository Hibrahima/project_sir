import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';




@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    
    return this.authService.isAuthenticated().then(result => {
      // user is logged in, so return true
      if (result) return true;

      /* 
       return false when no user is logged in and redirects to login
       return url is retrived at a later time in the 'LoginRedirectGuard' to navigate the user to the originated source url
       */
      this.router.navigate(['/login-register'], { queryParams: { returnUrl: state.url } });
      return false;
    });
  }
}

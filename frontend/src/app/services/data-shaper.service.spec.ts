import { TestBed } from '@angular/core/testing';

import { DataShaperService } from './data-shaper.service';

describe('DataShaperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataShaperService = TestBed.get(DataShaperService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl: string = '/api/users';
  headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }
 
  createUser(user) {
    return this.http.post(this.baseUrl + '/register', user, { headers: this.headers });
  }

  getUserById(id: number) {
    return this.http.get(this.baseUrl + '/' + id);

  }
  getAll() {
    return this.http.get(this.baseUrl);

  }

  getAttentedMeetings(userId:number){
    return this.http.get(this.baseUrl + '/' + userId + '/attended-meetings');
  }


  getMissededMeetings(userId:number){
    return this.http.get(this.baseUrl + '/' + userId + '/missed-meetings');
  }

  getParticipations(userId: number){
    return this.http.get(this.baseUrl + '/' + userId + '/participations')
  }
}

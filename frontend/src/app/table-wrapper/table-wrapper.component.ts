import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'table-wrapper',
  templateUrl: './table-wrapper.component.html',
  styleUrls: ['./table-wrapper.component.css']
})
export class TableWrapperComponent implements OnInit {

  @Input('headers') headers: string[];
  @Input('fields') fields: string[];
  @Input('data') data;

  constructor() { }

  ngOnInit() {
  }

}

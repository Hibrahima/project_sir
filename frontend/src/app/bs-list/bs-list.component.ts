import { BsList } from './../models/bs-list';
import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-bs-list',
  templateUrl: './bs-list.component.html',
  styleUrls: ['./bs-list.component.css']
})
export class BsListComponent implements OnInit {

  @Input('list') list: BsList[] = [];

  constructor() { }

  ngOnInit() {
  }

  formatBadgeValue(value:Number){
    if(!value)
      return ""

    return value
  }

}

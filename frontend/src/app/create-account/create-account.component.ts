import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service'
import { NotificationService } from '../services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {
  user = {
    lastName: "",
    firstName: "",
    password: "",
    email: ""
  };
  confirm_password: string;
  match_password: boolean = true;
  constructor(
    private userservice: UserService, 
    private notif: NotificationService,
    private router: Router) { }

  ngOnInit() {
  }

  createUserForm() {
    if (this.user.password == this.confirm_password) {
      //this.match_password = true;
      this.userservice.createUser(this.user).subscribe((dataUser:any) => {
        if(dataUser.error){
          this.notif.showError('Je ne peux pas créer votre compte.', 'Erreur');
          return;
        }
        
        
        this.notif.showSuccess('Votre compte a bien été créé.', 'Compte Créé');
      
      }, error => { this.notif.showError('Je ne peux pas creer votre compte.', 'Erreur'); });
    }
    else { this.match_password = false; }
  }
}

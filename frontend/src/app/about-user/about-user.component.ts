import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../user';
@Component({
  selector: 'app-about-user',
  templateUrl: './about-user.component.html',
  styleUrls: ['./about-user.component.css']
})
export class AboutUserComponent implements OnInit {
  setUsers: User[];
  id: number = 5;
  length_of_users: number;
  constructor(private userservice: UserService) {

  }

  ngOnInit() {
    this.userservice.getAll().subscribe((data) => {
      this.length_of_users = data['length'];
    });

    this.getAllUser();
  }
  getAllUser() {
    this.userservice.getAll().subscribe((data) => {
      this.setUsers = data as User[];
    });
  }
}

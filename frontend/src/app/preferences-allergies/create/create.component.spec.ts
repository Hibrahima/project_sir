import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFoodPrefAllergyComponent } from './create.component';

describe('CreateComponent', () => {
  let component: CreateFoodPrefAllergyComponent;
  let fixture: ComponentFixture<CreateFoodPrefAllergyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFoodPrefAllergyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFoodPrefAllergyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Subscription } from 'rxjs';
import { PreferenceService } from './../../services/preference.service';
import { MeetingService } from 'src/app/services/meeting.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateFoodPrefAllergyComponent implements OnInit, OnDestroy {

  preference = {
    id: {
      userId: undefined,
      meetingId: undefined
    },
    name: "",
    description: ""
  }

  allergy = {
    name: ""
  }
  meeting: any;

  foodPrefAndAllergy = {
    pref: this.preference,
    allergy: this.allergy
  }

  subscription1: Subscription;
  subscription2: Subscription;

  constructor(
    private route: ActivatedRoute,
    private meetingService: MeetingService,
    private notif: NotificationService,
    private preferenceService: PreferenceService,
    private router: Router) {

    let meetingId = Number(this.route.snapshot.paramMap.get('id'));
    if (meetingId) {
      this.subscription1 = this.meetingService.getById(meetingId).subscribe((m: any) => {
        if (m.error) {
          this.notif.showError('Je ne trouve les infos de la reunion ' + meetingId + ', Erreur : ' + m.meesage, 'Erreur');
          return;
        }

        this.meeting = m;

      }, error => { this.notif.showError('Je ne trouve les infos de la reunion ' + meetingId + ', Erreur : ' + JSON.stringify(error), 'Erreur'); });
    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.subscription1)
      this.subscription1.unsubscribe();
    if (this.subscription2)
      this.subscription2.unsubscribe();
  }

  saveChanges() {
    let userId:number = Number(sessionStorage.getItem('user_id'));
    this.preference.id.userId = userId;
    this.preference.id.meetingId = this.meeting.id;

    this.subscription2 = this.preferenceService.savePrefAndAllergy(this.foodPrefAndAllergy).subscribe((res:any) => {
      if(res.error){
        this.notif.showError('Je ne suis pas arrivé à enregister vos préferences ou/et allergies, Erreur : ' + res.message, 'Erreur');
        return;
      }

     this.notif.showSuccess(' Vos preferences ou/et allergies ont été enrégistrées', 'Informmations Enrégistrées');
     
     if(userId){ this.router.navigateByUrl('/users/' + userId + '/prefs-allergies'); }
     

    }, error => { this.notif.showError('Je ne suis pas arrivé à enregister vos préferences ou/et allergies, Erreur : ' + JSON.stringify(error), 'Erreur'); } );
  }

}

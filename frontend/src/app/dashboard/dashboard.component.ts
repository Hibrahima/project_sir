import { AuthService } from './../services/auth.service';
import { LoginService } from './../services/login.service';
import { Component, OnInit, Input } from '@angular/core';
import { elementStart } from '@angular/core/src/render3';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  loggedUser;

  elements: any[] = []
  isAdmin: boolean;

  constructor(
    private loginService: LoginService,
    private authService: AuthService,
    private router: Router) {

    this.loginService.getUser().then(user => {
      this.loggedUser = user;
      this.authService.isAdmin().then(result => this.isAdmin = result);
      //this.authService.isCreator().then(result => this.isCreator = result);
      //this.authService.isParticipant().then(result => this.isParticipant = result);
    })
  }

  ngOnInit() {

    this.elements.push({ name: 'dashboard', classColor: 'lay_dsh', badge: undefined, link: '/dashboard', text: 'Dashboard', iClass: 'fa fa-home', iColor: '#F48FB1' })
    this.elements.push({ name: 'created-surveys', classColor: 'lay_created_surveys', badge: this.loggedUser.createdSurveys.length, link: '/users/' + this.loggedUser.id + '/created-surveys', text: 'Sondages Crées', iClass: 'fa fa-folder-open', iColor: '#EF9A9A' })
    this.elements.push({ name: 'attended-meetings', classColor: 'lay_attended_meetings', badge: undefined, link: '/users/' + this.loggedUser.id + '/attended-meetings', text: 'Reunions Assistées', iClass: 'fa fa-bars', iColor: '#EF9A9A' })
    this.elements.push({ name: 'missed-meetings', classColor: 'lay_missed_meetings', badge: undefined, link: '/users/' + this.loggedUser.id + '/missed-meetings', text: 'Reunions Manquées', iClass: 'fa fa-bars', iColor: '#EF9A9A' })
    if (this.isAdmin) {
      this.elements.push({ name: 'users', classColor: 'lay_user', badge: undefined, link: '/users', text: 'Utilsateurs', iClass: 'fa fa-users', iColor: '#EF9A9A' })
      this.elements.push({ name: 'dates', classColor: 'lay_participations', badge: undefined, link: '/dates', text: 'Gérer Dates', iClass: 'fa fa-sticky-note', iColor: '#EF9A9A' });
    }
    this.elements.push({ name: 'all_surveys', classColor: 'lay_all_surveys', badge: undefined, link: '/surveys', text: 'Tous les Sondages', iClass: 'fa fa-book', iColor: '#EF9A9A' })
    this.elements.push({ name: 'my-participations', classColor: 'lay_participations', badge: undefined, link: '/users/' + this.loggedUser.id + '/participations', text: 'Mes Participations', iClass: 'fa fa-sticky-note', iColor: '#EF9A9A' })


  }

  goTo(name: string) {
    switch (name) {
      case 'dashboard':
        this.router.navigateByUrl('/dashboard');
        break;
      case 'created-surveys':
        this.router.navigateByUrl('/users/' + this.loggedUser.id + '/created-surveys');
        break;
      case 'attended-meetings':
        this.router.navigateByUrl('/users/' + this.loggedUser.id + '/attended-meetings');
        break;
      case 'missed-meetings':
        this.router.navigateByUrl('/users/' + this.loggedUser.id + '/missed-meetings');
        break;
      case 'users':
        this.router.navigateByUrl('/users');
        break;
      case 'dates':
        this.router.navigateByUrl('/dates');
        break;
      case 'all_surveys':
        this.router.navigateByUrl('/surveys');
        break;
      case 'my-participations':
        this.router.navigateByUrl('/users/' + this.loggedUser.id + '/participations');
        break;

      default:
        break;
    }
  }

}

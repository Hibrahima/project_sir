import { SurveyService } from 'src/app/services/survey.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-participant-list',
  templateUrl: './participant-list.component.html',
  styleUrls: ['./participant-list.component.css']
})
export class ParticipantListComponent implements OnInit {


  users: any[] = [];
  data;
 
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private notif: NotificationService,
    private surveyService: SurveyService) { }

  ngOnInit() {
    let surveyId = Number(this.route.snapshot.paramMap.get('id'));
    if (surveyId) {
      this.surveyService.getParticipants(surveyId).subscribe((users: any) => {
        if (users.error) {
          this.notif.showInfo('Je ne trouve pas les participants de ce sondage, Erreur : ' + users.message, 'Erreur');
          return;
        }

        this.users = users;
      },
        error => { this.notif.showError('Je ne trouve pas les participants de ce sondage, Erreur : ' + JSON.stringify(error), 'Erreur'); });
    }

  }

  findUser(userId: number) {
    if (this.users) {
      for (let i = 0; i < this.users.length; i++) {
        if (this.users[i].id === userId)
          return this.users[i];
      }
      return undefined;
    }

    return undefined;
  }

  showRoles(userId: number) {
    let user = this.findUser(userId);
    if (user)
      this.data = user.roles;
  }

  showAllergies(userId: number) {
    let user = this.findUser(userId);
    if (user)
      this.data = user.allergies;
  }

  showAttendedMeetings(userId: number) {
    this.userService.getAttentedMeetings(userId).subscribe((meetings: any) => {
      if (meetings.error) {
        this.notif.showInfo("Je ne trouve pas les reunions de l'utilisateur avec id " + userId + 'Erreur : ' + meetings.message, "Erreur");
        return;
      }
      this.data = meetings;
    },
      error => { this.notif.showError("Je ne trouve pas les reunions de l'utilsareur avec id " + userId + 'Erreur : ' + JSON.stringify(error), "Erreur"); });

  }

  showMissedMeetings(userId: number) {
    this.userService.getMissededMeetings(userId).subscribe((meetings: any) => {
      if (meetings.error) {
        this.notif.showInfo("Je ne trouve pas les reunions de l'utilisateur avec id " + userId + 'Erreur : ' + meetings.message, "Erreur");
        return;
      }
      this.data = meetings;
    },
      error => { this.notif.showError("Je ne trouve pas les reunions de l'utilsareur avec id " + userId + 'Erreur : ' + JSON.stringify(error), "Erreur"); });

  }

  showSurveys(userId: number) {
    let user = this.findUser(userId);
    if (user)
      this.data = user.createdSurveys;
  }

}

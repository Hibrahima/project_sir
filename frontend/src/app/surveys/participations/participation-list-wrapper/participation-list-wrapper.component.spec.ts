import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipationListWrapperComponent } from './participation-list-wrapper.component';

describe('ParticipationListWrapperComponent', () => {
  let component: ParticipationListWrapperComponent;
  let fixture: ComponentFixture<ParticipationListWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipationListWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipationListWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

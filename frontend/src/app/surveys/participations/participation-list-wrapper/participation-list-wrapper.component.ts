import { DateFormater } from 'src/app/helper/date-formater';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'participation-list-wrapper',
  templateUrl: './participation-list-wrapper.component.html',
  styleUrls: ['./participation-list-wrapper.component.css']
})
export class ParticipationListWrapperComponent implements OnInit {

  @Input('participations') participations
  @Output('id') idEmitter = new EventEmitter<boolean>();
  dateFormater: DateFormater;

  constructor() {
    this.dateFormater = new DateFormater();
   }

  ngOnInit() {
  }

  formatDate(date){
    return this.dateFormater.formatDate(date);
  }
  formatTime(time){
    return this.dateFormater.formatTime(time);
  }

  viewMore(id) {
    this.idEmitter.emit(id);
  }

}

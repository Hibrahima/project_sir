import { DataShaperService } from 'src/app/services/data-shaper.service';
import { ParticipationService } from './../../../services/participation.service';
import { SurveyService } from 'src/app/services/survey.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, ModuleWithComponentFactories } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';
import { DateFormater } from 'src/app/helper/date-formater';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-participation',
  templateUrl: './create-participation.component.html',
  styleUrls: ['./create-participation.component.css']
})
export class CreateParticipationComponent implements OnInit, OnDestroy {

  surveyDates: any[] = undefined;
  subscription1: Subscription;
  subscription2: Subscription;
  maxNumberOfRows: number;
  dateFormater: DateFormater;
  survey: any;
  userId: number;
  participation = {
    id: {
      userId: this.userId,
      surveyId: undefined
    },
    timeSlots: []
  };


  constructor(
    private route: ActivatedRoute,
    private surveyService: SurveyService,
    private notif: NotificationService,
    private participationService: ParticipationService,
    private dataShaper: DataShaperService,
    private router: Router) {

    this.dateFormater = new DateFormater();
    let surveyId = Number(route.snapshot.paramMap.get('id'));
    if (surveyId) {
      this.userId = Number(sessionStorage.getItem('user_id'));

      this.subscription1 = this.participationService.canUserParticipate(this.userId, surveyId).subscribe((result: any) => {
        if (result.error) {
          notif.showError("Je rencontre un probleme interne. Veuillez réessayer plus tard, Erreur " + result.meesage, 'Erreur')
          return;
        }

        if (result.hasParticipated) {
          notif.showInfo('Vous avez déjà partiicpé à ce sondage', 'Déjà Participé ?');
          this.router.navigateByUrl('/no-right');
        }
        else {
          this.subscription2 = surveyService.getById(surveyId).subscribe((survey: any) => {
            if (survey.error) {
              notif.showError('Erreur lors de la recuperation du sondage avec id ' + surveyId + '. Erreur ' + survey.message, 'Erreur');
              return;
            }

            if (survey.status === 'VALIDATED') {
              notif.showInfo('Ce sondage a déjà été validé', 'Sondage déja VALIDÉ');
              this.router.navigateByUrl('/no-right')
            }

            this.surveyDates = survey.dates;
            //this.maxNumberOfRows = this.findMaxOccurence();
            this.survey = survey;
            this.participation.id.surveyId = survey.id;
            this.participation.id.userId = this.userId;
          },
            error => {
              notif.showError('Erreur lors de la recuperation du sondage avec id ' + surveyId, 'Erreur');
            })
        }

      }, error => { notif.showError("Je rencontre un probleme interne. Veuillez réessayer plus tard", 'Erreur'); })


    }

  }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.subscription1)
      this.subscription1.unsubscribe();
    if (this.subscription2)
      this.subscription2.unsubscribe();
  }

  addTimeSlot(timeSlot: any) {
    this.participation.timeSlots.push(timeSlot);
  }

  findTimeSlot(id: number) {
    for (let i = 0; i < this.participation.timeSlots.length; i++) {
      if (this.participation.timeSlots[i].id === id)
        return { index: i, exist: true };
    }

    return { index: -1, exits: false };
  }

  removeTimeSlot(id: number) {
    let result = this.findTimeSlot(id);
    if (result.exist) {
      this.participation.timeSlots.splice(result.index, 1);
    }
  }

  addOrRemoveTimeSlots(object: any) {
    let checkboxState = object.state;
    let timeSlot = object.timeSlot;
    if (checkboxState) {
      this.addTimeSlot(timeSlot);
      this.notif.showInfo('La plage horaire ' + this.formatTime(timeSlot) + ' a été ajoutée avec succès', 'Plage Horaire Ajoutée');
    }

    else {
      this.removeTimeSlot(timeSlot.id);
      this.notif.showInfo('La plage horaire ' + this.formatTime(timeSlot) + ' a été rétirée avec succès', 'Plage Horaire Rétirée');
    }
  }


  formatTime(timeSlot: any) {
    return this.dateFormater.formatTimeWithBeginAndEnd(timeSlot);
  }

  sendChoices() {
    this.shapeTimeSlots();
    if (this.participation.timeSlots.length == 0) {
      this.notif.showInfo('Vous devez au moins sélectionner une plage horaire', 'Pas de Plage Horaire Séctionnée!');
      return;
    }
    this.participationService.create(this.participation).subscribe((res: any) => {
      if (res.error) {
        this.notif.showError('Oups, je ne suis pas arrivé à enregister votre participation', 'Erreur');
        return;
      }

      this.notif.showSuccess('Votre participation a bien été enregistrée. Merci!', 'Participation Bien Enregistrée');
      this.router.navigateByUrl('/users/' + this.participation.id.userId + '/participations');
    },
      error => {

      })
  }

  shapeTimeSlots() {
    for (let i = 0; i < this.participation.timeSlots.length; i++) {
      this.participation.timeSlots[i] = this.dataShaper.formatTimeSlot(this.participation.timeSlots[i]);
    }
  }

  /*formatDate(date:any){
    return this.dateFormater.formatDate(date);
  }

  filterDates(dayOfWeek: string) {
    let filteredDates: any[] = []
    for (let i = 0; i < this.surveyDates.length; i++) {
      if (this.surveyDates[i].date.dayOfWeek === dayOfWeek)
        filteredDates.push(this.surveyDates[i]);
    }

    console.log('filtered dates : '+filteredDates);
    return filteredDates;
  }

  filteredDays() {
    let filteredDays: string[] = [];
    for (let i = 0; i < this.surveyDates.length; i++) {
      if (!this.containsThatDay(filteredDays, this.surveyDates[i].date.dayOfWeek))
        filteredDays.push(this.surveyDates[i].date.dayOfWeek);
    }
    return filteredDays;
  }

  containsThatDay(days: string[], dayOfWeek: string): boolean {

    for (let i = 0; i < days.length; i++) {
      if (days[i] === dayOfWeek)
        return true;
    }
    return false;
  }

  getDayNumber(dayOfWeek: string): number {
    switch (dayOfWeek) {
      case "MONDAY":
        return 1;
      case "TUESDAY":
        return 2;
      case "WEDNESDAY":
        return 3;
      case "THURSDAY":
        return 4;
      case "FRIDAY":
        return 5;
      case "SATURDAY":
        return 6;
      case "SUNDAY":
        return 7;
      default:
        break;
    }
  }

  findDateOccurence(dayOfWeek: string): number {
    //let dayNumber: number = this.getDayNumber(dayOfWeek);
    let ocurence: number = 0;
    for (let i = 0; i < this.surveyDates.length; i++) {
      if (this.surveyDates[i].date.dayOfWeek === dayOfWeek)
        ocurence++;
    }

    return ocurence;
  }

  findMaxOccurence(): number {
    let occurrences: number[] = [];
    for (let i = 0; i < this.surveyDates.length; i++) {
      occurrences.push(this.findDateOccurence(this.surveyDates[i].date.dayOfWeek));
    }

    return Math.max(...occurrences);
  }*/

}

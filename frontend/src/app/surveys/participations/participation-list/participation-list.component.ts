import { Subscription } from 'rxjs';
import { SurveyService } from './../../../services/survey.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-participation-list',
  templateUrl: './participation-list.component.html',
  styleUrls: ['./participation-list.component.css']
})
export class ParticipationListComponent implements OnInit, OnDestroy {

  participations:any;
  subscription: Subscription;

  constructor(
    private route: ActivatedRoute, 
    private surveyService: SurveyService,
    private notif: NotificationService) {

    let surveyId = Number(route.snapshot.paramMap.get('id'))
    if(surveyId){
      this.subscription = surveyService.getParticipations(surveyId).subscribe((p: any) => {
        if(p.error){
          this.notif.showError("Je n'arrive pas a recupere les participations du sondage avec id "  + surveyId, "Erreur");
          return;
        }
        this.participations = p;
      }, 
      error => { this.notif.showError("Je n'arrive pas a recupere les participations sondage avec id "  + surveyId, "Erreur"); })
    }
    else{
      this.subscription = surveyService.getAll().subscribe((p: any) => {
        if(p.error){
          this.notif.showError("Je n'arrive pas a recupere les participations du sondage avec id "  + surveyId, "Erreur");
          return;
        }
        this.participations = p;
      }, 
      error => { this.notif.showError("Je n'arrive pas a recupere les participations sondage avec id "  + surveyId, "Erreur"); })
    }
   }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  viewMore(id){

  }

}

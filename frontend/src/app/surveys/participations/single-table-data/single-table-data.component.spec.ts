import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleTableDataComponent } from './single-table-data.component';

describe('SingleTableDataComponent', () => {
  let component: SingleTableDataComponent;
  let fixture: ComponentFixture<SingleTableDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleTableDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleTableDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

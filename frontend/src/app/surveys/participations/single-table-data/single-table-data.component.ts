import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DateFormater } from 'src/app/helper/date-formater';



@Component({
  selector: 'single-table-data',
  templateUrl: './single-table-data.component.html',
  styleUrls: ['./single-table-data.component.css']
})
export class SingleTableDataComponent implements OnInit {

  @Input('surveyDates') surveyDates;
  @Input('day') day;
  @Output('dataToEmit') dataToEmit = new EventEmitter;
  @Input('isCheckbox') isCheckbox: boolean = true
  dateFormater: DateFormater;

  constructor() { 
    this.dateFormater = new DateFormater();
  }

  ngOnInit() {
  }

  formatDate(date:any){
    return this.dateFormater.formatDate(date.date);
  }

  formatTime(time:any){
    return this.dateFormater.formatTimeWithBeginAndEnd(time);
  }

  filterDates() {
    let filteredDates: any[] = []
    for (let i = 0; i < this.surveyDates.length; i++) {
      if (this.surveyDates[i].date.dayOfWeek === this.day)
        filteredDates.push(this.surveyDates[i]);
    }

    return filteredDates;
  }

  sendData(event, timeSlot:any){
    let object = {
      state: event.target.checked,
      timeSlot: timeSlot
    }
    this.dataToEmit.emit(object);
  }


}

import { DateFormater } from 'src/app/helper/date-formater';
import { Component, OnInit, Input } from '@angular/core';
import { DateService } from 'src/app/services/date.service';

@Component({
  selector: 'single-date-column',
  templateUrl: './single-date-column.component.html',
  styleUrls: ['./single-date-column.component.css']
})
export class SingleDateColumnComponent implements OnInit {

  @Input('dates') dates;
  dateFormater: DateFormater;

  constructor() { 
    this.dateFormater = new DateFormater();
  }

  ngOnInit() {
  }

  formatDate(date:any){
    return this.dateFormater.formatDateWithDayName(date);
  }

  formatTime(time:any){
    return this.dateFormater.formatTimeWithBeginAndEnd(time);
  }

}

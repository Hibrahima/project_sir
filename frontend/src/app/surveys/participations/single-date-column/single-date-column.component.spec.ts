import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleDateColumnComponent } from './single-date-column.component';

describe('SingleDateColumnComponent', () => {
  let component: SingleDateColumnComponent;
  let fixture: ComponentFixture<SingleDateColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleDateColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleDateColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { AuthService } from './../../services/auth.service';
import { DateFormater } from './../../helper/date-formater';
import { SurveyService } from './../../services/survey.service';
import { BsList } from './../../models/bs-list';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'app-survey-details',
  templateUrl: './survey-details.component.html',
  styleUrls: ['./survey-details.component.css']
})
export class SurveyDetailsComponent implements OnInit {

  list: BsList[] = []
  survey;
  dateFornater: DateFormater;
  //$survey: Observable<any>;

  constructor(
    private surveyService: SurveyService,
    private route: ActivatedRoute,
    private notif: NotificationService,
    private authService: AuthService) {

    this.dateFornater = new DateFormater();
  }

  ngOnInit() {
    let id: number = Number(this.route.snapshot.paramMap.get('id'));
    this.surveyService.getSuveyWithAllData(id, (survey) => {
      if (survey === undefined) {
        this.notif.showError("Je n'arrive pas à récupérer le sondage avec id " + id, "Erreur");
        return;
      }
      this.survey = survey;
      if (survey.status != 'READY' && survey.dates) {
        this.list.push(new BsList('Voir Dates', '/surveys/' + id + '/dates', survey.dates.length));
      }
      /*if (survey.status === 'VALIDATED') {
        this.list.push(new BsList('Voir Résultat Final', '/time-slots/' + 1 + '/update', null));
      }*/
      
      if (survey.creator.createdSurveys)
        this.list.push(new BsList('Voir Créateur', '/users/' + survey.creator.id + '/details', null));

      this.list.push(new BsList('Voir Reunion', '/meetings/' + survey.meeting.id + '/details', null));

      if (survey.participations)
        this.list.push(new BsList('Voir Participations', '/surveys/' + id + '/participations', survey.participations.length));

      if (survey.participants)
        this.list.push(new BsList('Voir Participants', '/surveys/' + id + '/participants', survey.participations.length));

      this.authService.isAdmin().then(isAdmin => {
        if (isAdmin) {
          if (survey.status != 'VALIDATED')
            this.list.push(new BsList('Valider/Confirmer', '/surveys/' + id + '/validate', survey.participations.length));
        }
      })

    });

  }

  formatDate(date) {
    return this.dateFornater.formatDate(date);
  }

  formatTime(time) {
    return this.dateFornater.formatTime(time);
  }

}

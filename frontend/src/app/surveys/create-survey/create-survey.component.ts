import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from './../../services/notification.service';
import { SurveyService } from './../../services/survey.service';
import { Component, OnInit } from '@angular/core';
import { DateService } from 'src/app/services/date.service';
import { DataShaperService } from 'src/app/services/data-shaper.service';

@Component({
  selector: 'app-create-survey',
  templateUrl: './create-survey.component.html',
  styleUrls: ['./create-survey.component.css']
})
export class CreateSurveyComponent implements OnInit {

  survey = {
    surveyId: undefined,
    meetingTitle: "",
    meetingOfficeAddress: "",
    surveyName: "",
    meetingSummary: "",
    dates: []
  }
  id: number;
  datesFromServer;

  constructor(
    private surveyService: SurveyService,
    private notif: NotificationService,
    private route: ActivatedRoute,
    private dateService: DateService,
    private dataShaper: DataShaperService,
    private router: Router) {

    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      surveyService.getById(this.id).subscribe((survey: any) => {
        this.survey = {
          surveyId: survey.id,
          meetingTitle: survey.meeting.title,
          meetingOfficeAddress: survey.meeting.officeAddress,
          surveyName: survey.name,
          meetingSummary: survey.meeting.summary,
          dates: survey.dates
        }
      },
        error => {
          notif.showError("Oups, something went wrong!", "Unknown Error");
        })
    }
  }

  ngOnInit() {
    this.dateService.getByStatus(true).subscribe((res: any) => {
      if (res.error) {
        this.notif.showError('Impossible de recuperer les dates qui sont actives. Erreur : ' + res.message, 'Erreur Serveur');
        return;
      }
      this.datesFromServer = res;
    },
      error => {
        this.notif.showError('Impossible de recuperer les dates qui sont actives. Erreur : ' + error, 'Erreur Serveur');
      })
  }

  saveChanges() {
    if (this.id) {
      this.updateSurvey();
    }
    else {
      this.persistSurvey();
    }

  }

  updateSurvey() {
    if (this.survey.dates.length < 1) {
      this.notif.showInfo('Vous devez au moins selectionner une date', 'Liste de date vide');
      return;
    }

    this.surveyService.update(this.survey).subscribe((survey: any) => {
      this.notif.showSuccess(survey.name + ' was sucessfully updated', 'Update');
    },
      error => {
        this.notif.showError('Oups. something went wrong!', 'Unknown Error');
      })
  }

  persistSurvey() {
    if (this.survey.dates.length < 1) {
      this.notif.showInfo('Vous devez au moins selectionner une date', 'Liste de date vide');
      return;
    }
    let userId = Number(sessionStorage.getItem('user_id'));
    this.surveyService.create(userId, this.survey).subscribe((result: any) => {
      if (result.error) {
        this.notif.showError('Oups, somethig went wrong! Error ' + result.message, 'Unknown error');
      }
      else {
        this.notif.showSuccess(result.surveyName + 'was successfully created', 'Sondage créé avec succès');
        this.router.navigateByUrl('/surveys');
      }
    }, 
    error=>{
      this.notif.showError('Oups, somethig went wrong! Error ' + error, 'Unknown error');
    })
  }

  shapeDateTimeSlots(date: any) {
    let result = '';
    for (let i = 0; i < date.timeSlots.length; i++) {
      let current = date.timeSlots[i]
      result += current.begin.hour + ':' + current.begin.minute + ' - ' + current.end.hour + ':' + current.end.minute + ' ** ';
    }
    return result;
  }

  containsSameDate(date: any): boolean {
    let result = this.getDate(date);
    if (result.exist)
      return true;

    return false;
  }

  getDate(date: any): any {
    for (let i = 0; i < this.survey.dates.length; i++) {
      let current = this.survey.dates[i];
      if (current.name === date.name && current.timeSlots.length === date.timeSlots.length) {
        return { index: i, exist: true };
      }
    }

    return { index: -1, exist: false };
  }

  addDate(date: any) {
    let result = this.containsSameDate(date);
    if (result) {
      this.notif.showInfo('Cette date a deja ete ajoutee', 'Date dupliquee');
      return;
    }
    let shapedDate = this.dataShaper.shapeComplexDate(date);
    this.survey.dates.push(shapedDate);
  }

  removeDate(date: any) {
    let result = this.getDate(date);
    this.survey.dates.splice(result.index, 1)
  }
}

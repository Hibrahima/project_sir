import { SurveyService } from './../../services/survey.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TimeSlotListComponent } from './../../time-slots/time-slot-list/time-slot-list.component';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ValidationComponent implements OnInit {

  participations: any;
  customDates: any;
  selectedTimeSlot:any = undefined;
  validsatinData = {
    surveyId: undefined,
    timeSlotId: undefined
  }
  surveyId:number = undefined
  constructor(
    private route: ActivatedRoute,
    private surveyService: SurveyService,
    private notif: NotificationService,
    private router: Router) { }

  ngOnInit() {
    this.surveyId  = Number(this.route.snapshot.paramMap.get('id'));
    if (this.surveyId) {
      this.surveyService.getParticipations(this.surveyId).subscribe((p: any) => {
        if (p.error) {
          this.notif.showError("Je n'arrive pas a recupere les participations du sondage avec id " + this.surveyId, "Erreur");
          return;
        }
        this.participations = p;
        this.customDates = this.transformData()
      },
        error => { this.notif.showError("Je n'arrive pas a recupere les participations sondage avec id " + this.surveyId, "Erreur"); })
    }
  }

  getTimeSlotsFromDate(day: string) {
    let timeSlots: any[] = [];
    for (let i = 0; i < this.participations.length; i++) {
      let p = this.participations[i];
      for (let j = 0; j < p.timeSlots.length; j++) {
        let timeSlot = p.timeSlots[j];
        if (timeSlot.date.dayOfWeek === day) {
          timeSlots.push(timeSlot)
        }
      }
    }

    return timeSlots;
  }

  transformData() {
    let customDates: any[] = [];
    for (let i = 0; i < this.participations.length; i++) {
      let p = this.participations[i];
      for (let j = 0; j < p.timeSlots.length; j++) {
        let timeSlot = p.timeSlots[j];
        let cd = {
          date: timeSlot.date,
          timeSlots: this.getTimeSlotsFromDate(timeSlot.date.dayOfWeek)
        }

        if(!this.contains(cd.date.dayOfWeek, customDates))
          customDates.push(cd);
      }
    }

    return customDates;
  }

  contains(dayOfWeek:string, customsDates:any[]){
    for(let i = 0; i < customsDates.length; i++){
      if(customsDates[i].date.dayOfWeek === dayOfWeek)
        return true;
    }

    return false;
  }

  updateSelectedTimeSlot(object: any) {
    this.selectedTimeSlot = object.state ?  object.timeSlot : undefined;
  }

  validateSurvey() {
    if(this.selectedTimeSlot === undefined){
      this.notif.showInfo('Vous devez sélectionner la plage horaire à retenir comme résut final.' ,'Pas de Plahe Hoarire Sélctionée');
      return;
    }

    this.validsatinData.surveyId = this.surveyId;
    this.validsatinData.timeSlotId = this.selectedTimeSlot.id;
    this.surveyService.validate(this.validsatinData).subscribe((result:any) => {
      if(result.error) {
        this.notif.showError("Je n'arrive pas a valider le choix final pour le sondage avec id " + this.surveyId + 'Erreur : '+result.message, "Erreur");
        return;
      }

      this.notif.showSuccess('Votre réponse a bien été enrégistrée', 'Réponse Enrégistrée');
      this.router.navigateByUrl('/dashboard');

    }, error => { this.notif.showError("Je n'arrive pas a valider le choix final pour le sondage avec id " + this.surveyId, "Erreur"); } );

  }

}

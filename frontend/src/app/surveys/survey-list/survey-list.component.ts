import { ActivatedRoute } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { LoginService } from './../../services/login.service';
import { DateFormater } from './../../helper/date-formater';
import { Component, OnInit } from '@angular/core';
import { SurveyService } from 'src/app/services/survey.service';
import { NotificationService } from 'src/app/services/notification.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {

  surveys$;
  dateFormater: DateFormater;
  headers = ['Nom', 'Lien'];
  fields = ['name', 'link'];
  isAdmin: boolean;
  link: string;

  constructor(
    private surveryService: SurveyService,
    private notif: NotificationService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private loginService: LoginService) {

    this.dateFormater = new DateFormater();

    this.authService.isAdmin().then(result => this.isAdmin = result);
    //this.authService.isCreator().then(result => this.isCreator = result);
    //this.authService.isParticipant().then(result => this.isParticipant = result);
  }

  ngOnInit() {
    let userId = Number(this.route.snapshot.paramMap.get('id'));
    if (userId) {
      this.loginService.getUser().then(user => {
        if (user) {
          this.surveys$ = new Observable(observer => { 
            observer.next(user.createdSurveys);
            observer.complete();
            return;
          });
          
        }
      })
    }
    this.surveys$ = this.surveryService.getAll();
  }

  formatDate(date) {
    return this.dateFormater.formatDate(date);
  }

  formatTime(time) {
    return this.dateFormater.formatTime(time);
  }

  formatMeetingDate(date, time) {
    return this.dateFormater.formatMeetingDate(date, time);
  }


}

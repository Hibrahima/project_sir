import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyDateListComponent } from './survey-date-list.component';

describe('SurveyDateListComponent', () => {
  let component: SurveyDateListComponent;
  let fixture: ComponentFixture<SurveyDateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyDateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyDateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

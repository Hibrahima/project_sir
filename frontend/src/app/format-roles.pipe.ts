import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatRoles'
})
export class FormatRolesPipe implements PipeTransform {

  transform(value: any[], args?: any): any {
    if(value.length) {
      return value
        .map(i => i.name)
        .join(', ')
    }
    return 'User';
  }

}

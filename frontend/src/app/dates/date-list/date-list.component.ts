import { SurveyService } from './../../services/survey.service';
import { DateFormater } from './../../helper/date-formater';
import { Subscription } from 'rxjs';
import { DateService } from './../../services/date.service';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-date-list',
  templateUrl: './date-list.component.html',
  styleUrls: ['./date-list.component.css']
})
export class DateListComponent implements OnInit {

  dates;
  subscription: Subscription;
  dateFormater: DateFormater;

  constructor(
    private dateService: DateService,
    private notif: NotificationService,
    private route: ActivatedRoute,
    private surveyService: SurveyService) {

    this.dateFormater = new DateFormater();
  }

  ngOnInit() {
    this.updateView();
  }


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  updateView() {
    let id: number = Number(this.route.snapshot.paramMap.get('id'));
    if (id) {
     this.subscription = this.surveyService.getById(id).subscribe((survey: any) => {
        if (survey.error) {
          this.notif.showError("Je n'arrive pas a recupere le sondage avec id " + id, "Erreur");
          return;
        }
        this.dates = survey.dates;
      },
        error => { this.notif.showError("Je n'arrive pas a recupere le sondage avec id " + id, "Erreur"); })
    }
    else {
      this.subscription = this.dateService.getAll().subscribe((res: any) => {
        if (res.error) {
          this.notif.showError('Oups, something went wrong', 'Unknown Error');
          return;
        }
        this.dates = res;
      },
        error => { this.notif.showError('Oups, something went wrong', 'Unknown Error'); })
    }

  }


  // formatStatus(status: boolean): string {
  //   if (status)
  //     return "Actif";

  //   return "Inactif";
  // }

  deleteDate(dateId: number) {
    let shouldIDelete: boolean = confirm('Are you sure you want to delete this date ?');
    if (shouldIDelete) {
      this.dateService.delete(dateId).subscribe((result: any) => {
        if (result.error) {
          this.notif.showError('Oups, something went wrong! Error : ' + result.message, 'Unknown Error');
          return;
        }

        this.notif.showSuccess('This date was successfully deleted', 'Successful Deletion');
        this.updateView();
      },
        error => {
          this.notif.showError('Oups, something went wrong! Error : ' + error, 'Unknown Error');
        });
    }

  }




}

import { DataShaperService } from './../../services/data-shaper.service';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';
import { DateService } from 'src/app/services/date.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-date',
  templateUrl: './create-date.component.html',
  styleUrls: ['./create-date.component.css']
})
export class CreateDateComponent implements OnInit {

  currentDate = new Date();
  date = {
    name: "",
    date: {
      year: this.currentDate.getFullYear(),
      month: this.currentDate.getMonth() + 1,
      day: this.currentDate.getDate()
    },
    timeSlots: []
  }

  constructor(
    private notifService: NotificationService, 
    private dateService: DateService,
    private dataShaper: DataShaperService,
    private router: Router) { }

  ngOnInit() {
  }

  

  saveChanges() {
    let shapedDate = this.dataShaper.shapeDate(this.date);
    if(this.date.timeSlots.length <1){
      this.notifService.showInfo('Vous devez au moins ajouter une plage horaire', 'Liste de plages horaires vide');
      return;
    }
    this.dateService.create(shapedDate).subscribe((res:any)=>{
      if(res.error){
        this.notifService.showError("Cette date n'a pas pu être créée. Erreur : "+res.message, "Impossible de creer cette date");
        return;
      }
      this.notifService.showSuccess('Cette a ete creee avec succees', 'Date cree avec succes');
      this.router.navigateByUrl('/dates');
    }, 
    error =>{
      this.notifService.showError("Cette date n'a pas pu être créée. Erreur : "+error, "Impossible de creer cette date");
    })
  }

  formatDate() {
    return this.date.date.day + '/' + this.date.date.month + '/' + this.date.date.year;
  }

  addTimeSlot(timeSlot: any) {
    if(this.containsSameTimeSlot(timeSlot)){
      this.notifService.showInfo('Cette plage horaire a déja été ajoutée', 'Plage Horaire Duliquée');
      return;
    }
    this.date.timeSlots.push(timeSlot);
  }

  containsSameTimeSlot(timeSlot: any):boolean {
    let result = this.getTimeSlot(timeSlot);
    if (result.exist)
        return true;

    return false;
  }

  getTimeSlot(timeSlot:any):any{
    for(let i=0; i<this.date.timeSlots.length; i++){
      let current = this.date.timeSlots[i];
      if (current.begin === timeSlot.begin && current.end === timeSlot.end){
        return {index: i, exist: true};
      }
    }

    return {index: -1, exist: false};
  }

  removeTimeSlot(timeSlot:any){
    let result = this.getTimeSlot(timeSlot);
    this.date.timeSlots.splice(result.index, 1);
  }

}

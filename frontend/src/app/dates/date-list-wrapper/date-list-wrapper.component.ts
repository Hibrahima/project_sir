import { AuthService } from './../../services/auth.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DateFormater } from 'src/app/helper/date-formater';
import { DateService } from 'src/app/services/date.service';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'date-list-wrapper',
  templateUrl: './date-list-wrapper.component.html',
  styleUrls: ['./date-list-wrapper.component.css']
})
export class DateListWrapperComponent implements OnInit {

  @Input('dates') dates;
  @Output('dateId') dateId = new EventEmitter<number>();
  dateFormater: DateFormater;
  isAdmin: boolean;

  constructor(private authService: AuthService) {

    this.dateFormater = new DateFormater();
    this.authService.isAdmin().then(result => this.isAdmin = result);
    //this.authService.isCreator().then(result => this.isCreator = result);
    //this.authService.isParticipant().then(result => this.isParticipant = result);
   }

  ngOnInit() {
  }

  formateDate(date) {
    return this.dateFormater.formatDate(date);
  }




  formatStatus(status: boolean): string {
    if (status)
      return "Actif";

    return "Inactif";
  }


  deleteDate(dateId: number) {
    this.dateId.emit(dateId);
  }

}

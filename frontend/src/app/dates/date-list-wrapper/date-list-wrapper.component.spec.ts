import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateListWrapperComponent } from './date-list-wrapper.component';

describe('DateListWrapperComponent', () => {
  let component: DateListWrapperComponent;
  let fixture: ComponentFixture<DateListWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateListWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateListWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

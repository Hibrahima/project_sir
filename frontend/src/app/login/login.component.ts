import { NotificationService } from './../services/notification.service';
import { LoginService } from './../services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData = {}


  constructor(private loginService: LoginService, 
    private notificationService: NotificationService,
    private router: Router) { }

  ngOnInit() { }

  login() {
    this.loginService.getLoggedUserData(JSON.stringify(this.loginData)).subscribe((user:any) => {
      if(user.error){
        this.notificationService.showInfo("Email ou mot de passe incorrects! Veuillez reéssayer.", "Login")
      }
      else{
        this.loginService.setUser(user)
        sessionStorage.setItem('user_id', user.id)
        this.router.navigateByUrl(this.loginService.returnUrl);
      }
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css']
})
export class LeftNavComponent implements OnInit {

  elements: any[] = [];
  isAdmin: boolean;
  loggedUser;

  constructor(
    private loginService: LoginService,
    private authService: AuthService) {

    this.loginService.getUser().then(user => {
      this.loggedUser = user;
      this.authService.isAdmin().then(result => this.isAdmin = result);
      //this.authService.isCreator().then(result => this.isCreator = result);
      //this.authService.isParticipant().then(result => this.isParticipant = result);
    })
  }

  ngOnInit() {

    if (this.loggedUser) {
      this.elements.push({ name: 'dashboard', classColor: 'lay_dsh', badge: undefined, link: '/dashboard', text: 'Dashboard', iClass: 'fa fa-home', iColor: '#F48FB1' })
      this.elements.push({ name: 'created-surveys', classColor: 'lay_created_surveys', badge: this.loggedUser.createdSurveys.length, link: '/users/' + this.loggedUser.id + '/created-surveys', text: 'Sondages Crées', iClass: 'fa fa-folder-open', iColor: '#EF9A9A' })
      this.elements.push({ name: 'attended-meetings', classColor: 'lay_attended_meetings', badge: undefined, link: '/users/' + this.loggedUser.id + '/attended-meetings', text: 'Reunions Assistées', iClass: 'fa fa-bars', iColor: '#EF9A9A' })
      this.elements.push({ name: 'missed-meetings', classColor: 'lay_missed_meetings', badge: undefined, link: '/users/' + this.loggedUser.id + '/missed-meetings', text: 'Reunions Manquées', iClass: 'fa fa-bars', iColor: '#EF9A9A' })
      if (this.isAdmin) {
        this.elements.push({ name: 'users', classColor: 'lay_user', badge: undefined, link: '/users', text: 'Utilsateurs', iClass: 'fa fa-users', iColor: '#EF9A9A' })
        this.elements.push({ name: 'dates', classColor: 'lay_participations', badge: undefined, link: '/dates', text: 'Gérer Dates', iClass: 'fa fa-sticky-note', iColor: '#EF9A9A' });
      }
      this.elements.push({ name: 'all_surveys', classColor: 'lay_all_surveys', badge: undefined, link: '/surveys', text: 'Tous les Sondages', iClass: 'fa fa-book', iColor: '#EF9A9A' })
      this.elements.push({ name: 'my-participations', classColor: 'lay_participations', badge: undefined, link: '/users/' + this.loggedUser.id + '/participations', text: 'Mes Participations', iClass: 'fa fa-sticky-note', iColor: '#EF9A9A' })
    }


  }

}

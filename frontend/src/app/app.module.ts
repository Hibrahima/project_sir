import { CreateFoodPrefAllergyComponent } from './preferences-allergies/create/create.component';
import { CreateTimeSlotComponent } from './time-slots/create-time-slot/create-time-slot.component';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';  
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CreateAccountComponent } from './create-account/create-account.component';
import { AboutUserComponent } from './about-user/about-user.component';
import { FormatRolesPipe } from './format-roles.pipe';
import { UpdateUserComponent } from './update-user/update-user.component';
import { LoginRegisterComponent } from './login-register/login-register.component';
import { CreateSurveyComponent } from './surveys/create-survey/create-survey.component';
import { SurveyListComponent } from './surveys/survey-list/survey-list.component';
import { BsListComponent } from './bs-list/bs-list.component';
import { SurveyDetailsComponent } from './surveys/survey-details/survey-details.component';
import { TimeSlotListComponent } from './time-slots/time-slot-list/time-slot-list.component';
import { DateListComponent } from './dates/date-list/date-list.component';
import { DateListWrapperComponent } from './dates/date-list-wrapper/date-list-wrapper.component';
import { SurveyDateListComponent } from './surveys/survey-date-list/survey-date-list.component';
import { CreateDateComponent } from './dates/create-date/create-date.component';
import { TimeSlotListWrapperComponent } from './time-slots/time-slot-list-wrapper/time-slot-list-wrapper.component';
import { ToastrModule } from 'ngx-toastr';
import { CreateParticipationComponent } from './surveys/participations/create-participation/create-participation.component';
import { NTimesDirective } from './directives/n-times.directive';
import { SingleDateColumnComponent } from './surveys/participations/single-date-column/single-date-column.component';
import { SingleTableDataComponent } from './surveys/participations/single-table-data/single-table-data.component';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { UserListWrapperComponent } from './users/user-list-wrapper/user-list-wrapper.component';
import { TableWrapperComponent } from './table-wrapper/table-wrapper.component';
import { MeetingDetailsComponent } from './meetings/meeting-details/meeting-details.component';
import { MeetingsListWrapperComponent } from './meetings/meetings-list-wrapper/meetings-list-wrapper.component';
import { ParticipationListWrapperComponent } from './surveys/participations/participation-list-wrapper/participation-list-wrapper.component';
import { ParticipationListComponent } from './surveys/participations/participation-list/participation-list.component';
import { ValidationComponent } from './surveys/validation/validation.component';
import { NoRightComponent } from './no-right/no-right.component';
import { UserParticipationListComponent } from './users/participations/participations.component';
import { UserAttendedMeetingsComponent } from './users/attended-meetings/attended-meetings.component';
import { UserMissedMeetingsComponent } from './users/missed-meetings/missed-meetings.component';
import { ParticipantListComponent } from './surveys/participant-list/participant-list.component';
import { LeftNavComponent } from './left-nav/left-nav.component';
import { AboutAppComponent } from './about-app/about-app.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CreateAccountComponent,
    AboutUserComponent,
    FormatRolesPipe,
    UpdateUserComponent,
    DashboardComponent,
    NotFoundComponent,
    BsNavbarComponent,
    CreateAccountComponent,
    LoginRegisterComponent,
    CreateSurveyComponent,
    SurveyListComponent,
    BsListComponent,
    SurveyDetailsComponent,
    CreateTimeSlotComponent,
    TimeSlotListComponent,
    DateListComponent,
    DateListWrapperComponent,
    SurveyDateListComponent,
    CreateDateComponent,
    TimeSlotListWrapperComponent,
    CreateParticipationComponent,
    NTimesDirective,
    SingleDateColumnComponent,
    SingleTableDataComponent,
    UserDetailsComponent,
    UserListWrapperComponent,
    TableWrapperComponent,
    MeetingsListWrapperComponent,
    MeetingDetailsComponent,
    ParticipationListWrapperComponent,
    ParticipationListComponent,
    ValidationComponent,
    NoRightComponent,
    UserParticipationListComponent,
    UserAttendedMeetingsComponent,
    UserMissedMeetingsComponent,
    ParticipantListComponent,
    CreateFoodPrefAllergyComponent,
    LeftNavComponent,
    AboutAppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgbModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class DateFormater {

    formatDate(date) {
        if(!date)
            return "Not set / En cours";
        return date.dayOfMonth + "/" + date.monthValue + "/" + date.year;
    }

    formatTime(time) {
        if(!time)
            return "Not set / En cours"
        return time.hour + ":" + time.minute + ":" + time.second;
    }

    formatTimeWithBeginAndEnd(timeSlot:any){
      let begin = this.formatTime(timeSlot.begin);
      let end = this.formatTime(timeSlot.end);
      return begin + " - " + end;
    }

    formatMeetingDate(date, time) {
        if (!date || !time)
            return "En cours"

        return this.formatDate(date) + " - " + time;
    }

    formatDateWithDayName(date:any) {
        if(!date)
            return "Not set";
        return this.getDayName(date.dayOfWeek) + " " + date.dayOfMonth + "/" + date.monthValue + "/" + date.year;
    }

    getDayName(dayOfWeek: string): string {
        switch (dayOfWeek) {
          case "MONDAY":
            return "Lundi";
          case "TUESDAY":
            return "Mardi";
          case "WEDNESDAY":
            return "Mercredi";
          case "THURSDAY":
            return "Jeudi";
          case "FRIDAY":
            return "Vendredi";
          case "SATURDAY":
            return "Samedi";
          case "SUNDAY":
            return "Dimanche";
          default:
            break;
        }
      }

}

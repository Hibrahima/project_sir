import { AuthService } from './../services/auth.service';
import { LoginService } from './../services/login.service';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../services/notification.service';
import { Router } from '@angular/router';


@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {

  loggedUser;
  isAdmin: boolean;
  isCreator: boolean;
  isParticipant: boolean;

  constructor(
    private loginService: LoginService,
    private notifService: NotificationService,
    private router: Router,
    private authService: AuthService) {


    this.loginService.getUser().then(
      user => {
        this.loggedUser = user;
        this.authService.isAdmin().then(result => this.isAdmin = result);
        this.authService.isCreator().then(result => this.isCreator = result);
        this.authService.isParticipant().then(result => this.isParticipant = result);
      },
      error => this.notifService.showError('Oups, something went wrong! Please retry later', 'Unknow error')
    );
  }

  ngOnInit() {
  }

  logout() {
    let userId = sessionStorage.getItem('user_id');
    if (userId) { sessionStorage.removeItem('user_id'); }
    this.loginService.logout();
    this.loggedUser = null;
    this.router.navigate(['/login-register']);
  }

  showUserSurveys(){
    this.router.navigateByUrl('/users/' + this.loggedUser.id + '/created-surveys');
  }

  showUserParticipations(){
    this.router.navigateByUrl('/users/' + this.loggedUser.id + '/participations');
  }

  showUserAttendedMeetings(){
    this.router.navigateByUrl('/users/' + this.loggedUser.id + '/attended-meetings');
  }

  showUserMissedMeetings(){
    this.router.navigateByUrl('/users/' + this.loggedUser.id + '/missed-meetings');
  }

}
